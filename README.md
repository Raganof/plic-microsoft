# README #

This repository contains the "VirtualWiki" project sources.

### What is VirtualWiki? ###

* VirtualWiki aims to add a brand new experience
  to discovery on Wikipedia under the form of a
  virtual space!


### How do I get set up? ###

* Dependencies:
- .Net Framework >= 4.5
- .Net libraries: Json.Net, Neo4jClient, dotNetRdf (these are included in the project), RabbitMQ

- Neo4j Database.


## Important Note ##
As of now, the application targets the Neo4j
db at http://localhost:7474/db/data/, but should be soon changed to support
an environment variable!
