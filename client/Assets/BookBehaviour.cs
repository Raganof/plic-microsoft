﻿using UnityEngine;
using System.Collections.Generic;
using Assets.Scripts.DataModels;
using UnityEngine.UI;

public class BookBehaviour : MonoBehaviour {

    private GameObject keyToDisplay;
    private Camera cameraView;
    public Camera nextCamera;
    public float distance = 1f;

    private bool isDisplaying;
    private GameObject player;
    private GameObject mainCamera;
    private Camera prevCamera;
    private FirstPersonController playerController;
    private MouseLocker mouseLocker;
    private HashIDs hash;
    private Animator anim; // The animator to animate the character
    //private BoxCollider collider;

    private BookContent bookContent;

    public Preview Preview { get; set; }
    private GameObject gms;
    private Search s;

    private bool isObserving = false;

    private float interactTime;

    private Text actionText;

    public void Awake()
    {
        keyToDisplay = GameObject.Find("KeyEventCanvas").transform.GetChild(0).gameObject;
        actionText = keyToDisplay.transform.GetChild(0).gameObject.GetComponent<Text>();

        cameraView = GameObject.Find("CameraSearchBord").GetComponent<Camera>();

        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        player = GameObject.FindGameObjectWithTag("Player");
        playerController = player.GetComponent<FirstPersonController>();
        mouseLocker = GameObject.FindGameObjectWithTag("GameController").GetComponent<MouseLocker>();
        hash = GameObject.FindGameObjectWithTag("GameController").GetComponent<HashIDs>();
        anim = player.GetComponentInChildren<Animator>();
        Preview = null;

        s = GameObject.FindGameObjectWithTag("Search").GetComponent<Search>();

        bookContent = GameObject.Find("BookPanel").GetComponent<BookContent>();
    }

    public void OnTriggerEnter(Collider other)
    {
        //Display the animation
        if (other.gameObject == player && !isDisplaying
            && Preview != null)
        {
            isDisplaying = true;
            keyToDisplay.SetActive(true);
            actionText.text = "Read \"" + Preview.Name + "\"";
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player && isDisplaying
            && Preview != null)
        {
            isDisplaying = false;
            keyToDisplay.SetActive(false);
            mouseLocker.Undisplay();
            actionText.text = "";
        }
    }

    public void Open()
    {
        bookContent.Open(this, "Book_" + Preview.Id);
    }

    public void Return()
    {
        bookContent.Return();
    }

    public void Update()
    {
        if (Time.time < interactTime + 0.5)
            return;

        //If the player is in the area and press the button Space...
        if (isDisplaying && Input.GetButtonDown("Interact") && !isObserving
            && Preview != null)
        {
            isObserving = true;
            // We unlock the mouse
            //mouseLocker.Display();
            //We hide the key
            keyToDisplay.SetActive(false);

            //... then we place him in front of the object
            Open();
            Debug.Log("Oppppppppppppppppppeeeeeeeen");
            interactTime = Time.time;
        }
        else if (isObserving && Input.GetKeyDown(KeyCode.Escape))
        {
            isObserving = false;
            //mouseLocker.Undisplay();
            //playerController.isLock = false;
            keyToDisplay.SetActive(true);

            Return();
        }
    }

    public void MoveAndObserve(Camera prevCamera)
    {
        if (prevCamera != null)
            this.prevCamera = prevCamera;
        interactTime = Time.time;
        keyToDisplay.SetActive(false);

        // We unlock the mouse
        //mouseLocker.Display();
        isDisplaying = true;

        //... then we place him in front of the object
        ObserveObject(Preview);

        isObserving = true;
        //We hide the key
        keyToDisplay.SetActive(false);


    }

    public void ObserveObject(Preview preview)
    {
        //Lock all movement of the player
        playerController.isLock = true;
        anim.SetFloat(hash.speedFloat, 0);
  

        if (s != null)
            s.ShowDescription(Preview);
        

        //Rotate the player to be in front of the object 

        if (cameraView != null)
        {
            mainCamera.gameObject.SetActive(false);
            cameraView.enabled = true;
            cameraView.tag = "MainCamera";
            player.transform.position = new Vector3(cameraView.transform.position.x, player.transform.position.y, cameraView.transform.position.z);
            player.transform.rotation = Quaternion.Euler(player.transform.eulerAngles.x, cameraView.transform.eulerAngles.y, player.transform.eulerAngles.z);
            if (cameraView.transform.eulerAngles.y - cameraView.transform.eulerAngles.y % 1f == 0)
                player.transform.Translate(0f, 0f, -0.2f);
            else if (cameraView.transform.eulerAngles.y - cameraView.transform.eulerAngles.y % 1f == 90)
                player.transform.Translate(-0.2f, 0f, 0f);
            else if (cameraView.transform.eulerAngles.y - cameraView.transform.eulerAngles.y % 1f == 270)
                player.transform.Translate(0.2f, 0f, 0f);
            else
                player.transform.Translate(0f, 0f, -0.2f);
        }
        else
        {
            player.transform.LookAt(transform);

            float x = gameObject.transform.position.x - player.transform.position.x;
            float z = gameObject.transform.position.z - player.transform.position.z;
            float yAngle = Mathf.Atan(z / x);
            yAngle = Mathf.Rad2Deg * yAngle;

            if (yAngle > 0)
                yAngle -= 90;
            else
                yAngle += 90;

            player.transform.RotateAround(gameObject.transform.position, Vector3.up, yAngle);

            //Move the player forward or backward to be at the best position
            Vector3 direction = new Vector3(gameObject.transform.position.x - player.transform.position.x, 0, gameObject.transform.position.z - player.transform.position.z);

            float scale = -distance / direction.magnitude;
            player.transform.position = gameObject.transform.position;
            player.transform.Translate(direction * scale);
        }
    }
}
