﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.DataModels;
using Assets.Scripts.Format;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using System.Collections.Generic;

public class BookContent : MonoBehaviour {

    private GameObject mainCamera;
    private MouseLocker mouseLocker;
    private GameObject player;
    private FirstPersonController playerController;
    private GameObject bookCanvas;
    public WWW WwwContent { get; set; }

    private Transform returnButton;
    private Transform nextButton;
    private Transform previousButton;

    private Transform summaryButton;

    private Text pageTitle;
    private Text pageContent;

    private Transform content;

    private LoadEvent loadingBar;
    private int currentPage;

    private BookBehaviour book;
    private string articleName;

    private bool locked;

    private bool wasLoaded;
    private bool loaded;

    private CanvasGroup cg;

    public Button buttonPrefab;

    private List<List<Button>> summaryButtons;
	// Use this for initialization
	void Start () {
        currentPage = 0;

        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        mouseLocker = GameObject.FindGameObjectWithTag("GameController").GetComponent<MouseLocker>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerController = player.GetComponent<FirstPersonController>();
        //bookCanvas = GameObject.FindGameObjectWithTag("BookCanvas");
        loadingBar = transform.FindChild("Loading_Bar").GetComponent<LoadEvent>();

        cg = transform.parent.GetComponent<CanvasGroup>();

        pageTitle = gameObject.transform.FindChild("Title").FindChild("Text").GetComponent<Text>();
        pageContent = gameObject.transform.FindChild("Content").FindChild("ScrollView").FindChild("Text").GetComponent<Text>();

        content = gameObject.transform.FindChild("Content");

        summaryButtons = new List<List<Button>>();

        WwwContent = null;
        book = null;
        articleName = "";

        returnButton = transform.FindChild("Return");
        nextButton = transform.FindChild("Next");
        previousButton = transform.FindChild("Previous");
        summaryButton = transform.FindChild("Summary_Button");

        returnButton.GetComponent<Button>().onClick.AddListener(() => { Return(); });

        this.wasLoaded = false;
        this.loaded = false;

        UpdateUI(loaded);

        this.locked = false;
        
	}

    void UpdateUI(bool status)
    {
        returnButton.gameObject.SetActive(status);
        //nextButton.gameObject.SetActive(status);
        //previousButton.gameObject.SetActive(status);
        pageTitle.transform.parent.gameObject.SetActive(status);
        //pageContent.transform.parent.gameObject.SetActive(status);
        content.gameObject.SetActive(status);
        summaryButton.gameObject.SetActive(status);

        this.wasLoaded = status;
    }
	
	// Update is called once per frame
	void Update () {

        if (this.loaded != this.wasLoaded)
            UpdateUI(this.loaded);

        if (this.locked)
        {
            mouseLocker.Display();
            playerController.isLock = true;
        }
        else
        {
            playerController.isLock = false;
        }

        if (WwwContent != null && WwwContent.isDone
            && this.book != null)
        {
            CreateBookContent(this.book);

            nextButton.GetComponent<Button>().onClick.AddListener(() => { NextPage(this.book); });
            previousButton.GetComponent<Button>().onClick.AddListener(() => { PreviousPage(this.book); });

            summaryButton.GetComponent<Button>().onClick.AddListener(() => { DisplaySummaryPage(this.book, 0); });

            DisplayPage(this.book, currentPage);

            BookBehaviour bh = GameObject.Find(this.articleName).GetComponent<BookBehaviour>();
            Debug.Log("Namu: " + this.articleName);
            bh = this.book;

            WwwContent = null;
        }
	}

    public void Open(BookBehaviour book, string tag)
    {
        this.locked = true;
        this.book = book;
        this.articleName = tag;


        this.cg.interactable = true;
        this.cg.blocksRaycasts = true;
        this.cg.alpha = 1;

        if (this.book.Preview.Content == null)
        {
            RequestContent(this.book.Preview.Id);
        }
        else
        {
            nextButton.GetComponent<Button>().onClick.AddListener(() => { NextPage(this.book); });
            previousButton.GetComponent<Button>().onClick.AddListener(() => { PreviousPage(this.book); });

            DisplayPage(this.book, currentPage);
        }
    }

    void NextPage(BookBehaviour book)
    {
        currentPage++;
        DisplayPage(book, currentPage);
    }

    void PreviousPage(BookBehaviour book)
    {
        currentPage--;
        DisplayPage(book, currentPage);
    }

    public void Return()
    {
        currentPage = 0;
        //this.book = null;
        //this.articleName = "";

        this.locked = false;
        this.loaded = false;

        this.cg.interactable = false;
        this.cg.blocksRaycasts = false;
        this.cg.alpha = 0;
    }

    private void CreateBookContent(BookBehaviour book)
    {
        JSONObject json = new JSONObject(WwwContent.text);
        WwwContent = null;
        JSONObject result = json["Content"];
        book.Preview.HtmlContent = result.str.Replace("\\\"", "'");
        book.Preview.HtmlContent = Regex.Replace(book.Preview.HtmlContent, @"<br[^\<]*>", " ");
        //selectedResult.HtmlContent = Regex.Replace(selectedResult.HtmlContent, @"<b[^\<]*>", " ");
        book.Preview.HtmlContent = book.Preview.HtmlContent.Trim('\r', '\n');
        book.Preview.Content = new Article(book.Preview.Name, Parser.ConcatParagraph(Parser.Parse(book.Preview.HtmlContent)));

        List<Part> parts = new List<Part>();

        foreach (var p in book.Preview.Content.parts)
        {
            if (p is Part)
            {
                Part pp = (Part)p;

                if (pp.name.Length > 0)
                    parts.Add(pp);
            }
        }
        book.Preview.Content.realParts = parts;
        //First the room is created
        //museumDoor.GetComponent<RoomUnlocker>().CreateRoom(selectedResult);
        //libraryDoor.GetComponent<RoomUnlocker>().CreateLibraryRoom(resultPreviews);

        CreateSummary(book);

        loadingBar.StopLoadEvent();
        this.loaded = true;

        previousButton.gameObject.SetActive(true);
        nextButton.gameObject.SetActive(true);
    }

    private void CreateSummary(BookBehaviour book)
    {
        summaryButtons = new List<List<Button>>();

        List<Button> buttons = new List<Button>();
        for (int i = 0; i < book.Preview.Content.realParts.Count && i < 2; i++)
        {
            for (int j = 0; (i * j) < book.Preview.Content.realParts.Count && j < 4; j++)
            {
                Part tp = book.Preview.Content.realParts[j + 4 * i];

                Button b = Instantiate<Button>(buttonPrefab);

                b.transform.SetParent(gameObject.transform);

                float yy = 60 - ((60 + b.transform.localScale.y) * j);
                float xx = -90 + ((158 + 30) * i);

                b.transform.localPosition = new Vector3(xx, yy, 0);

                b.transform.GetChild(0).GetComponent<Text>().text = tp.name;

                int temp = j + 4 * i;
                b.onClick.AddListener(() => { DisplaySummaryPage(book, 0); DisplayPage(book, temp); });
                Debug.Log("thiiis: " + (j + 4 * i));

                b.gameObject.SetActive(false);
                buttons.Add(b);
            }
        }
        summaryButtons.Add(buttons);
    }

    public void DisplaySummaryPage(BookBehaviour book, int index)
    {
        bool status = content.gameObject.activeSelf;

        previousButton.gameObject.SetActive(!status);
        nextButton.gameObject.SetActive(!status);
        content.gameObject.SetActive(!status);

        if (status)
            pageTitle.text = "Summary";

        foreach (var b in summaryButtons[index])
        {
            b.gameObject.SetActive(status);
        }

        if (!status)
            DisplayPage(book, currentPage);
        
    }

    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;

        // check for errors
        if (www.error == null)
        {
            Debug.Log("WWW Ok!: " + www.text);
        }
        else
        {
            Debug.Log("WWW Error: " + www.error);
        }
    }

    public void RequestContent(long resultId)
    {
        if (WwwContent == null)
        {
            this.loaded = false;
            previousButton.gameObject.SetActive(false);
            nextButton.gameObject.SetActive(false);
            loadingBar.StartInfiniteLoadEvent();

            //descriptionWindow.SetActive(false);
            gameObject.SetActive(true);

            string url = "localhost:60599/api/resourceapi/get/" + resultId;
            WwwContent = new WWW(url);

            StartCoroutine(WaitForRequest(WwwContent));
        }
    }

    void DisplayPage(BookBehaviour book, int i)
    {
        if (!content.gameObject.activeSelf)
            content.gameObject.SetActive(true);

        Debug.Log("index: " + i);
        if (i < book.Preview.Content.realParts.Count)
        {
            currentPage = i;
            if (i == 0)
                previousButton.gameObject.SetActive(false);
            else
                previousButton.gameObject.SetActive(true);

            if (i == (book.Preview.Content.realParts.Count - 1))
                nextButton.gameObject.SetActive(false);
            else
                nextButton.gameObject.SetActive(true);

            Content ccontent = book.Preview.Content.realParts[i];

            pageTitle.text = ((Part)ccontent).name;
            pageContent.text = ((Part)ccontent).ToStringWoName();

            this.loaded = true;
        }
    }
}
