﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.DataModels;
using System.Collections.Generic;

public class BookShelfBehaviour : MonoBehaviour {

    public GameObject bookPrefab;

    private Vector3[] booksPos = {
                                    new Vector3(-0.092f, -0.101f, -0.222f),
                                    new Vector3(-0.090f, -0.108f, 0.0040f),
                                    new Vector3(-0.092f, -0.103f, 0.212f),
                                    new Vector3(-0.158f, -0.103f, -0.220f),
                                    new Vector3(-0.157f, -0.111f, 0.0041f),
                                    new Vector3(-0.157f, -0.106f, 0.212f),
                                    new Vector3(-0.092f, -0.020f, 0.2224f),
                                    new Vector3(-0.092f, -0.022f, 0.0010f),
                                    new Vector3(-0.092f, -0.038f, -0.218f),
                                    new Vector3(-0.157f, -0.020f, 0.2224f),
                                    new Vector3(-0.158f, -0.023f, 0.0016f),
                                    new Vector3(-0.156f, -0.033f, -0.218f)
                                 };
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void GenerateBooks(List<Preview> bbl)
    {
        for (int i = 0; i < booksPos.Length; i++)
        {
            GameObject bok = Instantiate(bookPrefab);
            bok.transform.parent = gameObject.transform;
            bok.transform.localPosition = booksPos[i];

            bok.GetComponent<BookBehaviour>().Preview = (i < bbl.Count) ? bbl[i] : null;

            string tag = "Book";
            if (bok.GetComponent<BookBehaviour>().Preview != null)
                tag += "_" + bok.GetComponent<BookBehaviour>().Preview.Id;

            bok.name = tag;
        }       
    }
}
