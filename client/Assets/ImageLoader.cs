﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ImageLoader : MonoBehaviour {
    public string url;

    private Sprite image;

	// Use this for initialization
	void Start () {
        if (url == null)
            return;

        StartCoroutine(DownloadImage());
	}

    IEnumerator DownloadImage()
    {
        WWW www = new WWW(url);
        yield return www;
        image = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0.5f, 0.5f));

        MyFunctionToFix();
    }

    //FIXME
    void MyFunctionToFix()
    {
        //FIXME
    }
}
