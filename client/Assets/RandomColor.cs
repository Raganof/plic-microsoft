﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RandomColor : MonoBehaviour {
    public List<Color> colors;
    public Material[] materials;
	// Use this for initialization
	void Start () {

        colors.Add(new Color(0.67f, 0.125f, 0.125f));
        colors.Add(new Color(0.67f, 0.23f, 0.125f));
        colors.Add(new Color(0.337f, 0.514f, 0.867f));

        int randomIndex = Random.Range(0, colors.Count);
        gameObject.transform.GetChild(0).GetComponent<MeshRenderer>().material.color = colors[randomIndex];
        int randomIndex2 = Random.Range(0, materials.Length);

        Debug.Log("index: " + materials[randomIndex2].name);
        //gameObject.transform.GetChild(0).GetComponent<MeshRenderer>().materials[1] = materials[randomIndex2];
        Debug.Log("fname: " + gameObject.transform.GetChild(0).GetComponent<MeshRenderer>().materials[1].name);
        gameObject.transform.GetChild(0).GetComponent<MeshRenderer>().materials[1].color = colors[randomIndex];

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
