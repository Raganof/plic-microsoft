using System;
using System.Collections.Generic;

namespace Assets.Scripts.DataModels
{
    public class Article
    {
        public string title { get; set; }
        public List<Content> parts { get; set; }

        public List<Part> realParts { get; set; }

        public Article(string title, List<Content> list)
        {
            this.title = title;
            this.parts = list;
        }

        public override string ToString()
        {
            string str = title + System.Environment.NewLine + System.Environment.NewLine + System.Environment.NewLine;
            foreach (Content part in parts)
            {
                str += part;
            }
            return str;
        }

        public int GetSizeMuseum()
        {
            int size = 0;
            foreach (int sizePart in GetSizesPart())
                size += sizePart;

            return size;
        }

        public List<float> GetSizesPart()
        {
            List<float> sizes = new List<float>();
            float size = 0;
            foreach (Content part in parts)
            {
                if (part is Part && ((Part) part).ContainsPart())
                {
                    size = size + 2;
                    size = (float)(Math.Ceiling(size / 1.4f));
                    sizes.Add(size);
                    size = part.GetSizeMuseum() + 1;
                }
                else
                {
                    size += part.GetSizeMuseum();
                }
            }
            sizes.Add(size);
            return sizes;
        }
    }
}