namespace Assets.Scripts.DataModels
{
    public abstract class Content
    {
        public static readonly int lenPerUnity = 500;
        public static readonly int lenPerLine = 75;
        public static readonly int nbLinePerUnity = 12;

        public abstract float GetSizeMuseum();

        public abstract int GetNbLine();
    }
}