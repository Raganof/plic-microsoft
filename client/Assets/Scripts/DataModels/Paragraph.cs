using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Assets.Scripts.DataModels
{
    public class Paragraph : Content
    {
        public string content { get; set; }

        private float size = 0;
        private int nbLine = 0;

        public override string ToString()
        {
            return content + System.Environment.NewLine;
        }

        public override float GetSizeMuseum()
        {
            if (size != 0)
                return size;

            string[] words = Regex.Split(content, " ");
            int nbLine = 0;
            int currentLengthOfLine = 0;
            foreach (string word in words)
            {
                currentLengthOfLine += currentLengthOfLine > 0 ? 1 : 0;
                currentLengthOfLine += word.Length;
                if (currentLengthOfLine > lenPerLine)
                {
                    nbLine += 1;
                    currentLengthOfLine = word.Length;
                }
            }
            nbLine += currentLengthOfLine > 0 ? 1 : 0;
            size = (float) ((float)nbLine / (float)nbLinePerUnity);

            return size;
        }

        public override int GetNbLine()
        {
            if (nbLine != 0)
                return nbLine;

            string[] words = Regex.Split(content, " ");
            nbLine = 0;
            int currentLengthOfLine = 0;
            foreach (string word in words)
            {
                currentLengthOfLine += currentLengthOfLine > 0 ? 1 : 0;
                currentLengthOfLine += word.Length;
                if (currentLengthOfLine > lenPerLine)
                {
                    nbLine += 1;
                    currentLengthOfLine = word.Length;
                }
            }
            nbLine += currentLengthOfLine > 0 ? 1 : 0;
			size = (float)((float)nbLine / (float)nbLinePerUnity);

            return nbLine;
        }

        //Met un espace la ou il en manque et les supprime la ou il y en a trop
        public void fixContent()
        {
            content = content.Trim();
            content = content.Replace(",", ", ");
            content = content.Replace(".", ". ");
            content = content.Replace("!", "! ");
            content = content.Replace("?", "? ");
            content = content.Replace(";", "; ");
            content = content.Replace(":", ": ");
            content = content.Replace("  ", " ");           
        }

        public List<Paragraph> CutInUnit(int firstContentNbLines)
        {
            List<Paragraph> paragraphs = new List<Paragraph>();
            string[] sentences = Regex.Split(content, @"(?<=[\.!\?])\s+");

            string currentParagraph = "";
            foreach (string sentence in sentences)
            {
                Paragraph paragraph = new Paragraph() { content = currentParagraph + " " + sentence };
                if (paragraph.GetNbLine() > firstContentNbLines)
                {
                    paragraphs.Add(new Paragraph() { content = currentParagraph });
                    currentParagraph = sentence;
                    firstContentNbLines = nbLinePerUnity;
                }
                else
                {
                    if (currentParagraph.Length != 0)
                        currentParagraph += " ";
                    currentParagraph += sentence;
                }
            }

            return paragraphs;
        }
    }
}