using System;
using System.Collections.Generic;

namespace Assets.Scripts.DataModels
{
    public class Part : Content
    {
        public string name { get; set; }
        public List<Content> subParts { get; set; }

        private float size = 0;

        public override string ToString()
        {
            string str = System.Environment.NewLine + name + System.Environment.NewLine + System.Environment.NewLine;
            foreach (Content content in subParts)
            {
                str += content;
            }
            return str;
        }

        public string ToStringWoName()
        {
            string str = "";
            foreach (Content content in subParts)
            {
                str += content;
            }
            return str;
        }

        public float GetSimpleSize()
        {
            float size = 0f;
            foreach (Content content in CutInSimpleParts())
                size += content.GetSizeMuseum();
            size = (float) Math.Ceiling(size);
            return size;
        }

        public override float GetSizeMuseum()
        {
            if (size != 0)
                return size;

            size = (float) (2.0f / nbLinePerUnity);
            foreach (Content content in subParts)
                size += content.GetSizeMuseum();
            size = (float) Math.Ceiling(size);

            return size;
        }

        public override int GetNbLine()
        {
            int nbLine = 2;
            foreach (Content subPart in subParts)
                nbLine += subPart.GetNbLine();
            return nbLine;
        }

        public bool IsContentText()
        {
            foreach (Content content in subParts)
                if (content is Part)
                    return false;
            return true;
        }

        public bool ContainsPart()
        {
            foreach (var subPart in subParts)
                if (subPart is Part)
                    return true;
            return false;
        }

        public string GetContent()
        {
            string str = "";
            foreach (Content content in subParts)
                str += content.ToString() + System.Environment.NewLine;
            return str;
        }

        public List<List<Content>> CutInParts(int nbParts, int type = 2)
        {
			switch (type)
			{
			case 3:
				return CutInPartsType3(nbParts);
			default:
				return CutInPartsEquals(nbParts);
			}
            
        }

		public List<List<Content>> CutInPartsEquals(int nbParts)
		{
			List<List<Content>> listContentCut = new List<List<Content>>();
			List<Content> simpleContents = CutInSimpleParts ();
			float size = simpleContents.Count;
			float requiredSize = size /  (float)nbParts;
			float percentageError = (size - requiredSize * (float)nbParts) / (float) nbParts;
			//For float precision we had a little to be sure
			percentageError += percentageError / 100f;
			percentageError += 1f;
			
			List<Content> contentCut = new List<Content>();
			float currentSize = 0;
			foreach (Content content in simpleContents)
			{
				float sizeMuseum = 1;
				currentSize += sizeMuseum;
				if (currentSize > requiredSize * percentageError)
				{
					listContentCut.Add(contentCut);
					contentCut = new List<Content>();
					contentCut.Add(content);
					currentSize = sizeMuseum;
				}
				else
				{
					contentCut.Add(content);
				}
			}
			if (contentCut.Count > 0)
				listContentCut.Add(contentCut);
			
			return listContentCut;
		}

		public List<List<Content>> CutInPartsType3(int nbParts)
		{
			List<List<Content>> listContentCut = new List<List<Content>>();
			List<Content> simpleContents = CutInSimpleParts ();
			float size = simpleContents.Count;;
			float requiredSize = size /  (float)nbParts;
			float percentageError = 1f;
			int firstPartSize = 0;

			List<Content> contentCut = new List<Content>();
			float currentSize = 0;
			int part = 0;
			foreach (Content content in simpleContents) {
				if (part == 0)
				{
					contentCut.Add(content);
					if (content is Paragraph)
					{
						listContentCut.Add(contentCut);
						firstPartSize = contentCut.Count;
						requiredSize = (size - firstPartSize * (nbParts / 4)) / ((int) (nbParts * 0.75f));
						contentCut = new List<Content>();
						part++;

						percentageError = (size - firstPartSize * (nbParts / 4) - requiredSize * (float) (nbParts * 0.75f)) / (float) (nbParts * 0.75f);
						//For float precision we had a little to be sure
						percentageError += percentageError / 100f;
						percentageError += 1f;
					}
				} else if (part % 4 == 0) {
					float sizeMuseum = 1;
					currentSize += sizeMuseum;
					if (currentSize > firstPartSize)
					{
						listContentCut.Add(contentCut);
						contentCut = new List<Content>();
						contentCut.Add(content);
						currentSize = sizeMuseum;
					}
					else
					{
						contentCut.Add(content);
					}
				} else {
					float sizeMuseum = 1;
					currentSize += sizeMuseum;
					if (currentSize > requiredSize * percentageError)
					{
						listContentCut.Add(contentCut);
						contentCut = new List<Content>();
						contentCut.Add(content);
						currentSize = sizeMuseum;
					}
					else
					{
						contentCut.Add(content);
					}
				}
			}

			return listContentCut;
		}

        public List<Content> CutInSimpleParts(int firstContentLines = 12)
        {
            List<Content> simpleSubParts = new List<Content>();
            foreach (Content content in subParts)
            {
                if (content is Paragraph)
                {
                    //Cut it if too big
                    if (content.GetSizeMuseum() > firstContentLines)
                    {
                        Paragraph paragraph = (Paragraph)content;
                        List<Paragraph> paragraphs = paragraph.CutInUnit(firstContentLines);
                        foreach (Paragraph p in paragraphs)
                            simpleSubParts.Add(p);
                    }
                    else
                       simpleSubParts.Add(content);
                }
                else
                {
                    Part subPart = (Part) content;
                    List<Content> contents = subPart.CutInSimpleParts(nbLinePerUnity - 2);
					if (contents[0] is Paragraph)
                    	contents[0] = new SimplePart() { name = subPart.name, content = (Paragraph)contents[0] };
                    else
						contents[0] = new SimplePart() { name = subPart.name + " - " + ((SimplePart)contents[0]).name, content = ((SimplePart)contents[0]).content };
					contents.RemoveAt(0);

					foreach (Content c in contents)
                        simpleSubParts.Add(c);
                }

                firstContentLines = nbLinePerUnity;
            }

            return simpleSubParts;
        }
    }

}
