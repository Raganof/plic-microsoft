﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DataModels
{
    public class Preview
    {
        public long Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public String Img { get; set; }
        public String HtmlContent { get; set; }
        public Article Content { get; set; }

        override
        public String ToString()
        {
            return "Page Id: " + Id + System.Environment.NewLine
                   + "Title: " + Name + System.Environment.NewLine
                   + "Description: " + System.Environment.NewLine
                   + Description; 
        }
    }
}
