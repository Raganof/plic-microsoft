﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.DataModels
{
    class SimplePart : Content
    {
        public string name { get; set; }
        public Paragraph content { get; set; }

        private float size = 0;

        public override int GetNbLine()
        {
            int nbLine = 2;
            nbLine += content.GetNbLine();
            return nbLine;
        }

        public override float GetSizeMuseum()
        {
            if (size != 0)
                return size;

            size = (float) (2f / nbLinePerUnity);
            size += content.GetSizeMuseum();

            return size;
        }
    }
}
