﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.DataModels;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

public class GenerateLibraryRoom : MonoBehaviour
{
    public Transform doorPrefab;
    public Transform wallPrefab;
    public Transform groundPrefab;
    public Transform roofPrefab;
    public Transform corridorPrefab;

    public Transform treePrefab;
    public Transform plantPrefab;

    public Transform bookShelfPrefab;
    public Transform bookPrefab;

    public Transform simpleCanvasPrefab;
    public Transform partCanvasPrefab;
    public Transform titlePrefab;

    private float width;
    private float height;
    private float wallWidth;

    private float space = 1.5f;
    private int xSizu = 5;
    private int articlesByRoom = 2;

    public void Awake()
    {
        Renderer renderer = groundPrefab.GetComponent<Renderer>();
        //width = renderer.bounds.size.x;
        width = renderer.bounds.size.z;

        renderer = wallPrefab.GetComponent<Renderer>();
        wallWidth = renderer.bounds.size.x;
        height = renderer.bounds.size.y;
    }

    public void CreateRoom(GameObject otherDoor, List<Preview> previews)
    {
        int ySize = 3;
        Debug.Log("Size: " + xSizu);

        int partsNum = (previews.Count - 1) / articlesByRoom + 1;
        int xSize = partsNum * xSizu;


        //Then call the CreateRoom function
        GameObject room = CreateRoom(otherDoor, "LibraryRoom", previews, xSize, ySize);
        //We add separation between parts
        CreateSeparation(room, partsNum, previews, ySize);
    }

    public GameObject CreateRoom(GameObject otherDoor, string roomName, List<Preview> previews, int xSize, int zSize)
    {
        // First we create the entire room
        GameObject room = new GameObject(roomName); //The Game Object containing the entire room

        GameObject ground = new GameObject("Ground"); // The GameObject containing the entire ground
        GameObject roof = new GameObject("Roof"); // Same as ground but for roof and with lights
        GameObject wall = new GameObject("Wall"); // The GameObject containg walls and doors
        GameObject corridor = Instantiate(corridorPrefab).gameObject;

        ground.transform.parent = room.transform; //Put ground in room
        roof.transform.parent = room.transform; //Put roof in room
        wall.transform.parent = room.transform; //Put wall in room
        corridor.transform.parent = room.transform; //Put corridor in room

        CreateGroundAndroof(ground, roof, xSize, zSize);
        CreateWall(wall, previews, xSize, zSize);

        float lenCorridor = width * 3;
        corridor.transform.localEulerAngles = new Vector3(0, 180f, 0);
        corridor.transform.localPosition = new Vector3(-width, 0, width);

        //Now We have to link the other room to the new one
        float yRotation = (otherDoor.transform.rotation.eulerAngles.y + 360) % 360;
        room.transform.Rotate(0, yRotation + 180, 0);
        float x = otherDoor.transform.position.x;
        float y = otherDoor.transform.position.y;
        float z = otherDoor.transform.position.z;

        if (yRotation - yRotation % 1 == 0)
        {
            x -= (width / 2 + wallWidth / 3) + lenCorridor;
            z += (zSize - 1) * width / 2;
        }
        else if (yRotation - yRotation % 1 == 90)
        {
            x += (zSize - 1) * width / 2;
            z += width / 2 + wallWidth / 3 + lenCorridor;
        }
        else if (yRotation - yRotation % 1 == 180)
        {
            x += width / 2 + wallWidth / 3 + lenCorridor;
            z -= (zSize - 1) * width / 2;
        }
        else
        {
            x -= (zSize - 1) * width / 2;
            z -= (width / 2 + wallWidth / 3) + lenCorridor;
        }
        room.transform.position = new Vector3(x, y, z);

        return room;
    }

    private void CreateGroundAndroof(GameObject ground, GameObject roof, int xSize, int zSize)
    {
        // We create a room of xSize * zSize
        for (int i = 0; i < xSize; i++)
        {
            for (int j = 0; j < zSize; j++)
            {
                Transform groundPiece = Instantiate(groundPrefab);
                //Transform roofPiece = Instantiate(roofPrefab);

                groundPiece.parent = ground.transform;
                //roofPiece.parent = roof.transform;

                groundPiece.localPosition = new Vector3(i * width, 0, j * width);
                //roofPiece.localPosition = new Vector3(i * width, height, j * width);

            }
        }
    }

    private void CreateWall(GameObject wall, List<Preview> previews, int xSize, int zSize)
    {
        // We create a room of 3 * 4;
        for (int i = 0; i < xSize; i++)
        {
            for (int j = 0; j < zSize; j++)
            {
                // Check if we are not on the other door (This is where we will link the room)
                if (i == 0 && j == (zSize / 2))
                {
                    //Add a new door
                    Transform enterDoor = Instantiate(doorPrefab);
                    enterDoor.parent = wall.transform;
                    enterDoor.localPosition = new Vector3(-width / 2, 0, j * width);

                    GenerateBookShelf(enterDoor, previews.GetRange(0, 9), true);
                }
                else if (i == 0)
                {
                    Transform wallPiece = Instantiate(wallPrefab);
                    wallPiece.parent = wall.transform;
                    wallPiece.localPosition = new Vector3(-width / 2, 0, j * width);
                }
                else if (i == xSize - 1)
                {
                    Transform wallPiece = Instantiate(wallPrefab);
                    wallPiece.parent = wall.transform;
                    wallPiece.rotation = Quaternion.Euler(new Vector3(wallPiece.eulerAngles.x, wallPiece.eulerAngles.y + 180, wallPiece.eulerAngles.z));
                    wallPiece.localPosition = new Vector3(i * width + width / 2, 0, j * width);
                }
                if (j == 0)
                {
                    Transform wallPiece = Instantiate(wallPrefab);
                    wallPiece.parent = wall.transform;
                    wallPiece.rotation = Quaternion.Euler(new Vector3(wallPiece.eulerAngles.x, wallPiece.eulerAngles.y - 90, wallPiece.eulerAngles.z));
                    wallPiece.localPosition = new Vector3(i * width, 0, -width / 2);
                }
                if (j == zSize - 1)
                {
                    Transform wallPiece = Instantiate(wallPrefab);
                    wallPiece.parent = wall.transform;
                    wallPiece.rotation = Quaternion.Euler(new Vector3(wallPiece.eulerAngles.x, wallPiece.eulerAngles.y + 90, wallPiece.eulerAngles.z));
                    wallPiece.localPosition = new Vector3(i * width, 0, j * width + width / 2);
                }
            }
        }
    }

    private void CreateSeparation(GameObject room, int partsNum, List<Preview> previews, int ySize)
    {
        GameObject seperation = new GameObject("Separator");
        seperation.transform.parent = room.transform;
        seperation.transform.localPosition = new Vector3();
        seperation.transform.localEulerAngles = new Vector3();

        float xPosition = 0;
        for (int index = 0; index < partsNum - 1; ++index)
        {
            int size = this.xSizu;
            float zPosition = 0;
            xPosition += size * width + 0.05f;
            for (int i = 0; i < ySize; ++i)
            {
                Transform wall = Instantiate(wallPrefab);
                if (i != ySize / 2)
                {
                    wall.parent = seperation.transform;
                    wall.localPosition = new Vector3(xPosition - 0.01f, 0, zPosition);
                    wall.localEulerAngles = new Vector3(wall.localEulerAngles.x, wall.localEulerAngles.y + 90, wall.localEulerAngles.z);
                    wall = Instantiate(wallPrefab);
                    wall.parent = seperation.transform;
                    wall.localPosition = new Vector3(xPosition + 0.01f, 0, zPosition);
                    wall.localEulerAngles = new Vector3(wall.localEulerAngles.x, wall.localEulerAngles.y - 90, wall.localEulerAngles.z);
                }
                else
                {
                    //A wall where we gonna put the title of the part
                    wall.parent = seperation.transform;
                    wall.localPosition = new Vector3(xPosition + width, 0, zPosition);
                    wall.localEulerAngles = new Vector3(wall.localEulerAngles.x, wall.localEulerAngles.y + 90, wall.localEulerAngles.z);

                    GenerateBookShelf(wall, previews.GetRange(0, 9));
                }
                zPosition += width;
            }
        }
    }

    private void GenerateBookShelf(Transform tr, List<Preview> previews, bool isFirst = false)
    {
        Transform boo = Instantiate(bookShelfPrefab);
        boo.parent = tr;

        if (!isFirst)
            boo.transform.localPosition = new Vector3(-16f, 0f, 5f);
        else
        {
            boo.transform.localPosition = new Vector3(25f, 0f, 5f);
        }

        BookShelfBehaviour bsb = boo.GetComponent<BookShelfBehaviour>();
        bsb.GenerateBooks(previews);

        if (!isFirst)
            boo.transform.localEulerAngles = new Vector3(tr.localEulerAngles.x, tr.localEulerAngles.y + 180, tr.localEulerAngles.z - 90);
        else
        {
            boo.transform.localEulerAngles = new Vector3(tr.localEulerAngles.x, tr.localEulerAngles.y, tr.localEulerAngles.z - 90);

            Transform tree = Instantiate(treePrefab);
            tree.parent = boo;
            tree.localPosition = new Vector3(-0.649f, 0.701f, -0.462f);
            tree.localEulerAngles = new Vector3(6.41f, 92.78f, 271.95f);
        }
    }
}

