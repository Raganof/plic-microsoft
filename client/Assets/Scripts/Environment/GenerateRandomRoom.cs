﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.DataModels;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using Assets.Scripts.Environment.RandomRoom;

namespace Assets.Scripts.Environment
{
    class GenerateRandomRoom : MonoBehaviour
    {
        public Transform doorPrefab;
        public Transform wallPrefab;
        public Transform groundPrefab;

        public Transform textPrefab;
        public Transform partTextPrefab;
        public Transform titlePrefab;
        public Transform imagePrefab;

        public Transform cameraPrefab;

        private float width;
        private float height;
        private float wallWidth;

        private float space = 1.5f;

        private List<GenerateRoomAbstract> generators;

        private const int CORRIDOR_LEFT = 1;
        private const int CORRIDOR_RIGHT = 2;
        private const int CORRIDOR_CENTER = 3;

        private GameObject currentMuseum;
        private int currentIndex;
        private List<Content> currentContents;
        private String currentArticleTitle;
        private Queue<ImageGetter> currentImageGetters;
        private GameObject currentExitDoor;
        private bool currentSide;

        private bool isLock;

        public void Awake()
        {
            Renderer renderer = groundPrefab.GetComponent<Renderer>();
            //width = renderer.bounds.size.x;
            width = renderer.bounds.size.z;
            
            renderer = wallPrefab.GetComponent<Renderer>();
            wallWidth = renderer.bounds.size.x;
            height = renderer.bounds.size.y;

            generators = new List<GenerateRoomAbstract>();
            generators.Add(GenerateRoomSmall.CreateInstance<GenerateRoomSmall>().Init(doorPrefab, wallPrefab, groundPrefab, textPrefab, partTextPrefab, titlePrefab, imagePrefab, cameraPrefab));
            generators.Add(GenerateRoomInC.CreateInstance<GenerateRoomInC>().Init(doorPrefab, wallPrefab, groundPrefab, textPrefab, partTextPrefab, titlePrefab, imagePrefab, cameraPrefab));
            //generators.Add(new GenerateRoomInS(doorPrefab, wallPrefab, groundPrefab, textPrefab, partTextPrefab, titlePrefab, imagePrefab, cameraPrefab));

            currentImageGetters = new Queue<ImageGetter>();
        }

        public GameObject GenerateMuseum(GameObject otherDoor, Article article)
        {
            currentMuseum = new GameObject(article.title);
            
            StartCoroutine(GenerateMuseumInBackground(otherDoor, article));

            return currentMuseum;
        }

		private IEnumerator GenerateMuseumInBackground(GameObject otherDoor, Article article)
		{
			yield return new WaitForEndOfFrame ();

            currentArticleTitle = article.title;

            List<Content> introduction = new List<Content>();
			while (article.parts[0] is Paragraph)
			{
				Content content = article.parts[0];
				introduction.Add(content);
				article.parts.Remove(content);
			}
			Part introPart = new Part() { name = "Introduction", subParts = introduction };
			article.parts.Insert(0, introPart);
			
			currentImageGetters.Enqueue(ImageGetter.CreateInstance<ImageGetter>().Init(article.title));
			foreach (Content content in article.parts)
			{
				if (content is Part)
				{
					currentImageGetters.Enqueue(ImageGetter.CreateInstance<ImageGetter>().Init(currentArticleTitle + " " + ((Part)content).name));
				}
			}
			
			currentIndex = 0;
			currentContents = article.parts;
			
			GenerateCorridor(otherDoor, CORRIDOR_LEFT);

			yield return new WaitForEndOfFrame();

			while (currentIndex < currentContents.Count)
			{
				if (currentContents != null && !isLock)
				{
					isLock = true;
					if (currentIndex >= currentContents.Count)
                    {
                        currentContents = null;
                        currentArticleTitle = null;
                        currentImageGetters.Clear();
                        currentIndex = 0;
                    }
                    else
                    {
                        Content content = currentContents[currentIndex];
                        currentIndex++;
                        if (content is Part)
                        {
                            ImageGetter imageGetter = currentImageGetters.Dequeue();
                            Part part = content as Part;

                            if (part.name == "References" || part.name == "External links")
                                break;

                            GameObject room = GenerateRoom(currentExitDoor, part, imageGetter, currentSide);
                            room.transform.SetParent(currentMuseum.transform);

                            try
                            {
                                currentExitDoor = room.transform.FindChild("Wall").FindChild("ExitDoor").gameObject;
                            }
                            catch (NullReferenceException)
                            {
                                currentExitDoor = room.transform.FindChild("Wall").FindChild("End Part Wall").FindChild("ExitDoor").gameObject;
                            }
                            currentSide = !currentSide;
                        }
                    }
                    isLock = false;
                }
            }
        }

        public GameObject GenerateRoom(GameObject otherDoor, Part part, ImageGetter imageGetter, bool leftSide = false)
        {
            List<GenerateRoomAbstract> validGenerators = new List<GenerateRoomAbstract>();
            foreach (GenerateRoomAbstract generator in generators)
                if (generator.IsValid(part))
                    validGenerators.Add(generator);

            System.Random random = new System.Random();
            int index = random.Next() % validGenerators.Count;
            return validGenerators[index].CreateRoom(otherDoor, part, imageGetter, leftSide);
        }

        private GameObject GenerateCorridor(GameObject otherDoor, int type, String name = "")
        {
            GameObject corridor = name.Length == 0 ? new GameObject("Corridor") : new GameObject("Corridor " + name);

            Transform wallInstance = Instantiate(wallPrefab);
            wallInstance.SetParent(corridor.transform);
            wallInstance.localPosition = new Vector3(0.5f * width, 0, -0.5f * width);
            wallInstance.localRotation = Quaternion.Euler(wallInstance.localEulerAngles.x, wallInstance.localEulerAngles.y - 90, wallInstance.localEulerAngles.z);
            wallInstance = Instantiate(wallPrefab);
            wallInstance.SetParent(corridor.transform);
            wallInstance.localPosition = new Vector3(0.5f * width, 0, 0.5f * width);
            wallInstance.localRotation = Quaternion.Euler(wallInstance.localEulerAngles.x, wallInstance.localEulerAngles.y + 90, wallInstance.localEulerAngles.z);
            Transform groundInstance = Instantiate(groundPrefab);
            groundInstance.SetParent(corridor.transform);
            groundInstance.localPosition = new Vector3(0.5f * width, -height / 2, 0);

            switch (type)
            {
                case CORRIDOR_CENTER:
                    wallInstance = Instantiate(wallPrefab);
                    wallInstance.SetParent(corridor.transform);
                    wallInstance.localPosition = new Vector3(1.5f * width, 0, -0.5f * width);
                    wallInstance.localRotation = Quaternion.Euler(wallInstance.localEulerAngles.x, wallInstance.localEulerAngles.y - 90, wallInstance.localEulerAngles.z);
                    wallInstance = Instantiate(wallPrefab);
                    wallInstance.SetParent(corridor.transform);
                    wallInstance.localPosition = new Vector3(1.5f * width, 0, 0.5f * width);
                    wallInstance.localRotation = Quaternion.Euler(wallInstance.localEulerAngles.x, wallInstance.localEulerAngles.y + 90, wallInstance.localEulerAngles.z);

                    wallInstance = Instantiate(doorPrefab);
                    wallInstance.SetParent(corridor.transform);
                    wallInstance.tag = "ExitDoor";
                    wallInstance.Rotate(0, 180, 0);
                    wallInstance.localPosition = new Vector3(2, 0, 0);

                    groundInstance = Instantiate(groundPrefab);
                    groundInstance.SetParent(corridor.transform);
                    groundInstance.localPosition = new Vector3(1.5f * width, -height / 2, 0);

                    break;
                case CORRIDOR_LEFT:
                    wallInstance = Instantiate(wallPrefab);
                    wallInstance.SetParent(corridor.transform);
                    wallInstance.localPosition = new Vector3(1.5f * width, 0, -0.5f * width);
                    wallInstance.Rotate(0, 90, 0);
                    wallInstance = Instantiate(wallPrefab);
                    wallInstance.SetParent(corridor.transform);
                    wallInstance.localPosition = new Vector3(width, 0, width);
                    wallInstance = Instantiate(wallPrefab);
                    wallInstance.SetParent(corridor.transform);
                    wallInstance.localPosition = new Vector3(2 * width, 0, 0);
                    wallInstance.Rotate(0, 180, 0);
                    wallInstance = Instantiate(wallPrefab);
                    wallInstance.SetParent(corridor.transform);
                    wallInstance.localPosition = new Vector3(2 * width, 0, width);
                    wallInstance.Rotate(0, 180, 0);

                    groundInstance = Instantiate(groundPrefab);
                    groundInstance.SetParent(corridor.transform);
                    groundInstance.localPosition = new Vector3(1.5f * width, -height / 2, 0);
                    groundInstance = Instantiate(groundPrefab);
                    groundInstance.SetParent(corridor.transform);
                    groundInstance.localPosition = new Vector3(1.5f * width, -height / 2, width);

                    wallInstance = Instantiate(doorPrefab);
                    wallInstance.SetParent(corridor.transform);
                    wallInstance.tag = "ExitDoor";
                    wallInstance.Rotate(0, 90, 0);
                    wallInstance.localPosition = new Vector3(1.5f * width, 0, 1.5f * width);
                    break;
                case CORRIDOR_RIGHT:
                    wallInstance = Instantiate(wallPrefab);
                    wallInstance.SetParent(corridor.transform);
                    wallInstance.localPosition = new Vector3(1.5f * width, 0, 0.5f * width);
                    wallInstance.Rotate(0, 90, 0);
                    wallInstance = Instantiate(wallPrefab);
                    wallInstance.SetParent(corridor.transform);
                    wallInstance.localPosition = new Vector3(width, 0, -width);
                    wallInstance = Instantiate(wallPrefab);
                    wallInstance.SetParent(corridor.transform);
                    wallInstance.localPosition = new Vector3(2 * width, 0, 0);
                    wallInstance.Rotate(0, 180, 0);
                    wallInstance = Instantiate(wallPrefab);
                    wallInstance.SetParent(corridor.transform);
                    wallInstance.localPosition = new Vector3(2 * width, 0, -width);
                    wallInstance.Rotate(0, 180, 0);

                    groundInstance = Instantiate(groundPrefab);
                    groundInstance.SetParent(corridor.transform);
                    groundInstance.localPosition = new Vector3(1.5f * width, -height / 2, 0);
                    groundInstance = Instantiate(groundPrefab);
                    groundInstance.SetParent(corridor.transform);
                    groundInstance.localPosition = new Vector3(1.5f * width, -height / 2, -width);

                    wallInstance = Instantiate(doorPrefab);
                    wallInstance.SetParent(corridor.transform);
                    wallInstance.tag = "ExitDoor";
                    wallInstance.Rotate(0, -90, 0);
                    wallInstance.localPosition = new Vector3(1.5f * width, 0, 1.5f * width);
                    break;
            }

            currentExitDoor = wallInstance.gameObject;
            float yRotation = (otherDoor.transform.rotation.eulerAngles.y + 360) % 360;
            corridor.transform.Rotate(0, yRotation, 0);
            float x = otherDoor.transform.position.x;
            float y = otherDoor.transform.position.y;
            float z = otherDoor.transform.position.z;

            corridor.transform.position = new Vector3(x, y, z);

            return corridor;
        }
    }
}