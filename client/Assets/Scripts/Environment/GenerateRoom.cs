﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.DataModels;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using Assets.Scripts.Environment;

public class GenerateRoom : MonoBehaviour {
    private GenerateRandomRoom generateRandomRoom;

    public void Awake()
    {
        generateRandomRoom = GameObject.FindWithTag("GameController").GetComponent<GenerateRandomRoom>();
    }

    public void CreateRoom(GameObject otherDoor, Preview preview)
    {
        Article article = preview.Content;
        generateRandomRoom.GenerateMuseum(otherDoor, article);
        return;

/*        List<String> introductionStr = new List<String>();
        introductionStr = CutContent(introduction);

        float startX = width;
        float endX = startX;
        float startY = 0.24f;
        float leftZ = -(float) (ySize - Math.Ceiling((double)ySize / 2)) * width - width + wallWidth / 2;
        float rightZ = (float) (width * Math.Ceiling((double) ySize / 2)) - width - wallWidth / 2;

        int indexPart = 0;
        int j = 0;
        foreach (String paragraph in introductionStr)
        {
            //Initialize a new canvas containing only simple Text (No title)
            Transform textCanvas = Instantiate(simpleCanvasPrefab);
            //Setting the content
            textCanvas.GetComponentInChildren<Text>().text = paragraph;

            //Put into Text gameObject
            textCanvas.parent = text.transform;
            //Position the canvas in the museum
            textCanvas.localPosition = new Vector3(leftZ, startY, endX);
            //Rotate it correctly
            //textCanvas.localEulerAngles = new Vector3();
            endX += width * space;
            j += 1;
        }

        j = 0;
        bool left = false;
        while (i < article.parts.Count)
        {
            if (article.parts[i] is Part)
            {
                Part part = (Part)article.parts[i];
                if (part.ContainsPart())
                {
                    left = true;
                    
                    startX = titles[indexPart].parent.localPosition.x;
                    endX = startX;
                    titles[indexPart].GetComponentInChildren<Text>().text = part.name;
                    foreach (var subPart in part.subParts)
                    {
                        if (subPart is Part)
                            CreateSubPart(text, (Part)subPart, ref left, ref startX, ref endX, startY, leftZ, rightZ);
                        else
                        {
                            Transform textCanvas = Instantiate(simpleCanvasPrefab);
                            textCanvas.GetComponentInChildren<Text>().text = ((Paragraph)subPart).content;
                            textCanvas.parent = text.transform; 
                            if (left)
                            {
                                textCanvas.localPosition = new Vector3(leftZ, startY, endX);
                                endX += width * space;
                            }
                            else
                            {
                                textCanvas.localPosition = new Vector3(rightZ, startY, startX);
                                textCanvas.localEulerAngles = new Vector3(textCanvas.localEulerAngles.x, textCanvas.localEulerAngles.y + 180, textCanvas.localEulerAngles.z);
                                startX += width * space;
                            }
                            left = !left;
                        }
                    }
                    indexPart++;
                }
                else
                {
                    List<String> paragraphs = CutContent(part.subParts);
                    Transform simplePart = Instantiate(partCanvasPrefab);
                    simplePart.parent = text.transform;
                    //Set the title
                    simplePart.FindChild("Title").GetComponent<Text>().text = part.name;
                    //And the content
                    simplePart.FindChild("Text").GetComponent<Text>().text = paragraphs[0];

                    //Then place it correctly in the museum
                    float xRotation = simplePart.localEulerAngles.x;
                    float yRotation = left ? simplePart.localEulerAngles.y : simplePart.localEulerAngles.y + 180;
                    float zRotation = simplePart.localEulerAngles.z;
                    float z = left ? leftZ : rightZ;
                    if (left)
                    {
                        endX = endX + width * space;
                        simplePart.localPosition = new Vector3(leftZ, startY, endX);
                        endX += width * space;
                    
                        for (int k = 1; k < paragraphs.Count; ++k)
                        {
                            Transform textCanvas = Instantiate(simpleCanvasPrefab);
                            textCanvas.parent = text.transform;
                            textCanvas.GetComponentInChildren<Text>().text = paragraphs[k];
                            textCanvas.localPosition = new Vector3(z, startY, endX);
                            textCanvas.localEulerAngles = new Vector3(xRotation, yRotation, zRotation);
                            endX += width * space;
                        }
                    }
                    else
                    {
                        startX += width * space;
                        simplePart.localPosition = new Vector3(rightZ, startY, startX);
                        simplePart.localEulerAngles = new Vector3(simplePart.localEulerAngles.x, simplePart.localEulerAngles.y + 180, simplePart.localEulerAngles.z);
                        startX += width * space;

                        for (int k = 1; k < paragraphs.Count; ++k)
                        {
                            Transform textCanvas = Instantiate(simpleCanvasPrefab);
                            textCanvas.parent = text.transform;
                            textCanvas.GetComponentInChildren<Text>().text = paragraphs[k];
                            textCanvas.localPosition = new Vector3(z, startY, startX);
                            textCanvas.localEulerAngles = new Vector3(xRotation, yRotation, zRotation);
                            startX += width * space;
                        }
                    }
                    left = !left;
                }
            }
            else
            {
                Transform textCanvas = Instantiate(simpleCanvasPrefab);
                float z = left ? leftZ : rightZ;
                float xRotation = textCanvas.localEulerAngles.x;
                float yRotation = left ? textCanvas.localEulerAngles.y : textCanvas.localEulerAngles.y + 180;
                float zRotation = textCanvas.localEulerAngles.z;
                float x = left ? endX : startX;
                textCanvas.parent = text.transform;
                textCanvas.GetComponentInChildren<Text>().text = ((Paragraph)article.parts[i]).content;
                textCanvas.localPosition = new Vector3(z, startY, x);
                textCanvas.localEulerAngles = new Vector3(xRotation, yRotation, zRotation);
                if (left)
                    endX += width * space;
                else
                    startX += width * space;
            }
            ++i;
        }*/
    }
    /*
    private List<string> CutContent(List<Content> contents)
    {
        List<string> paragraphs = new List<string>();
        int nbLine = 0;
        string str = "";
        foreach (Content par in contents)
        {
            if (nbLine + par.GetNbLine() >= Content.nbLinePerUnity)
            {
                paragraphs.Add(str);
                str = "";
            }
            str += par.ToString();
            nbLine += par.GetNbLine();
        }
        paragraphs.Add(str);

        return paragraphs;
    }


    private void CreateSubPart(GameObject text, Part subPart, ref bool left, ref float startX, ref float endX, float startY, float leftZ, float rightZ)
    {
        if (subPart.ContainsPart())
        {
            foreach (var part in subPart.subParts)
            {
                if (part is Part)
                    CreateSubPart(text, (Part)part, ref left, ref startX, ref endX, startY, leftZ, rightZ);
                else
                {
                    Transform textCanvas = Instantiate(simpleCanvasPrefab);
                    textCanvas.GetComponentInChildren<Text>().text = ((Paragraph)part).content;
                    textCanvas.parent = text.transform;
                    if (left)
                    {
                        textCanvas.localPosition = new Vector3(leftZ, startY, endX);
                        endX += width * space;
                    }
                    else
                    {
                        textCanvas.localPosition = new Vector3(rightZ, startY, startX);
                        textCanvas.localEulerAngles = new Vector3(textCanvas.localEulerAngles.x, textCanvas.localEulerAngles.y + 180, textCanvas.localEulerAngles.z);
                        startX += width * space;
                    }
                    left = !left;
                }
            }
        }
        else
        {
            List<String> paragraphs = CutContent(subPart.subParts);
            Transform simplePart = Instantiate(partCanvasPrefab);
            simplePart.parent = text.transform;
            //Set the title
            simplePart.FindChild("Title").GetComponent<Text>().text = subPart.name;
            //And the content
            simplePart.FindChild("Text").GetComponent<Text>().text = paragraphs[0];

            //Then place it correctly in the museum
            float xRotation = simplePart.localEulerAngles.x;
            float yRotation = left ? simplePart.localEulerAngles.y : simplePart.localEulerAngles.y + 180;
            float zRotation = simplePart.localEulerAngles.z;
            float z = left ? leftZ : rightZ;
            if (left)
            {
                endX = endX + width;
                simplePart.localPosition = new Vector3(leftZ, startY, startX);
                endX += width * space;
                for (int k = 1; k < paragraphs.Count; ++k)
                {
                    Transform textCanvas = Instantiate(simpleCanvasPrefab);
                    textCanvas.parent = text.transform;
                    textCanvas.GetComponentInChildren<Text>().text = paragraphs[k];
                    textCanvas.localPosition = new Vector3(z, startY, endX);
                    textCanvas.localEulerAngles = new Vector3(xRotation, yRotation, zRotation);
                    endX += width * space;
                }
            }
            else
            {
                startX += width * space;
                simplePart.localPosition = new Vector3(rightZ, startY, startX);
                simplePart.localEulerAngles = new Vector3(simplePart.localEulerAngles.x, simplePart.localEulerAngles.y + 180, simplePart.localEulerAngles.z);
                startX += width * space;
                for (int k = 1; k < paragraphs.Count; ++k)
                {
                    Transform textCanvas = Instantiate(simpleCanvasPrefab);
                    textCanvas.parent = text.transform;
                    textCanvas.GetComponentInChildren<Text>().text = paragraphs[k];
                    textCanvas.localPosition = new Vector3(z, startY, startX);
                    textCanvas.localEulerAngles = new Vector3(xRotation, yRotation, zRotation);
                    startX += width * space;
                }
            }


            left = !left;
        }
    }

    public GameObject CreateRoom(GameObject otherDoor, string roomName, int xSize, int zSize)
    {
        // First we create the entire room
        GameObject room = new GameObject(roomName); //The Game Object containing the entire room

        GameObject ground = new GameObject("Ground"); // The GameObject containing the entire ground
        GameObject roof = new GameObject("Roof"); // Same as ground but for roof and with lights
        GameObject wall = new GameObject("Wall"); // The GameObject containg walls and doors
        GameObject corridor = Instantiate(corridorPrefab).gameObject;

        ground.transform.parent = room.transform; //Put ground in room
        roof.transform.parent = room.transform; //Put roof in room
        wall.transform.parent = room.transform; //Put wall in room
        corridor.transform.parent = room.transform; //Put corridor in room

        CreateGroundAndroof(ground, roof, xSize, zSize);
        CreateWall(wall, xSize, zSize);

        float lenCorridor = width * 3;
        corridor.transform.localEulerAngles = new Vector3(0, 180f, 0);
        corridor.transform.localPosition = new Vector3(-width, 0, width);

        //Now We have to link the other room to the new one
        float yRotation = (otherDoor.transform.rotation.eulerAngles.y + 360) % 360;
        room.transform.Rotate(0, yRotation + 180, 0);
        float x = otherDoor.transform.position.x;
        float y = otherDoor.transform.position.y;
        float z = otherDoor.transform.position.z;

        if (yRotation - yRotation % 1 == 0)
        {
            x -= (width / 2 + wallWidth / 3) + lenCorridor;
            z += (zSize - 1) * width / 2;
        }
        else if (yRotation - yRotation % 1 == 90)
        {
            x += (zSize - 1) * width / 2;
            z += width / 2 + wallWidth / 3 + lenCorridor;
        }
        else if (yRotation - yRotation % 1 == 180)
        {
            x += width / 2 + wallWidth / 3 + lenCorridor;
            z -= (zSize - 1) * width / 2;            
        }
        else
        {
            x -= (zSize - 1) * width / 2;
            z -= (width / 2 + wallWidth / 3) + lenCorridor;
        }
        room.transform.position = new Vector3(x, y, z);

        return room;
    }

    private void CreateGroundAndroof(GameObject ground, GameObject roof, int xSize, int zSize)
    {
        // We create a room of xSize * zSize
        for (int i = 0; i < xSize; i++)
        {
            for (int j = 0; j < zSize; j++)
            {
                Transform groundPiece = Instantiate(groundPrefab);
                //Transform roofPiece = Instantiate(roofPrefab);

                groundPiece.parent = ground.transform;
                //roofPiece.parent = roof.transform;

                groundPiece.localPosition = new Vector3(i * width, 0, j * width);
                //roofPiece.localPosition = new Vector3(i * width, height, j * width);
            }
        }
    }

    private void CreateWall(GameObject wall, int xSize, int zSize)
    {
        // We create a room of 3 * 4;
        for (int i = 0; i < xSize; i++)
        {
            for (int j = 0; j < zSize; j++)
            {
                // Check if we are not on the other door (This is where we will link the room)
                if (i == 0 && j == (zSize / 2))
                {
                    //Add a new door
                    Transform enterDoor = Instantiate(doorPrefab);
                    enterDoor.parent = wall.transform;
                    enterDoor.localPosition = new Vector3(-width / 2, 0, j * width);
                }
                else if (i == 0)
                {
                    Transform wallPiece = Instantiate(wallPrefab);
                    wallPiece.parent = wall.transform;
                    wallPiece.localPosition = new Vector3(-width / 2, 0, j * width);
                }
                else if (i == xSize - 1)
                {
                    Transform wallPiece = Instantiate(wallPrefab);
                    wallPiece.parent = wall.transform;
                    wallPiece.rotation = Quaternion.Euler(new Vector3(wallPiece.eulerAngles.x, wallPiece.eulerAngles.y + 180, wallPiece.eulerAngles.z));
                    wallPiece.localPosition = new Vector3(i * width + width / 2, 0, j * width);
                }
                if (j == 0)
                {
                    Transform wallPiece = Instantiate(wallPrefab);
                    wallPiece.parent = wall.transform;
                    wallPiece.rotation = Quaternion.Euler(new Vector3(wallPiece.eulerAngles.x, wallPiece.eulerAngles.y - 90, wallPiece.eulerAngles.z));
                    wallPiece.localPosition = new Vector3(i * width, 0, - width / 2);
                }
                if (j == zSize - 1)
                {
                    Transform wallPiece = Instantiate(wallPrefab);
                    wallPiece.parent = wall.transform;
                    wallPiece.rotation = Quaternion.Euler(new Vector3(wallPiece.eulerAngles.x, wallPiece.eulerAngles.y + 90, wallPiece.eulerAngles.z));
                    wallPiece.localPosition = new Vector3(i * width, 0, j * width + width / 2);
                }
            }
        }
    }

    private List<Transform> CreateSeparation(GameObject room, Article article, int ySize)
    {
        GameObject seperation = new GameObject("Separator");
        seperation.transform.parent = room.transform;
        seperation.transform.localPosition = new Vector3();
        seperation.transform.localEulerAngles = new Vector3();

        List<Transform> titles = new List<Transform>();
        float xPosition = 0;
        List<float> sizes = article.GetSizesPart();
        for (int index = 0; index < sizes.Count - 1; ++index)
        {
            float size = sizes[index];
            float zPosition = 0;
            xPosition += size * width + 0.05f;
            for (int i = 0; i < ySize; ++i)
            {
                if (i != ySize / 2)
                {
                    Transform wall = Instantiate(wallPrefab);
                    wall.parent = seperation.transform;
                    wall.localPosition = new Vector3(xPosition - 0.01f, 0, zPosition);
                    wall.localEulerAngles = new Vector3(wall.localEulerAngles.x, wall.localEulerAngles.y + 90, wall.localEulerAngles.z);
                    wall = Instantiate(wallPrefab);
                    wall.parent = seperation.transform;
                    wall.localPosition = new Vector3(xPosition + 0.01f, 0, zPosition);
                    wall.localEulerAngles = new Vector3(wall.localEulerAngles.x, wall.localEulerAngles.y - 90, wall.localEulerAngles.z);
                }
                else
                {
                    //A wall where we gonna put the title of the part
                    Transform wall = Instantiate(wallPrefab);
                    wall.parent = seperation.transform;
                    wall.localPosition = new Vector3(xPosition + width, 0, zPosition);
                    wall.localEulerAngles = new Vector3(wall.localEulerAngles.x, wall.localEulerAngles.y + 90, wall.localEulerAngles.z);

                    //We put the title on this wall
                    Transform title = Instantiate(titlePrefab);
                    title.parent = wall;
                    title.localPosition = new Vector3(0.3f, 0f, 4.8f); //Change the value
                    title.localEulerAngles = new Vector3(0, 270, 270);
                    titles.Add(title);
                }
                zPosition += width;
            }
        }
        return titles;
    }
    */
}
