﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Environment
{
    class ImageGetter: ScriptableObject
    {
        private const String apiKey = "Sz8Tszquc2l6gsz7FwY0VzEFGSH6HFqSnFD4+brwb6I";

        private WWW www;
        private String currentSearch;
        private Queue<String> results;
        private Queue<TableauResize> tableauResizeQueue;

        public ImageGetter()
        {
            tableauResizeQueue = new Queue<TableauResize>();
        }

        private void WaitForRequest(WWW www)
        {
            while (!www.isDone);

            if (www.error != null && www.error != "")
            {
				Debug.Log(www.error);
            }
            else
            {
                JSONObject result = new JSONObject(www.text);
                JSONObject imagesJson = result["d"]["results"];

                results = new Queue<String>();
                foreach (var image in imagesJson.list)
                    results.Enqueue(image.GetField("MediaUrl").str);

                while (tableauResizeQueue.Count > 0)
                    tableauResizeQueue.Dequeue().SetImage(results.Dequeue());

                //currentSearch = null;
                www = null;
            }
        }

        public void SetImage(TableauResize resize)
        {
            if (results != null && results.Count > 0)
                resize.SetImage(results.Dequeue());
            else
                tableauResizeQueue.Enqueue(resize);
        }

        private String GetUrlImage()
        {
            if (results != null && results.Count > 0)
                return results.Dequeue();

            return null;   
        }

        internal ImageGetter Init(String searchImages)
        {
            String url = "https://api.datamarket.azure.com/Bing/Search/v1/Image?Query='" + searchImages.Trim().Replace(" ", "%20") + "'&$format=JSON&$top=100";
            Dictionary<String, String> headers = new Dictionary<String, String>();
            String authorization = "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(apiKey + ":" + apiKey));
            headers.Add("Authorization", authorization);

            www = new WWW(url, null, headers);
            WaitForRequest(www);

            return this;
        }
    }
}
