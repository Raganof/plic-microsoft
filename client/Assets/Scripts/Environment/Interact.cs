﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Interact : MonoBehaviour {
    public GameObject keyToDisplay;
    public Camera nextCamera;
    public Camera previousCamera;
    public Camera cameraView;

    public float distance = 0.2f;

    private bool isDisplaying;
    private GameObject player;
    private GameObject mainCamera;
    private FirstPersonController playerController;
    private MouseLocker mouseLocker;
    private HashIDs hash;
    private Animator anim; // The animator to animate the character
    //private BoxCollider collider;
    

    private bool isObserving = false;

    private float interactTime;

    public void Awake()
    {
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        player = GameObject.FindGameObjectWithTag("Player");
        playerController = player.GetComponent<FirstPersonController>();
        mouseLocker = GameObject.FindGameObjectWithTag("GameController").GetComponent<MouseLocker>();
        hash = GameObject.FindGameObjectWithTag("GameController").GetComponent<HashIDs>();
        anim = player.GetComponentInChildren<Animator>();
        cameraView = this.GetComponent<Camera>();

        if (cameraView != null)
            cameraView.enabled = false;

        if (keyToDisplay == null)
            keyToDisplay = GameObject.Find("KeyEventCanvas").transform.FindChild("SpaceBar").gameObject;
        GameObject thing = GameObject.FindGameObjectWithTag("BookShelf");
        thing = null;
    }

	public void OnTriggerEnter(Collider other)
    {
        //Display the animation
        if (other.gameObject == player && !(isDisplaying || isObserving)) {
            isDisplaying = true;
            keyToDisplay.SetActive(true);
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player && isDisplaying)
        {
            isDisplaying = false;
            keyToDisplay.SetActive(false);
            mouseLocker.Undisplay();
        }
    }

    public void Update()
    {
        if (Time.time < interactTime + 0.5)
            return;

        //If the player is in the area and press the button Space...
        if (isDisplaying && Input.GetButtonDown("Interact") && !isObserving)
        {
            isObserving = true;
            // We unlock the mouse
            mouseLocker.Display();
            //We hide the key
            keyToDisplay.SetActive(false);

            //... then we place him in front of the object
            ObserveObject();
            interactTime = Time.time;
        }
        else if (isObserving && Input.GetKeyDown(KeyCode.Escape))
        {
            isObserving = false;
            mouseLocker.Undisplay();
            playerController.isLock = false;
            keyToDisplay.SetActive(true);

            if (cameraView != null)
            {
                cameraView.tag = "CameraView";
                mainCamera.SetActive(true);
                cameraView.enabled = false;
            }
        }
        else if (isObserving && Input.GetButtonDown("Next"))
        {
            isDisplaying = false;
            isObserving = false;
            if (nextCamera != null)
            {
                if (cameraView != null)
                {
                    cameraView.tag = "CameraView";
                    mainCamera.SetActive(true);
                    cameraView.enabled = false;
                }
                Interact nextInter = nextCamera.transform.GetComponentInParent<Interact>();
                nextInter.isObserving = true;
                nextInter.MoveAndObserve(cameraView);
            }
            else
            {
                mouseLocker.Undisplay();
                playerController.isLock = false;
                isDisplaying = true;
                isObserving = false;
                keyToDisplay.SetActive(true);

                if (cameraView != null)
                {
                    cameraView.tag = "CameraView";
                    mainCamera.SetActive(true);
                    cameraView.enabled = false;
                }
            }
        }
        else if (isObserving && Input.GetButtonDown("Previous") && previousCamera != null)
        {
            isDisplaying = false;
            isObserving = false;
            if (cameraView != null)
            {
                cameraView.tag = "CameraView";
                mainCamera.SetActive(true);
                cameraView.enabled = false;
            }
            Interact nextInter = previousCamera.transform.GetComponentInParent<Interact>();
            nextInter.isObserving = true;
            nextInter.MoveAndObserve(null);
        }
    }

    public void MoveAndObserve(Camera prevCamera)
    {
        if (prevCamera != null)
            this.previousCamera = prevCamera;
        interactTime = Time.time;
        keyToDisplay.SetActive(false);

        // We unlock the mouse
        //mouseLocker.Display();
        isDisplaying = true;
        
        //... then we place him in front of the object
        ObserveObject();

        isObserving = true;
        //We hide the key
        keyToDisplay.SetActive(false);


    }

    public void ObserveObject()
    {
        //Lock all movement of the player
        playerController.isLock = true;
        anim.SetFloat(hash.speedFloat, 0);

        //Rotate the player to be in front of the object 
        
        if (cameraView != null)
        {
            mainCamera.gameObject.SetActive(false);
            cameraView.enabled = true;
            cameraView.tag = "MainCamera";
            player.transform.position = new Vector3(cameraView.transform.position.x, player.transform.position.y, cameraView.transform.position.z);
            player.transform.rotation = Quaternion.Euler(player.transform.eulerAngles.x, cameraView.transform.eulerAngles.y, player.transform.eulerAngles.z);
            if (cameraView.transform.eulerAngles.y - cameraView.transform.eulerAngles.y % 1f == 0)
                player.transform.Translate(0f, 0f, -0.2f);
            else if (cameraView.transform.eulerAngles.y - cameraView.transform.eulerAngles.y % 1f == 90)
                player.transform.Translate(-0.2f, 0f, 0f);
            else if (cameraView.transform.eulerAngles.y - cameraView.transform.eulerAngles.y % 1f == 270)
                player.transform.Translate(0.2f, 0f, 0f);
            else
                player.transform.Translate(0f, 0f, -0.2f);
        } 
        else
        {
            player.transform.LookAt(transform);

            float x = gameObject.transform.position.x - player.transform.position.x;
            float z = gameObject.transform.position.z - player.transform.position.z;
            float yAngle = Mathf.Atan(z / x);
            yAngle = Mathf.Rad2Deg * yAngle;

            if (yAngle > 0)
                yAngle -= 90;
            else
                yAngle += 90;

            player.transform.RotateAround(gameObject.transform.position, Vector3.up, yAngle);

            //Move the player forward or backward to be at the best position
            Vector3 direction = new Vector3(gameObject.transform.position.x - player.transform.position.x, 0, gameObject.transform.position.z - player.transform.position.z);

            float scale = -distance / direction.magnitude;
            player.transform.position = gameObject.transform.position;
            player.transform.Translate(direction * scale);

            /*Plane[] planes = GeometryUtility.CalculateFrustumPlanes(mainCamera);
            if (!GeometryUtility.TestPlanesAABB(planes, collider.bounds.))
            {
                Vector3 scale = direction;
                scale.Scale(scaleNeg);
                while (!GeometryUtility.TestPlanesAABB(planes, collider.bounds))
                {
                    player.transform.Translate(scale);
                    planes = GeometryUtility.CalculateFrustumPlanes(mainCamera);
                }
                scale = direction;
                scale.Scale(scalePos);
                player.transform.Translate(scale);
            }
            else
            {
                Vector3 scale = direction;
                scale.Scale(scalePos);
                while (GeometryUtility.TestPlanesAABB(planes, collider.bounds))
                {
                    player.transform.Translate(scale);
                    planes = GeometryUtility.CalculateFrustumPlanes(mainCamera);
                }
                scale = direction;
                scale.Scale(scaleNeg);
                player.transform.Translate(scale);
            } */
        }
    }
}
