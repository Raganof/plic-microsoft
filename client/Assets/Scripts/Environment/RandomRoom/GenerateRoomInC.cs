﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.DataModels;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Environment.RandomRoom
{
    class GenerateRoomInC : GenerateRoomAbstract
    {
        public GenerateRoomInC()
        {
        }

        public override GameObject CreateRoom(GameObject otherDoor, Part part, ImageGetter imageGetter, bool leftSide = false)
        {
            this.imageGetter = imageGetter;

            int spaceFromDoor = 2;

            List<List<Content>> cutContent = part.CutInParts(3);

            GameObject room = new GameObject(part.name);

            GameObject ground = new GameObject("Ground");
            ground.transform.SetParent(room.transform);
            ground.transform.localPosition = new Vector3(0, -height / 2, 0);
            GameObject wall = new GameObject("Wall");
            wall.transform.SetParent(room.transform);

            float factor = leftSide ? 1 : -1;
            //float rotation = leftSide ? 180f : 0f;
            float rotation = 0f;

            //We place a ground at the entrance and the title in front of the door
            Transform entranceGround = Instantiate(groundPrefab);
            entranceGround.SetParent(ground.transform);
            entranceGround.localPosition = new Vector3(width / 2.0f, 0, 0);
            Transform titleWall = Instantiate(titlePrefab);
            titleWall.SetParent(wall.transform);
            //Set the title of the Room/Part
            Text titleText = titleWall.FindChild("Canvas").FindChild("Title").GetComponent<Text>();
            titleText.text = part.name;
            //Then place it in the room
            titleWall.localPosition = new Vector3(width, 0, 0);
            titleWall.localRotation = Quaternion.Euler(new Vector3(titleWall.localEulerAngles.x, titleWall.eulerAngles.y + 180, titleWall.eulerAngles.z));
            CreateCamera(titleWall);

            Transform wallPiece = Instantiate(wallPrefab);
            wallPiece.SetParent(wall.transform);
            wallPiece.localPosition = new Vector3(0.5f * width, 0, -0.5f * factor * width);
            wallPiece.localRotation = Quaternion.Euler(wallPiece.localEulerAngles.x, wallPiece.localEulerAngles.y + 90f, wallPiece.localEulerAngles.z);

            Transform groundPiece = null;
            for (int i = 1; i <= spaceFromDoor; ++i)
            {
                if (i < spaceFromDoor - 1)
                {
                    wallPiece = Instantiate(wallPrefab);
                    wallPiece.SetParent(wall.transform);
                    wallPiece.localPosition = new Vector3(width, 0, width * i * factor);
                    wallPiece.localRotation = Quaternion.Euler(new Vector3(wallPiece.localEulerAngles.x, wallPiece.eulerAngles.y + 180, wallPiece.eulerAngles.z));
                }
                groundPiece = Instantiate(groundPrefab);
                groundPiece.SetParent(ground.transform);
                groundPiece.localPosition = new Vector3(width / 2.0f, 0, width * i * factor);

                wallPiece = Instantiate(wallPrefab);
                wallPiece.SetParent(wall.transform);
                wallPiece.localPosition = new Vector3(0, 0, width * i * factor);
            }

            wallPiece = Instantiate(wallPrefab);
            wallPiece.SetParent(wall.transform);
            wallPiece.localPosition = new Vector3(width * 0.5f, 0f, width * (spaceFromDoor + 0.5f) * factor);
            wallPiece.localRotation = Quaternion.Euler(new Vector3(wallPiece.localEulerAngles.x, wallPiece.eulerAngles.y + 90 * factor, wallPiece.eulerAngles.z));

            List<List<Transform>> wallPartList = CreatePrefabsFromContents(cutContent);
            float topLeftCornerPos = 0f;
            float topRightCornerPos = 0f;
            for (int i = 0; i < 3; ++i)
            {
                List<Transform> wallTextList = wallPartList[i];
                int indexWall = 0;
                bool onLeft = true;

                String name = "";
                int leftPos = 0, rightPos = 0;
                int offset = 1;
                switch (i)
                {
                    case 0:
                        name = "Start Part";
                        break;
                    case 1:
                        name = "Middle Part";
                        offset = 2;
                        break;
                    case 2:
                        name = "End Part";
                        offset = 2;
                        break;
                }

                GameObject wallPart = new GameObject(name + " Wall");
                GameObject groundPart = new GameObject(name + " Ground");
                wallPart.transform.SetParent(wall.transform);
                groundPart.transform.SetParent(ground.transform);

                if (i > 0)
                {
                    Transform trans = Instantiate(wallPrefab);
                    trans.SetParent(wallPart.transform);
                    trans.localRotation = Quaternion.Euler(wallPiece.localEulerAngles.x, wallPiece.localEulerAngles.y + 90f, wallPiece.localEulerAngles.z);
                    trans.localPosition = new Vector3(0f, 0f, 0f);
                    trans = Instantiate(wallPrefab);
                    trans.SetParent(wallPart.transform);
                    trans.localRotation = Quaternion.Euler(wallPiece.localEulerAngles.x, wallPiece.localEulerAngles.y + 90f, wallPiece.localEulerAngles.z);
                    trans.localPosition = new Vector3(0f, 0f, -width * factor);
                }

                while (indexWall < wallTextList.Count)
                {
                    if (!onLeft)
                    {
                        if (rightPos == 0)
                        {
                            Transform trans = Instantiate(wallPrefab);
                            trans.SetParent(wallPart.transform);
                            trans.localPosition = new Vector3(-width * 2f, 0f, -offset * width * factor);
                            rightPos += 1;
                        }
                        if (wallTextList.Count - indexWall > 2)
                        {
                            wallTextList[indexWall].SetParent(wallPart.transform);
                            wallTextList[indexWall + 1].SetParent(wallPart.transform);
                            List<Transform> wallPieces = CreateOnePaintTwoText(wallPart, wallTextList[indexWall], wallTextList[indexWall + 1]);
                            DestroyCamera(wallPieces[0]);

                            indexWall += 2;
                            TransformAndRotate(wallPieces, ref rightPos, factor, offset);
                        }
                        else
                        {
                            wallTextList[indexWall].SetParent(wallPart.transform);
                            List<Transform> wallPieces = CreateOnePaintOneText(wallPart, wallTextList[indexWall]);
                            DestroyCamera(wallPieces[0]);

                            indexWall += 1;
                            TransformAndRotate(wallPieces, ref rightPos, factor, offset);
                        }
                    }
                    else
                    {
                        if (indexWall == 0)
                        {
                            wallTextList[0].SetParent(wallPart.transform);
                            List<Transform> wallPieces = CreateOnePaintOneText(wallPart, wallTextList[0]);
                            DestroyCamera(wallPieces[wallPieces.Count - 2]);
                            List<Transform> groundPieces = CreateGround(groundPart, wallPieces.Count * 2);
                            indexWall += 1;
                            TransformAndRotate(wallPieces, ref leftPos, factor, offset, groundPieces);
                        }
                        else if (wallTextList.Count - indexWall == 1)
                        {
                            if (leftPos + 2 == rightPos)
                            {
                                wallTextList[indexWall].SetParent(wallPart.transform);
                                List<Transform> wallPieces = CreateTwoPaintOneText(wallPart, wallTextList[indexWall], i == 2);
                               DestroyCamera(wallPieces[wallPieces.Count - 2]);
                                List<Transform> groundPieces = CreateGround(groundPart, wallPieces.Count * 2);
                                indexWall += 1;
                                TransformAndRotate(wallPieces, ref leftPos, factor, offset, groundPieces);
                            }
                            else
                            {
                                wallTextList[indexWall].SetParent(wallPart.transform);
                                List<Transform> wallPieces = CreateOnePaintOneText(wallPart, wallTextList[indexWall], i == 2);
                               DestroyCamera(wallPieces[wallPieces.Count - 2]);
                                List<Transform> groundPieces = CreateGround(groundPart, wallPieces.Count * 2);
                                indexWall += 1;
                                TransformAndRotate(wallPieces, ref leftPos, factor, offset, groundPieces);
                            }
                        }
                        else if (wallTextList.Count - indexWall == 2)
                        {
                            if (leftPos + 3 == rightPos)
                            {
                                wallTextList[indexWall].SetParent(wallPart.transform);
                                wallTextList[indexWall + 1].SetParent(wallPart.transform);
                                List<Transform> wallPieces = CreateTwoPaintTwoText(wallPart, wallTextList[indexWall], wallTextList[indexWall + 1], i == 2);
                               DestroyCamera(wallPieces[wallPieces.Count - 2]);
                                List<Transform> groundPieces = CreateGround(groundPart, wallPieces.Count * 2);
                                indexWall += 2;
                                TransformAndRotate(wallPieces, ref leftPos, factor, offset, groundPieces);
                            }
                            else
                            {
                                wallTextList[indexWall].SetParent(wallPart.transform);
                                wallTextList[indexWall + 1].SetParent(wallPart.transform);
                                List<Transform> wallPieces = CreateOnePaintTwoText(wallPart, wallTextList[indexWall], wallTextList[indexWall + 1], i == 2);
                               DestroyCamera(wallPieces[wallPieces.Count - 2]);
                                List<Transform> groundPieces = CreateGround(groundPart, wallPieces.Count * 2);
                                indexWall += 2;
                                TransformAndRotate(wallPieces, ref leftPos, factor, offset, groundPieces);
                            }
                        }
                        else if (wallTextList.Count - indexWall == 3)
                        {
                            wallTextList[indexWall].SetParent(wallPart.transform);
                            List<Transform> wallPieces = CreateTwoPaintOneText(wallPart, wallTextList[indexWall]);
                           DestroyCamera(wallPieces[wallPieces.Count - 2]);
                            List<Transform> groundPieces = CreateGround(groundPart, wallPieces.Count * 2);
                            indexWall += 1;
                            TransformAndRotate(wallPieces, ref leftPos, factor, offset, groundPieces);
                        }
                        else
                        {
                            if (wallTextList.Count - indexWall == 4 || (wallTextList.Count - indexWall) % 2 == 1)
                            {
                                wallTextList[indexWall].SetParent(wallPart.transform);
                                List<Transform> wallPieces = CreateOnePaintOneText(wallPart, wallTextList[indexWall]);
                               DestroyCamera(wallPieces[wallPieces.Count - 2]);
                                List<Transform> groundPieces = CreateGround(groundPart, wallPieces.Count * 2);
                                indexWall += 1;
                                TransformAndRotate(wallPieces, ref leftPos, factor, offset, groundPieces);
                            }
                            else
                            {
                                wallTextList[indexWall].SetParent(wallPart.transform);
                                wallTextList[indexWall + 1].SetParent(wallPart.transform);
                                List<Transform> wallPieces = CreateOnePaintTwoText(wallPart, wallTextList[indexWall], wallTextList[indexWall + 1]);
                               DestroyCamera(wallPieces[wallPieces.Count - 2]);
                                List<Transform> groundPieces = CreateGround(groundPart, wallPieces.Count * 2);
                                indexWall += 2;
                                TransformAndRotate(wallPieces, ref leftPos, factor, offset, groundPieces);
                            }
                        }
                    }

                    onLeft = !onLeft;
                }
                float ecart = leftSide ? 2 : 2;
                switch (i)
                {
                    case 0:
                        topLeftCornerPos = leftPos + offset;
                        wallPart.transform.localPosition = new Vector3(width * 0.5f, 0f, width * (0.5f + ecart) * factor);
                        wallPart.transform.localRotation = Quaternion.Euler(0, -90f * factor, 0);
                        groundPart.transform.localPosition = new Vector3(width * 0.5f, 0f, width * (0.5f + ecart) * factor);
                        groundPart.transform.localRotation = Quaternion.Euler(0, -90f * factor, 0);
                        break;
                    case 1:
                        topRightCornerPos = leftPos + offset;
                        wallPart.transform.localPosition = new Vector3(width * topLeftCornerPos, 0f, width * spaceFromDoor * factor);
                        groundPart.transform.localPosition = new Vector3(width * topLeftCornerPos, 0f, width * spaceFromDoor * factor);
                        break;
                    case 2:
                        List<Transform> wallPieces = new List<Transform>();
                        Transform trans = Instantiate(wallPrefab);
                        trans.SetParent(wallPart.transform);
                        wallPieces.Add(trans);
                        trans = Instantiate(wallPrefab);
                        trans.SetParent(wallPart.transform);
                        wallPieces.Add(trans);
                        TransformAndRotate(wallPieces, ref rightPos, factor, offset);

                        trans = Instantiate(wallPrefab);
                        trans.SetParent(wallPart.transform);
                        trans.localPosition = new Vector3(-width * 0.5f, 0f, -(rightPos + offset - 0.5f) * width * factor);
                        trans.localRotation = Quaternion.Euler(trans.localEulerAngles.x, trans.localEulerAngles.y - 90f * factor * -1f, trans.localEulerAngles.z);
                        trans = Instantiate(wallPrefab);
                        trans.SetParent(wallPart.transform);
                        trans.localPosition = new Vector3(-width * 1.5f, 0f, -(rightPos + offset - 0.5f) * width * factor);
                        trans.localRotation = Quaternion.Euler(trans.localEulerAngles.x, trans.localEulerAngles.y - 90f * factor * -1, trans.localEulerAngles.z);


                        wallPart.transform.localPosition = new Vector3(width * (topLeftCornerPos - 0.5f), 0f, width * spaceFromDoor * factor + width * (topRightCornerPos - 0.5f) * factor * -1);
                        wallPart.transform.localRotation = Quaternion.Euler(0, -90f * factor * -1, 0);
                        groundPart.transform.localPosition = new Vector3(width * (topLeftCornerPos - 0.5f), 0f, width * spaceFromDoor * factor + width * (topRightCornerPos - 0.5f) * factor * -1);
                        groundPart.transform.localRotation = Quaternion.Euler(0, -90f * factor * -1, 0);
                        break;
                }
            }


            //Now We have to link the other room to the new one
            float yRotation = (otherDoor.transform.rotation.eulerAngles.y + 360) % 360;
            room.transform.Rotate(0, yRotation + 180, 0);
            float x = otherDoor.transform.position.x;
            float y = otherDoor.transform.position.y;
            float z = otherDoor.transform.position.z;

            room.transform.position = new Vector3(x, y, z);

            return room;
        }

        public override bool IsValid(Part part)
        {
            return part.CutInSimpleParts().Count >= 9;
        }

        private void TransformAndRotate(List<Transform> wallPieces, ref int pos, float factor, int offset, List<Transform> groundPieces = null)
        {
            if (groundPieces == null)
            {
                foreach (Transform trans in wallPieces)
                {
                    trans.localPosition = new Vector3(-width * 2, 0f, -(pos + offset) * width * factor);
                    pos += 1;
                }
            }
            else
            {
                for (int i = 0; i < wallPieces.Count; ++i)
                {
                    Transform wallPiece = wallPieces[i];
                    Transform groundPiece1 = groundPieces[i * 2];
                    Transform groundPiece2 = groundPieces[i * 2 + 1];

                    wallPiece.localRotation = Quaternion.Euler(wallPiece.localEulerAngles.x, wallPiece.localEulerAngles.y + 180f, wallPiece.localEulerAngles.z);
                    wallPiece.localPosition = new Vector3(0f, 0f, -(pos + offset) * width * factor);
                    groundPiece1.localPosition = new Vector3(-width * 0.5f, 0f, -(pos + offset) * width * factor);
                    groundPiece2.localPosition = new Vector3(-width * 1.5f, 0f, -(pos + offset) * width * factor);

                    pos += 1;
                }
            }
        }

    }
}
