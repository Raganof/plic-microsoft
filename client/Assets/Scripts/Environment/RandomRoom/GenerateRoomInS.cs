﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Scripts.DataModels;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Environment.RandomRoom
{
    class GenerateRoomInS: GenerateRoomAbstract
    {
        public GenerateRoomInS()
        {
        }

        public override GameObject CreateRoom(GameObject otherDoor, Part part, ImageGetter imageGetter, bool leftSide = false)
        {
            List<List<Content>> cutContent = part.CutInParts(8, 3);

            GameObject room = new GameObject(part.name);

            GameObject ground = new GameObject("Ground");
            ground.transform.SetParent(room.transform);
            ground.transform.localPosition = new Vector3(0, -height / 2, 0);
            GameObject wall = new GameObject("Wall");
            wall.transform.SetParent(room.transform);

            float factor = leftSide ? 1 : -1;

            //We place a ground at the entrance and the title in front of the door
            Transform entranceGround = Instantiate(groundPrefab);
            entranceGround.SetParent(ground.transform);
            entranceGround.localPosition = new Vector3(width * 0.5f, 0, 0);
            entranceGround = Instantiate(groundPrefab);
            entranceGround.SetParent(ground.transform);
            entranceGround.localPosition = new Vector3(width * 0.5f, 0, -factor * width);
            entranceGround = Instantiate(groundPrefab);
            entranceGround.SetParent(ground.transform);
            entranceGround.localPosition = new Vector3(width * 1.5f, 0, 0);
            entranceGround = Instantiate(groundPrefab);
            entranceGround.SetParent(ground.transform);
            entranceGround.localPosition = new Vector3(width * 1.5f, 0, -factor * width);

            Transform wallPiece = Instantiate(wallPrefab);
            wallPiece.SetParent(wall.transform);
            wallPiece.localPosition = new Vector3(0, 0, -factor * width);
            wallPiece = Instantiate(wallPrefab);
            wallPiece.SetParent(wall.transform);
            wallPiece.localPosition = new Vector3(0.5f * width, 0, -1.5f * factor * width);
            wallPiece.localRotation = Quaternion.Euler(wallPiece.localEulerAngles.x, wallPiece.localEulerAngles.y + 90f, wallPiece.localEulerAngles.z);
            wallPiece = Instantiate(wallPrefab);
            wallPiece.SetParent(wall.transform);
            wallPiece.localPosition = new Vector3(1.5f * width, 0, -1.5f * factor * width);
            wallPiece.localRotation = Quaternion.Euler(wallPiece.localEulerAngles.x, wallPiece.localEulerAngles.y + 90f, wallPiece.localEulerAngles.z);

            Transform titleWall = Instantiate(titlePrefab);
            titleWall.SetParent(wall.transform);
            //Set the title of the Room/Part
            Text titleText = titleWall.FindChild("Canvas").FindChild("Title").GetComponent<Text>();
            titleText.text = part.name;
            //Then place it in the room
            titleWall.localPosition = new Vector3(width * 2, 0, 0);
            titleWall.localRotation = Quaternion.Euler(new Vector3(titleWall.localEulerAngles.x, titleWall.eulerAngles.y + 180, titleWall.eulerAngles.z));

            List<List<Transform>> wallPartList = CreatePrefabsFromContents(cutContent);
            int introductionSize = 2; //The first wall + the title
            int vertPos = 0;
            int horPos = 1;

            //Introduction of the part
            foreach (Transform trans in wallPartList[0])
            {
                //Place the text
                trans.SetParent(wall.transform);
                trans.localRotation = Quaternion.Euler(trans.localEulerAngles.x, trans.localEulerAngles.y + 180, trans.localEulerAngles.z);
                trans.localPosition = new Vector3(2 * width, 0, factor * width * horPos);

                //Place ground
                Transform groundPiece = Instantiate(groundPrefab);
                groundPiece.SetParent(ground.transform);
                groundPiece.localPosition = new Vector3(0.5f * width, 0, factor * width * horPos);
                groundPiece = Instantiate(groundPrefab);
                groundPiece.SetParent(ground.transform);
                groundPiece.localPosition = new Vector3(1.5f * width, 0, factor * width * horPos);

                wallPiece = Instantiate(wallPrefab);
                wallPiece.SetParent(wall.transform);
                wallPiece.localPosition = new Vector3(0, 0, factor * width * horPos);

                introductionSize += 1;
                horPos += 1;
            }
            int bottomPos = horPos;

            // The first part on bottom of the room
            List<Transform> wallList = wallPartList[1];
            int indexWall = 0;
            while (indexWall < wallList.Count)
            {
                if (wallList.Count - indexWall == 1)
                {
                    wallList[indexWall].SetParent(wall.transform);
                    List<Transform> wallPieces = CreateOnePaintOneText(wall, wallList[indexWall]);
                    List<Transform> groundPieces = CreateGround(ground, wallPieces.Count);
                    TransformAndRotate(wallPieces, groundPieces, ref bottomPos, 0, factor, 0, 1, false);
                    indexWall++;
                }
                else
                {
                    System.Random random = new System.Random();
                    int rand = random.Next();
                    List<Transform> wallPieces = null;
                    switch (rand % 4)
                    {
                        case 0:
                            wallPieces[indexWall].SetParent(wall.transform);
                            wallPieces = CreateOnePaintOneText(wall, wallList[indexWall]);
                            ++indexWall;
                            break;
                        case 1:
                            wallPieces[indexWall].SetParent(wall.transform);
                            wallPieces = CreateTwoPaintOneText(wall, wallList[indexWall]);
                            break;
                        case 2:
                            wallPieces[indexWall].SetParent(wall.transform);
                            wallPieces[indexWall].SetParent(wall.transform);
                            break;
                        case 3:
                            wallPieces[indexWall].SetParent(wall.transform);
                            wallPieces[indexWall].SetParent(wall.transform);
                            break;
                    }
                    List<Transform> groundPieces = CreateGround(ground, wallPieces.Count);
                    TransformAndRotate(wallPieces, groundPieces, ref bottomPos, 0, factor, 0, 1, false);
                }
            }

            int sidePos = 1;
            //The second part on Right (or Left)
            wallList = wallPartList[2];
            while (indexWall < wallList.Count)
            {
                if (wallList.Count - indexWall == 1)
                {
                    wallList[indexWall].SetParent(wall.transform);
                    List<Transform> wallPieces = CreateOnePaintOneText(wall, wallList[indexWall]);
                    List<Transform> groundPieces = CreateGround(ground, wallPieces.Count);
                    TransformAndRotate(wallPieces, groundPieces, ref sidePos, bottomPos, factor, 0, -factor);
                    indexWall++;
                }
                else
                {
                    System.Random random = new System.Random();
                    int rand = random.Next();
                    List<Transform> wallPieces = null;
                    switch (rand % 4)
                    {
                        case 0:
                            wallPieces[indexWall].SetParent(wall.transform);
                            wallPieces = CreateOnePaintOneText(wall, wallList[indexWall]);
                            ++indexWall;
                            break;
                        case 1:
                            wallPieces[indexWall].SetParent(wall.transform);
                            wallPieces = CreateTwoPaintOneText(wall, wallList[indexWall]);
                            break;
                        case 2:
                            wallPieces[indexWall].SetParent(wall.transform);
                            wallPieces[indexWall].SetParent(wall.transform);
                            break;
                        case 3:
                            wallPieces[indexWall].SetParent(wall.transform);
                            wallPieces[indexWall].SetParent(wall.transform);
                            break;
                    }
                    List<Transform> groundPieces = CreateGround(ground, wallPieces.Count);
                    TransformAndRotate(wallPieces, groundPieces, ref sidePos, bottomPos, factor, 0, -factor);
                }
            }

            for (int i = 3; i < wallPartList.Count; ++i)
            {
                indexWall = 0;
                wallList = wallPartList[i];
                if (i == 3 || i == 7)
                {
                    int currentSize = 0;
                    int maxSize = bottomPos - introductionSize - 1;
                    int ecart = 0;
                    while (currentSize < maxSize)
                    {
                        if (maxSize - currentSize <= 5)
                        {
                            switch (maxSize - currentSize)
                            {
                                case 5:
                                    wallList[indexWall].SetParent(wall.transform);
                                    wallList[indexWall + 1].SetParent(wall.transform);
                                    List<Transform> wallPieces = CreateTwoPaintTwoText(wall, wallList[indexWall], wallList[indexWall + 1]);
                                    List<Transform> groundPieces = CreateGround(ground, 5 * (vertPos - 1));
                                    TransformAndRotate(wallPieces, groundPieces, ref horPos, vertPos, factor, 0, -1, false, -1);
                                    indexWall += 2;
                                    break;
                                case 4:
                                    wallList[indexWall].SetParent(wall.transform);
                                    wallPieces = null;
                                    if (wallList.Count - indexWall == 2)
                                    {
                                        wallList[indexWall + 1].SetParent(wall.transform);
                                        wallPieces = CreateOnePaintTwoText(wall, wallList[indexWall], wallList[indexWall + 1]);
                                        indexWall += 2;
                                    }
                                    else
                                    {
                                        wallPieces = CreateTwoPaintOneText(wall, wallList[indexWall]);
                                        indexWall += 1;
                                    }
                                    groundPieces = CreateGround(ground, 4 * (vertPos - 1));
                                    TransformAndRotate(wallPieces, groundPieces, ref horPos, vertPos, factor, 0, -1, false, -1);
                                    break;
                                default:
                                    wallList[indexWall].SetParent(wall.transform);
                                    wallPieces = CreateOnePaintOneText(wall, wallList[indexWall]);
                                    groundPieces = CreateGround(ground, 3 * (vertPos - 1));
                                    TransformAndRotate(wallPieces, groundPieces, ref horPos, vertPos, factor, 0, -1, false, -1);
                                    indexWall += 1;
                                    break;
                            }
                        }
                        else
                        {
                            List<int> maxTextList = new List<int>();
                            maxTextList.Add((maxSize - currentSize) / 5 * 2); // CreateTwoPaintTwoText
                            maxTextList.Add((maxSize - currentSize) / 4 * 2); // CreateOnePaintTwoText
                            maxTextList.Add((maxSize - currentSize) / 4 * 1); // CreateTwoPaintOneText
                            maxTextList.Add((maxSize - currentSize) / 3 * 1); // CreateOnePaintOneText

                            List<int> possibilities = new List<int>();
                            for (int j = 0; j < maxTextList.Count; ++j)
                            {
                                int nbTextToCreate = j < 2 ? 2 : 1;
                                int newEcart = (wallList.Count - indexWall - maxTextList[i]) / nbTextToCreate;
                                if (ecart > 0 && newEcart <= 0)
                                {
                                    possibilities.Add(j);
                                }
                                else if (ecart < 0 && newEcart >= 0)
                                {
                                    possibilities.Add(j);
                                }
                                else if (ecart == 0 && -1 < newEcart && newEcart < 1)
                                {
                                    possibilities.Add(j);
                                }
                            }

                            if (possibilities.Count == 0)
                            {
                                Console.WriteLine("Error A2: No possibilities !");
                                break;
                            }

                            System.Random random = new System.Random();
                            int r = random.Next();
                            switch (possibilities[r % possibilities.Count])
                            {
                                case 0:
                                    wallList[indexWall].SetParent(wall.transform);
                                    wallList[indexWall + 1].SetParent(wall.transform);
                                    List<Transform> wallPieces = CreateTwoPaintTwoText(wall, wallList[indexWall], wallList[indexWall + 1]);
                                    List<Transform> groundPieces = CreateGround(ground, 5 * (vertPos - 1));
                                    TransformAndRotate(wallPieces, groundPieces, ref horPos, vertPos, factor, 0, -1, false, -1);
                                    indexWall += 2;
                                    break;
                                case 1:
                                    wallList[indexWall].SetParent(wall.transform);
                                    wallList[indexWall + 1].SetParent(wall.transform);
                                    wallPieces = CreateOnePaintTwoText(wall, wallList[indexWall], wallList[indexWall + 1]);
                                    groundPieces = CreateGround(ground, 4 * (vertPos - 1));
                                    TransformAndRotate(wallPieces, groundPieces, ref horPos, vertPos, factor, 0, -1, false, -1);
                                    indexWall += 2;
                                    break;
                                case 2:
                                    wallList[indexWall].SetParent(wall.transform);
                                    wallPieces = CreateTwoPaintOneText(wall, wallList[indexWall]);
                                    groundPieces = CreateGround(ground, 4 * (vertPos - 1));
                                    TransformAndRotate(wallPieces, groundPieces, ref horPos, vertPos, factor, 0, -1, false, -1);
                                    indexWall += 1;
                                    break;
                                case 3:
                                    wallList[indexWall].SetParent(wall.transform);
                                    wallPieces = CreateTwoPaintOneText(wall, wallList[indexWall]);
                                    groundPieces = CreateGround(ground, 3 * (vertPos - 1));
                                    TransformAndRotate(wallPieces, groundPieces, ref horPos, vertPos, factor, 0, -1, false, -1);
                                    indexWall += 1;
                                    break;
                            }
                        }
                    }

                }
                else if (i == 4)
                {
                    horPos = 2;
                    if (wallList.Count == 1)
                    {
                        if (introductionSize == 3)
                        {
                            wallList[0].SetParent(wall.transform);
                            List<Transform> wallPieces = CreateOnePaintOneText(wall, wallList[0]);
                            List<Transform> groundPieces = CreateGround(ground, 3);
                            TransformAndRotate(wallPieces, groundPieces, ref horPos, vertPos, factor, 0, 1, false, -1);
                        }
                        else
                        {
                            wallList[0].SetParent(wall.transform);
                            List<Transform> wallPieces = CreateTwoPaintOneText(wall, wallList[0]);
                            List<Transform> groundPieces = CreateGround(ground, 4);
                            TransformAndRotate(wallPieces, groundPieces, ref horPos, vertPos, factor, 0, 1, false, -1);
                        }
                    }
                    else
                    {
                        if (wallList.Count > 2)
                            Console.WriteLine("Erreur A0: Trop de texte");

                        if (introductionSize == 4)
                        {
                            wallList[0].SetParent(wall.transform);
                            wallList[1].SetParent(wall.transform);
                            List<Transform> wallPieces = CreateOnePaintTwoText(wall, wallList[0], wallList[1]);
                            List<Transform> groundPieces = CreateGround(ground, 4);
                            TransformAndRotate(wallPieces, groundPieces, ref horPos, vertPos, factor, 0, 1, false, -1);
                        }
                        else
                        {
                            wallList[0].SetParent(wall.transform);
                            wallList[1].SetParent(wall.transform);
                            List<Transform> wallPieces = CreateTwoPaintTwoText(wall, wallList[0], wallList[1]);
                            List<Transform> groundPieces = CreateGround(ground, 5);
                            TransformAndRotate(wallPieces, groundPieces, ref horPos, vertPos, factor, 0, 1, false, -1);
                        }
                    }
                }
            }


            int incr = -1;
            for (int i = 2; i < wallPartList.Count; ++i)
            {
                if (wallList.Count - indexWall == 1)
                {
                    wallList[indexWall].SetParent(wall.transform);
                    List<Transform> wallPieces = CreateOnePaintOneText(wall, wallList[indexWall]);
                    List<Transform> groundPieces = CreateGround(ground, wallPieces.Count);
                    TransformAndRotate(wallPieces, groundPieces, ref vertPos, horPos, factor, 180, -1, true);
                    indexWall++;
                }
                else
                {
                    System.Random random = new System.Random();
                    int rand = random.Next();
                    List<Transform> wallPieces = null;
                    switch (rand % 4)
                    {
                        case 0:
                            wallPieces[indexWall].SetParent(wall.transform);
                            wallPieces = CreateOnePaintOneText(wall, wallList[indexWall]);
                            ++indexWall;
                            break;
                        case 1:
                            wallPieces[indexWall].SetParent(wall.transform);
                            wallPieces = CreateTwoPaintOneText(wall, wallList[indexWall]);
                            break;
                        case 2:
                            wallPieces[indexWall].SetParent(wall.transform);
                            wallPieces[indexWall].SetParent(wall.transform);
                            break;
                        case 3:
                            wallPieces[indexWall].SetParent(wall.transform);
                            wallPieces[indexWall].SetParent(wall.transform);
                            break;
                    }
                    List<Transform> groundPieces = CreateGround(ground, wallPieces.Count);
                    TransformAndRotate(wallPieces, groundPieces, ref vertPos, horPos, factor, 180, -1, true);
                }
            }



            //Now We have to link the other room to the new one
            float yRotation = (otherDoor.transform.rotation.eulerAngles.y + 360) % 360;
            room.transform.Rotate(0, yRotation + 180, 0);
            float x = otherDoor.transform.position.x;
            float y = otherDoor.transform.position.y + height / 2.0f;
            float z = otherDoor.transform.position.z;

            room.transform.position = new Vector3(x, y, z);

            return room;
        }

        public override bool IsValid(Part part)
        {
            return part.CutInSimpleParts().Count >= 13;
        }

        private void TransformAndRotate(List<Transform> wallPieces, List<Transform> groundPieces, ref int pos, int fixPos, float factor, float rotation, float factorGround = 1f, bool vertical = true, int incr = 1)
        {
            if (vertical)
            {
                int sizeGround = groundPieces.Count / wallPieces.Count;
                for (int i = 0; i < wallPieces.Count; ++i)
                {
                    Transform wallPiece = wallPieces[i];
                    wallPiece.localPosition = new Vector3(width * pos * factor, 0, fixPos);
                    wallPiece.localRotation = Quaternion.Euler(wallPiece.localEulerAngles.x, wallPiece.localEulerAngles.y - 90 + rotation, wallPiece.localEulerAngles.z);

                    for (int j = 0; j < sizeGround; ++j)
                    {
                        Transform groundPiece = groundPieces[j + i];
                        groundPiece.localPosition = new Vector3(width * pos * factor, 0, fixPos + (j + 0.5f) * factorGround * width);
                    }
                    pos += incr;
                }
            }
            else
            {
                for (int i = 0; i < wallPieces.Count; ++i)
                {
                    Transform wallPiece = wallPieces[i];
                    wallPiece.localPosition = new Vector3(fixPos, 0, width * pos * factor);
                    wallPiece.localRotation = Quaternion.Euler(wallPiece.localEulerAngles.x, wallPiece.localEulerAngles.y + rotation, wallPiece.localEulerAngles.z);

                    Transform groundPiece = groundPieces[i];
                    groundPiece.localPosition = new Vector3(fixPos + 0.5f * factorGround * width, 0, width * pos * factor);
                    pos += incr;
                }
            }
        }

    }
}
