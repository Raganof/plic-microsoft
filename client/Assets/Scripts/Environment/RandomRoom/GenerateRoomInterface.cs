﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.DataModels;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Assets.Scripts.Environment.RandomRoom
{
    abstract class GenerateRoomAbstract: ScriptableObject
    {
        protected Transform doorPrefab;
        protected Transform wallPrefab;
        protected Transform groundPrefab;

        protected Transform textPrefab;
        protected Transform partTextPrefab;
        protected Transform titlePrefab;
        protected Transform imagePrefab;

        protected Transform cameraPrefab;

        protected float width;
        protected float height;
        protected float wallWidth;

        protected static Camera previousCamera;
        protected ImageGetter imageGetter;

        protected System.Random random;

        public GenerateRoomAbstract Init(Transform doorPrefab, Transform wallPrefab, Transform groundPrefab,
                                            Transform textPrefab, Transform partTextPrefab, Transform titlePrefab, Transform imagePrefab, Transform cameraPrefab)
        {
            this.doorPrefab = doorPrefab;
            this.wallPrefab = wallPrefab;
            this.groundPrefab = groundPrefab;
            this.textPrefab = textPrefab;
            this.partTextPrefab = partTextPrefab;
            this.titlePrefab = titlePrefab;
            this.imagePrefab = imagePrefab;
            this.cameraPrefab = cameraPrefab;

            Renderer renderer = groundPrefab.GetComponent<Renderer>();
            width = renderer.bounds.size.z;

            renderer = wallPrefab.GetComponent<Renderer>();
            wallWidth = renderer.bounds.size.x;
            height = renderer.bounds.size.y;

            random = new System.Random();

            return this;
        }

        public abstract GameObject CreateRoom(GameObject otherDoor, Part part, ImageGetter imageGetter, bool leftSide = false);

        public abstract bool IsValid(Part part);

        protected List<List<Transform>> CreatePrefabsFromContents(List<List<Content>> cutContent)
        {
            List<List<Transform>> wallPartList = new List<List<Transform>>();
            foreach (List<Content> contents in cutContent)
            {
                List<Transform> wallTextList = new List<Transform>();
                foreach (Content content in contents)
                {
                    if (content is SimplePart)
                    {
                        Transform wallPiece = Instantiate(partTextPrefab);
                        wallPiece.localRotation = Quaternion.Euler(wallPiece.localEulerAngles.x, wallPiece.localEulerAngles.y, wallPiece.localEulerAngles.z);
                        Text text = wallPiece.FindChild("Canvas").FindChild("Title").GetComponent<Text>();
                        text.text = ((SimplePart)content).name;
                        text = wallPiece.FindChild("Canvas").FindChild("Content").GetComponent<Text>();
                        text.text = ((SimplePart)content).content.content;

                        wallTextList.Add(wallPiece);
                    }
                    else
                    {
                        Transform wallPiece = Instantiate(textPrefab);
                        wallPiece.localRotation = Quaternion.Euler(wallPiece.localEulerAngles.x, wallPiece.localEulerAngles.y, wallPiece.localEulerAngles.z);
                        Text text = wallPiece.FindChild("Canvas").FindChild("Content").GetComponent<Text>();
                        text.text = ((Paragraph)content).content;

                        wallTextList.Add(wallPiece);
                    }
                }
                wallPartList.Add(wallTextList);
            }
            return wallPartList;
        }

        protected List<Transform> CreatePrefabsFromContents(List<Content> cutContent)
        {
            List<Transform> wallTextList = new List<Transform>();
            foreach (Content content in cutContent)
            {
                if (content is SimplePart)
                {
                    Transform wallPiece = Instantiate(partTextPrefab);
                    wallPiece.localRotation = Quaternion.Euler(wallPiece.localEulerAngles.x, wallPiece.localEulerAngles.y, wallPiece.localEulerAngles.z);
                    Text text = wallPiece.FindChild("Canvas").FindChild("Title").GetComponent<Text>();
                    text.text = ((SimplePart)content).name;
                    text = wallPiece.FindChild("Canvas").FindChild("Content").GetComponent<Text>();
                    text.text = ((SimplePart)content).content.content;

                    wallTextList.Add(wallPiece);
                }
                else
                {
                    Transform wallPiece = Instantiate(textPrefab);
                    wallPiece.localRotation = Quaternion.Euler(wallPiece.localEulerAngles.x, wallPiece.localEulerAngles.y, wallPiece.localEulerAngles.z);
                    Text text = wallPiece.FindChild("Canvas").FindChild("Content").GetComponent<Text>();
                    text.text = ((Paragraph)content).content;

                    wallTextList.Add(wallPiece);
                }
            }

            return wallTextList;
        }

        protected List<Transform> CreateOnePaintOneText(GameObject wall, Transform text, bool exitDoor = false)
        {
            text.SetParent(wall.transform);

            List<Transform> walls = new List<Transform>();

            Transform wallPaint = Instantiate(imagePrefab);
            wallPaint.SetParent(wall.transform);

            TableauResize resize = wallPaint.GetComponentInChildren<TableauResize>();
            imageGetter.SetImage(resize);

            if (random.Next(0, 1) % 2 == 0)
            {
                walls.Add(text);
                CreateCamera(text);

                walls.Add(wallPaint);
                CreateCamera(wallPaint);
            }
            else
            {
                walls.Add(wallPaint);
                CreateCamera(wallPaint);

                walls.Add(text);
                CreateCamera(text);
            }

            Transform simpleWall = exitDoor ? Instantiate(doorPrefab) : Instantiate(wallPrefab);
            if (exitDoor)
            {
                simpleWall.tag = "ExitDoor";
                simpleWall.name = "ExitDoor";
            }
            simpleWall.SetParent(wall.transform);
            walls.Add(simpleWall);

            return walls;
        }

        protected List<Transform> CreateTwoPaintOneText(GameObject wall, Transform text, bool exitDoor = false)
        {
            text.SetParent(wall.transform);

            List<Transform> walls = new List<Transform>();

            Transform wallPaint1 = Instantiate(imagePrefab);
            wallPaint1.SetParent(wall.transform);
            wallPaint1.localRotation = Quaternion.Euler(wallPaint1.localEulerAngles.x, wallPaint1.localEulerAngles.y, wallPaint1.localEulerAngles.z);

            TableauResize resize = wallPaint1.GetComponentInChildren<TableauResize>();
            imageGetter.SetImage(resize);

            Transform wallPaint2 = Instantiate(imagePrefab);
            wallPaint2.SetParent(wall.transform);
            wallPaint2.localRotation = Quaternion.Euler(wallPaint2.localEulerAngles.x, wallPaint2.localEulerAngles.y, wallPaint2.localEulerAngles.z);

            resize = wallPaint2.GetComponentInChildren<TableauResize>();
            //resize.image = url;

            Transform simpleWall = exitDoor ? Instantiate(doorPrefab) : Instantiate(wallPrefab);
            if (exitDoor)
            {
                simpleWall.name = "ExitDoor";
                simpleWall.tag = "ExitDoor";
            }
            simpleWall.SetParent(wall.transform);

            int r = random.Next(0, 2);
            if (r % 3 == 0)
            {
                walls.Add(wallPaint1);
                CreateCamera(wallPaint1);

                walls.Add(text);
                CreateCamera(text);

                walls.Add(wallPaint2);
                CreateCamera(wallPaint2);
            }
            else if (r % 3 == 1)
            {
                walls.Add(wallPaint1);
                CreateCamera(wallPaint1);

                walls.Add(wallPaint2);
                CreateCamera(wallPaint2);

                walls.Add(text);
                CreateCamera(text);
            }
            else
            {
                walls.Add(text);
                CreateCamera(text);

                walls.Add(wallPaint1);
                CreateCamera(wallPaint1);

                walls.Add(wallPaint2);
                CreateCamera(wallPaint2);
            }

            walls.Add(simpleWall);

            return walls;
        }

        protected List<Transform> CreateOnePaintTwoText(GameObject wall, Transform text1, Transform text2, bool exitDoor = false)
        {
            text1.SetParent(wall.transform);
            text2.SetParent(wall.transform);

            List<Transform> walls = new List<Transform>();

            Transform wallPaint = Instantiate(imagePrefab);
            wallPaint.SetParent(wall.transform);
            wallPaint.localRotation = Quaternion.Euler(wallPaint.localEulerAngles.x, wallPaint.localEulerAngles.y, wallPaint.localEulerAngles.z);

            TableauResize resize = wallPaint.GetComponentInChildren<TableauResize>();
            imageGetter.SetImage(resize);

            Transform simpleWall = exitDoor ? Instantiate(doorPrefab) : Instantiate(wallPrefab);
            if (exitDoor)
            {
                simpleWall.name = "ExitDoor";
                simpleWall.tag = "ExitDoor";
            }
            simpleWall.SetParent(wall.transform);

            int r = random.Next(0, 2);
            if (r % 3 == 0)
            {
                walls.Add(text1);
                CreateCamera(text1);

                walls.Add(wallPaint);
                CreateCamera(wallPaint);

                walls.Add(text2);
                CreateCamera(text2);
            }
            else if (r % 3 == 1)
            {
                walls.Add(wallPaint);
                CreateCamera(wallPaint);

                walls.Add(text1);
                CreateCamera(text1);

                walls.Add(text2);
                CreateCamera(text2);
            }
            else
            {
                walls.Add(text1);
                CreateCamera(text1);

                walls.Add(text2);
                CreateCamera(text2);

                walls.Add(wallPaint);
                CreateCamera(wallPaint);
            }

            walls.Add(simpleWall);

            return walls;
        }

        protected List<Transform> CreateTwoPaintTwoText(GameObject wall, Transform text1, Transform text2, bool exitDoor = false)
        {
            text1.SetParent(wall.transform);
            text2.SetParent(wall.transform);

            List<Transform> walls = new List<Transform>();

            Transform wallPaint1 = Instantiate(imagePrefab);
            wallPaint1.SetParent(wall.transform);
            wallPaint1.localRotation = Quaternion.Euler(wallPaint1.localEulerAngles.x, wallPaint1.localEulerAngles.y, wallPaint1.localEulerAngles.z);

            TableauResize resize = wallPaint1.GetComponentInChildren<TableauResize>();
            imageGetter.SetImage(resize);

            Transform wallPaint2 = Instantiate(imagePrefab);
            wallPaint2.SetParent(wall.transform);
            wallPaint2.localRotation = Quaternion.Euler(wallPaint2.localEulerAngles.x, wallPaint2.localEulerAngles.y, wallPaint2.localEulerAngles.z);

            resize = wallPaint2.GetComponentInChildren<TableauResize>();
            //resize.image = url;

            Transform simpleWall = exitDoor ? Instantiate(doorPrefab) : Instantiate(wallPrefab);
            if (exitDoor)
            {
                simpleWall.name = "ExitDoor";
                simpleWall.tag = "ExitDoor";
            }
            simpleWall.SetParent(wall.transform);

            int r = random.Next(0, 3);
            if (r % 4 == 0)
            {
                walls.Add(text1);
                CreateCamera(text1);

                walls.Add(wallPaint1);
                CreateCamera(wallPaint1);

                walls.Add(wallPaint2);
                CreateCamera(wallPaint2);

                walls.Add(text2);
                CreateCamera(text2);
            }
            else if (r % 4 == 1)
            {
                walls.Add(wallPaint1);
                CreateCamera(wallPaint1);

                walls.Add(text1);
                CreateCamera(text1);

                walls.Add(text2);
                CreateCamera(text2);

                walls.Add(wallPaint2);
                CreateCamera(wallPaint2);
            }
            else if (r % 4 == 2)
            {
                walls.Add(text1);
                CreateCamera(text1);

                walls.Add(wallPaint1);
                CreateCamera(wallPaint1);

                walls.Add(text2);
                CreateCamera(text2);

                walls.Add(wallPaint2);
                CreateCamera(wallPaint2);
            }
            else
            {
                walls.Add(wallPaint1);
                CreateCamera(wallPaint1);

                walls.Add(text1);
                CreateCamera(text1);

                walls.Add(wallPaint2);
                CreateCamera(wallPaint2);

                walls.Add(text2);
                CreateCamera(text2);
            }

            walls.Add(simpleWall);

            return walls;
        }

        protected List<Transform> CreateGround(GameObject ground, int n)
        {
            List<Transform> grounds = new List<Transform>();

            for (int i = 0; i < n; ++i)
            {
                Transform groundPiece = Instantiate(groundPrefab);
                groundPiece.SetParent(ground.transform);
                grounds.Add(groundPiece);
            }

            return grounds;
        }

        protected void CreateCamera(Transform lookAtLeft)
        {
            Transform cameraInstance = Instantiate(cameraPrefab);
            cameraInstance.SetParent(lookAtLeft.transform);

            Camera camera = cameraInstance.GetComponent<Camera>();
            Interact interact = cameraInstance.GetComponent<Interact>();

            if (previousCamera != null)
            {
                Interact previousInteract = previousCamera.GetComponent<Interact>();
                previousInteract.nextCamera = camera;
            }

            interact.previousCamera = previousCamera;

            cameraInstance.localRotation = Quaternion.Euler(0, -90, 0);
            cameraInstance.localScale = new Vector3(1, 1, 1);
            cameraInstance.localPosition = new Vector3(21.62f, 0.042747f, 0.468739f);

            previousCamera = camera;
        }

        protected void DestroyCamera(Transform wallInstance)
        {
            Camera camera = wallInstance.GetComponentInChildren<Camera>();
            Transform cameraInstance = camera.transform;
            
            Interact interact = cameraInstance.GetComponent<Interact>();
            if (camera == previousCamera)
            {
                previousCamera = interact.previousCamera;
            }
            interact.previousCamera.GetComponent<Interact>().nextCamera = interact.nextCamera;
            Destroy(cameraInstance.gameObject);

        }
    }
}
