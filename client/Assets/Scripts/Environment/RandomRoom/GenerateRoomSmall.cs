﻿using UnityEngine;
using Assets.Scripts.DataModels;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

namespace Assets.Scripts.Environment.RandomRoom
{
    class GenerateRoomSmall : GenerateRoomAbstract
    {
        public GenerateRoomSmall()
        {
        }

        public override GameObject CreateRoom(GameObject otherDoor, Part part, ImageGetter imageGetter, bool leftSide = false)
        {
			this.imageGetter = imageGetter;

			GameObject room = new GameObject(part.name);
            
			GameObject ground = new GameObject("Ground");
            ground.transform.SetParent(room.transform);
            ground.transform.localPosition = new Vector3(0, -height / 2, 0);
            GameObject wall = new GameObject("Wall");
            wall.transform.SetParent(room.transform);

            List<Content> cutContent = part.CutInSimpleParts();
            int size = (int)part.GetSimpleSize();

            List<Transform> wallList = CreatePrefabsFromContents(cutContent);

            // Create the entrance
            Transform wallInstance = Instantiate(wallPrefab);
            wallInstance.SetParent(wall.transform);
            wallInstance.localPosition = new Vector3(0, 0, -width);
            wallInstance = Instantiate(wallPrefab);
            wallInstance.SetParent(wall.transform);
            wallInstance.localPosition = new Vector3(0, 0, width);

            wallInstance = Instantiate(wallPrefab);
            wallInstance.SetParent(wall.transform);
            wallInstance.localPosition = new Vector3(0.5f * width, 0, -1.5f * width);
            wallInstance.localRotation = Quaternion.Euler(wallInstance.localEulerAngles.x, wallInstance.localEulerAngles.y - 90, wallInstance.localEulerAngles.z);
            wallInstance = Instantiate(wallPrefab);
            wallInstance.SetParent(wall.transform);
            wallInstance.localPosition = new Vector3(0.5f * width, 0, 1.5f * width);
            wallInstance.localRotation = Quaternion.Euler(wallInstance.localEulerAngles.x, wallInstance.localEulerAngles.y + 90, wallInstance.localEulerAngles.z);

            List<Transform> groundPieces = CreateGround(ground, 3 * 2);
            groundPieces[0].localPosition = new Vector3(0.5f * width, 0, -width);
            groundPieces[1].localPosition = new Vector3(0.5f * width, 0, 0);
            groundPieces[2].localPosition = new Vector3(0.5f * width, 0, width);

            groundPieces[3].localPosition = new Vector3(1.5f * width, 0, -width);
            groundPieces[4].localPosition = new Vector3(1.5f * width, 0, 0);
            groundPieces[5].localPosition = new Vector3(1.5f * width, 0, width);

            //Place the title and one painting
            wallInstance = Instantiate(titlePrefab);
            wallInstance.SetParent(wall.transform);
            Text titleText = wallInstance.FindChild("Canvas").FindChild("Title").GetComponent<Text>();
            titleText.text = part.name;
            wallInstance.localPosition = new Vector3(1.5f * width, 0, -1.5f * width);
            wallInstance.localRotation = Quaternion.Euler(wallInstance.localEulerAngles.x, wallInstance.localEulerAngles.y - 90, wallInstance.localEulerAngles.z);

            wallInstance = Instantiate(imagePrefab);
            wallInstance.SetParent(wall.transform);
            TableauResize resize = wallInstance.GetComponentInChildren<TableauResize>();
            imageGetter.SetImage(resize);

            wallInstance.localPosition = new Vector3(1.5f * width, 0, 1.5f * width);
            wallInstance.localRotation = Quaternion.Euler(wallInstance.localEulerAngles.x, wallInstance.localEulerAngles.y + 90, wallInstance.localEulerAngles.z);
            CreateCamera(wallInstance);

            int indexWall = 0;
            int pos = 2;
            while (indexWall < wallList.Count)
            {
                if (indexWall == 0)
                {
                    List<Transform> wallPiecesLeft = new List<Transform>();
                    List<Transform> wallPiecesRight = new List<Transform>();
                    wallList[indexWall].SetParent(wall.transform);
                    wallPiecesLeft.Add(wallList[indexWall]);
                    CreateCamera(wallList[indexWall]);

                    indexWall += 1;

                    if (wallList.Count - indexWall == 0)
                    {
                        wallInstance = Instantiate(imagePrefab);
                        wallInstance.SetParent(wall.transform);
                        resize = wallInstance.GetComponentInChildren<TableauResize>();
                        imageGetter.SetImage(resize);
                        wallPiecesRight.Add(wallInstance);
                    }
                    else
                    {
                        wallInstance = wallList[indexWall];
                        wallInstance.SetParent(wall.transform);
                        wallPiecesRight.Add(wallInstance);
                        indexWall += 1;
                    }

                    if (wallList.Count - indexWall != 1)
                    {
                        groundPieces = CreateGround(ground, 2 * 3);
                        wallInstance = Instantiate(wallPrefab);
                        wallInstance.SetParent(wall.transform);
                        wallPiecesLeft.Add(wallInstance);
                        wallInstance = Instantiate(wallPrefab);
                        wallInstance.SetParent(wall.transform);
                        wallPiecesRight.Add(wallInstance);
                    }
                    else
                    {
                        groundPieces = CreateGround(ground, 3);
                        CreateCamera(wallPiecesRight[0]);
                    }

                    TransformAndRotate(wallPiecesLeft, wallPiecesRight, groundPieces, ref pos);
                }
                else if (indexWall == 2)
                {
                    if (wallList.Count - indexWall == 1)
                    {
                        List<Transform> wallPiecesLeft = new List<Transform>();
                        List<Transform> wallPiecesRight = new List<Transform>();

                        wallInstance = Instantiate(imagePrefab);
                        wallInstance.SetParent(wall.transform);
                        resize = wallInstance.GetComponentInChildren<TableauResize>();
                        imageGetter.SetImage(resize);
                        CreateCamera(wallInstance);
                        wallPiecesLeft.Add(wallInstance);

                        wallInstance = wallList[indexWall];
                        wallInstance.SetParent(wall.transform);
                        indexWall += 1;
                        wallPiecesRight.Add(wallInstance);

                        wallInstance = Instantiate(wallPrefab);
                        wallInstance.SetParent(wall.transform);
                        wallPiecesLeft.Add(wallInstance);
                        wallInstance = Instantiate(wallPrefab);
                        wallInstance.SetParent(wall.transform);
                        wallPiecesRight.Add(wallInstance);

                        groundPieces = CreateGround(ground, 2 * 3);
                        TransformAndRotate(wallPiecesLeft, wallPiecesRight, groundPieces, ref pos);
                    }
                    else
                    {
                        if (wallList.Count - indexWall == 3)
                        {
                            List<Transform> wallPiecesLeft = CreateTwoPaintOneText(wall, wallList[indexWall]);
                            DestroyCamera(wallPiecesLeft[0]);
                            List<Transform> wallPiecesRight = CreateOnePaintTwoText(wall, wallList[indexWall + 1], wallList[indexWall + 2]);
                            DestroyCamera(wallPiecesRight[wallPiecesRight.Count - 2]);

                            groundPieces = CreateGround(ground, 4 * 3);
                            TransformAndRotate(wallPiecesLeft, wallPiecesRight, groundPieces, ref pos);
                            indexWall += 3;
                        }
                        else
                        {
                            List<Transform> wallPiecesLeft = CreateOnePaintOneText(wall, wallList[indexWall]);
                            DestroyCamera(wallPiecesLeft[0]);

                            List<Transform> wallPiecesRight = CreateOnePaintOneText(wall, wallList[indexWall + 1]);
                            DestroyCamera(wallPiecesRight[wallPiecesRight.Count - 2]);

                            groundPieces = CreateGround(ground, 3 * 3);
                            TransformAndRotate(wallPiecesLeft, wallPiecesRight, groundPieces, ref pos);
                            indexWall += 2;
                        }
                    }

                }
                else
                {
                    if (wallList.Count - indexWall == 2)
                    {
                        List<Transform> wallPiecesLeft = CreateOnePaintOneText(wall, wallList[indexWall]);
                        DestroyCamera(wallPiecesLeft[0]);

                        List<Transform> wallPiecesRight = CreateOnePaintOneText(wall, wallList[indexWall + 1]);
                        DestroyCamera(wallPiecesRight[wallPiecesRight.Count - 2]);

                        groundPieces = CreateGround(ground, 3 * 3);
                        TransformAndRotate(wallPiecesLeft, wallPiecesRight, groundPieces, ref pos);
                        indexWall += 2;
                    }
                    else
                    {
                        List<Transform> wallPiecesLeft = CreateOnePaintTwoText(wall, wallList[indexWall], wallList[indexWall + 1]);
                        DestroyCamera(wallPiecesLeft[0]);

                        List<Transform> wallPiecesRight = null;
                        if (wallList.Count - indexWall == 1)
                            wallPiecesRight = CreateTwoPaintOneText(wall, wallList[indexWall + 2]);
                        else
                            wallPiecesRight = CreateOnePaintTwoText(wall, wallList[indexWall + 2], wallList[indexWall + 3]);
                        DestroyCamera(wallPiecesRight[wallPiecesRight.Count - 2]);

                        groundPieces = CreateGround(ground, 4 * 3);
                        TransformAndRotate(wallPiecesLeft, wallPiecesRight, groundPieces, ref pos);
                        indexWall += 4;
                    }
                }
            }

            if (wallList.Count == 0)
            {
                List<Transform> wallPiecesLeft = new List<Transform>();
                List<Transform> wallPiecesRight = new List<Transform>();
                groundPieces = CreateGround(ground, 3);
                wallInstance = Instantiate(wallPrefab);
                wallInstance.SetParent(wall.transform);
                wallPiecesLeft.Add(wallInstance);
                wallInstance = Instantiate(wallPrefab);
                wallInstance.SetParent(wall.transform);
                wallPiecesRight.Add(wallInstance);
                TransformAndRotate(wallPiecesLeft, wallPiecesRight, groundPieces, ref pos);
            }

            wallInstance = Instantiate(wallPrefab);
            wallInstance.SetParent(wall.transform);
            wallInstance.localPosition = new Vector3(pos * width, 0, -width);
            wallInstance.localRotation = Quaternion.Euler(wallInstance.localEulerAngles.x, wallInstance.localEulerAngles.y + 180, wallInstance.localEulerAngles.z);
            wallInstance = Instantiate(wallPrefab);
            wallInstance.SetParent(wall.transform);
            wallInstance.localPosition = new Vector3(pos * width, 0, width);
            wallInstance.localRotation = Quaternion.Euler(wallInstance.localEulerAngles.x, wallInstance.localEulerAngles.y + 180, wallInstance.localEulerAngles.z);

            wallInstance = Instantiate(doorPrefab);
            wallInstance.tag = "ExitDoor";
            wallInstance.name = "ExitDoor";
            wallInstance.SetParent(wall.transform);
            wallInstance.localPosition = new Vector3(pos * width, 0, 0);
            wallInstance.localRotation = Quaternion.Euler(wallInstance.localEulerAngles.x, wallInstance.localEulerAngles.y + 180, wallInstance.localEulerAngles.z);

            //Now We have to link the other room to the new one
            float yRotation = (otherDoor.transform.rotation.eulerAngles.y + 360) % 360;
            room.transform.Rotate(0, yRotation + 180, 0);
            float x = otherDoor.transform.position.x;
            float y = otherDoor.transform.position.y;
            float z = otherDoor.transform.position.z;

            room.transform.position = new Vector3(x, y, z);

            return room;
        }
	

        public override bool IsValid(Part part)
        {
            return part.CutInSimpleParts().Count <= 8;
        }

        private void TransformAndRotate(List<Transform> wallPiecesLeft, List<Transform> wallPiecesRight, List<Transform> groundPieces, ref int pos)
        {
            for (int i = 0; i < wallPiecesLeft.Count; ++i)
            {
                Transform wallInstance = wallPiecesLeft[i];
                wallInstance.localPosition = new Vector3((pos + 0.5f) * width, 0, -1.5f * width);
                wallInstance.localRotation = Quaternion.Euler(wallInstance.localEulerAngles.x, wallInstance.localEulerAngles.y - 90, wallInstance.localEulerAngles.z);
                wallInstance = wallPiecesRight[i];
                wallInstance.localPosition = new Vector3((pos + 0.5f) * width, 0, 1.5f * width);
                wallInstance.localRotation = Quaternion.Euler(wallInstance.localEulerAngles.x, wallInstance.localEulerAngles.y + 90, wallInstance.localEulerAngles.z);
                for (int j = -1; j <= 1; ++j)
                    groundPieces[i * 3 + j + 1].localPosition = new Vector3((pos + 0.5f) * width, 0, j * width);

                pos += 1;
            }
        }
    }
}
