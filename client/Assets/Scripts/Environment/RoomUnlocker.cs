﻿using UnityEngine;
using System.Collections;
using Assets.Scripts.DataModels;
using System.Collections.Generic;

public class RoomUnlocker : MonoBehaviour {
    private GenerateRoom generator;
    private GenerateLibraryRoom libraryGenerator;
    private GameObject laser;

	// Use this for initialization
	void Start () {
        generator = GameObject.FindWithTag("GameController").GetComponent<GenerateRoom>();
        libraryGenerator = GameObject.FindWithTag("GameController").GetComponent<GenerateLibraryRoom>();
        laser = transform.FindChild("Laser").gameObject;
	}

    public void CreateRoom(Preview preview)
    {
        //Launch a loading event possible here
        generator.CreateRoom(gameObject, preview);

        laser.SetActive(false);
    }

    public void CreateLibraryRoom(List<Preview> previews)
    {
        libraryGenerator.CreateRoom(gameObject, previews);

        laser.SetActive(false);
    }
}
