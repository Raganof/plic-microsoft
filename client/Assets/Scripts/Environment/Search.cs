﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using Assets.Scripts.Tools;
using Assets.Scripts.DataModels;
using Assets.Scripts.Format;
using System.Text.RegularExpressions;

public class Search : MonoBehaviour {
    public GameObject itemPrefab;
    public GameObject laser;

    //public GameObject bookShelfPrefab;

    private InputField inputSearch;
	private Transform searchField;
	private Transform searchButton;
    private Transform resultField;
    private Transform resultFieldContent;
    private Transform returnButton;

    private LoadEvent loadingBar;

    private GameObject libraryDoor;
    private GameObject museumDoor;

    //Component for displaying description windows
    private GameObject descriptionWindow;
    private Text descriptionTitle;
    private Text descriptionContent;
    private Image descriptionImage;
    private Text loadingText;
    private Text playlistText;

    private List<Preview> resultPreviews;
    private Preview selectedResult;

    private List<GameObject> resultItems = new List<GameObject>();

    private Transform generateMuseum;
    private Transform generateLibrary;

    public WWW Www { get; set; }
    public WWW WwwContent { get; set; }

    public int generationChoice;

	public void Awake()
	{
        generationChoice = 0;

        generateMuseum = transform.FindChild("GenerateMuseum");
        generateLibrary = transform.FindChild("GenerateLibrary");

		searchField = transform.FindChild("SearchField");
		searchButton = transform.FindChild("SearchButton");
		inputSearch = searchField.GetComponent<InputField> ();
        resultField = transform.FindChild("ResultField");
        resultFieldContent = resultField.FindChild("ScrollView").FindChild("Content");
        loadingBar = transform.FindChild("Loading_Bar").GetComponent<LoadEvent>();
        returnButton = transform.FindChild("Return");

        loadingText = transform.FindChild("LoadingText").GetComponent<Text>();
        playlistText = transform.FindChild("PlaylistText").GetComponent<Text>();

        playlistText.gameObject.SetActive(false);

        returnButton.gameObject.SetActive(false);
        resultField.gameObject.SetActive(false);
        returnButton.GetComponent<Button>().onClick.AddListener(() => {Back();});

        libraryDoor = GameObject.FindWithTag("DoorLibrary");
        museumDoor = GameObject.FindWithTag("DoorMuseum");
       

        descriptionWindow = transform.parent.FindChild("DescriptionResult").gameObject;
        descriptionTitle = descriptionWindow.transform.FindChild("Title").FindChild("Text").GetComponent<Text>();
        descriptionContent = descriptionWindow.transform.FindChild("Content").FindChild("ScrollView").FindChild("Text").GetComponent<Text>();
        descriptionImage = descriptionWindow.transform.FindChild("Image").GetComponent<Image>();

        resultPreviews = new List<Preview>();

        searchButton.gameObject.SetActive(false);
        searchField.gameObject.SetActive(false);
    }

    public void ChooseMuseum()
    {
        generationChoice = 0;

        generateMuseum.gameObject.SetActive(false);
        generateLibrary.gameObject.SetActive(false);

        searchButton.gameObject.SetActive(true);
        searchField.gameObject.SetActive(true);
    }

    public void ChooseLibrary()
    {
        generationChoice = 1;

        generateMuseum.gameObject.SetActive(false);
        generateLibrary.gameObject.SetActive(false);

        searchButton.gameObject.SetActive(true);
        searchField.gameObject.SetActive(true);
    }

    public void Update()
    {
        if (WwwContent != null && WwwContent.isDone)
        {
            CreateRoom();
            WwwContent = null;
        }
        else if (Www != null && Www.isDone)
        {
            if (Www.text != null)
                UpdateSearchResults();

            //if (Www.texture != null)
            //    UpdateImg();
            Www = null;
        }
    }

    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;

        // check for errors
        if (www.error == null)
        {
            Debug.Log("WWW Ok!: " + www.text);
        }
        else
        {
            Debug.Log("WWW Error: " + www.error);
        }
    }

    /*IEnumerator WaitForImgStart(string url)
    {
        Www = new WWW("http://upload.wikimedia.org/wikipedia/en/2/2e/Google_Hummingbird_Logo.png");
        yield return Www;
        
    }*/

	public void SearchResults()
	{
        //Ignore multiple request     
        if (Www == null)
        {
            searchButton.gameObject.SetActive(false);
            //If there was a previous research
            if (resultItems != null)
            {
                //We delete the old results
                foreach (GameObject resultItem in resultItems)
                    Destroy(resultItem);
                resultItems.Clear();
            }
            
            loadingBar.StartInfiniteLoadEvent();
            loadingText.text = "Searching for \"" + inputSearch.text + "\"";

            String searchWord = inputSearch.text.Replace(" ", "_");
            string url = "localhost:60599/api/wikiapi/search/" + searchWord + "/count/10/page/0";
            Www = new WWW(url);

            StartCoroutine(WaitForRequest(Www));
        }
    }

    public void UpdateSearchResults()
    {
        searchButton.gameObject.SetActive(false);
        searchField.gameObject.SetActive(false);
        resultField.gameObject.SetActive(true);
        returnButton.gameObject.SetActive(true);

        playlistText.gameObject.SetActive(true);
        playlistText.text = "Playlist for \"" + inputSearch.text + "\"";

        
        JSONObject j = new JSONObject(Www.text);

        try
        {
            int rescount = (int)j["count"].n;
            JSONObject results = j["resources"];

            resultPreviews.Clear();

            foreach (var r in results.list)
            {
                Assets.Scripts.DataModels.Preview p = new Assets.Scripts.DataModels.Preview();
                p.Id = (int)r.GetField("Id").n;
                p.Name = r.GetField("Name").str;
                p.Description = r.GetField("Comment").str;
                p.Img = r.GetField("Depiction").str;

                resultPreviews.Add(p);
            }


            /*Transform tr = GameObject.FindGameObjectWithTag("Home").GetComponent<Transform>();
            GameObject boo = Instantiate(bookShelfPrefab);
            boo.transform.parent = tr;

            boo.transform.localPosition = new Vector3(-9.4f, 60.7f, 15.9f);

            BookShelfBehaviour bsb = boo.GetComponent<BookShelfBehaviour>();
            bsb.GenerateBooks(resultPreviews);*/
            

            //End TODO

            // We add a field for each result
            RectTransform rowRectTransform = itemPrefab.GetComponent<RectTransform>();
            RectTransform containerRectTransform = resultFieldContent.GetComponent<RectTransform>();

            containerRectTransform.rect.Set(containerRectTransform.rect.xMin, containerRectTransform.rect.yMin, containerRectTransform.rect.width, containerRectTransform.rect.height + 30);
            float width = containerRectTransform.rect.width;
            float height = rowRectTransform.rect.height;

            float scrollHeight = height * resultPreviews.Count;
            containerRectTransform.offsetMin = new Vector2(containerRectTransform.offsetMin.x, -scrollHeight / 2);
            containerRectTransform.offsetMax = new Vector2(containerRectTransform.offsetMax.x, scrollHeight / 2);

            for (int i = 0; i < resultPreviews.Count; ++i)
            {
                Preview preview = resultPreviews[i];
                GameObject resultItem = Instantiate(itemPrefab);
                resultItems.Add(resultItem);
                resultItem.name = preview.Name + "_ResultItem";
                resultItem.transform.SetParent(resultFieldContent);
                resultItem.GetComponent<Button>().onClick.AddListener(() => { ShowDescription(preview); });

                Text text = resultItem.transform.FindChild("Text").GetComponent<Text>(); ;
                text.text = preview.Name;

                RectTransform rectTransform = resultItem.GetComponent<RectTransform>();
                rectTransform.localScale = new Vector3(1, 1, 1);
                rectTransform.localPosition = new Vector3(0, 0, 0);
                rectTransform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));

                float x = -containerRectTransform.rect.width / 2;
                float y = containerRectTransform.rect.height / 2 - height * i;
                rectTransform.offsetMin = new Vector2(x, y);

                x = rectTransform.offsetMin.x + width;
                y = rectTransform.offsetMin.y + height;
                rectTransform.offsetMax = new Vector2(x, y);
            }
        }
        catch (NullReferenceException ex)
        {
            Debug.Log("The server is not launched or crashed");
        }
        
        loadingBar.StopLoadEvent();
        loadingText.text = "";
        Www = null;

        RequestCreateRoom();
    }

    public void Back()
    {
        searchButton.gameObject.SetActive(true);
        searchField.gameObject.SetActive(true);
        resultField.gameObject.SetActive(false);
        returnButton.gameObject.SetActive(false);

        playlistText.gameObject.SetActive(false);
        playlistText.text = "";

    }

    private void UpdateImg()
    {
        Debug.Log(Www.texture);
        descriptionImage.sprite = Sprite.Create(Www.texture,
                                                new Rect(0, 0, Www.texture.width, Www.texture.height),
                                                new Vector2(0, 0));

        Debug.Log(descriptionImage.sprite);

        Www = null;
    }

    public void ShowDescription(Preview preview)
    {
        selectedResult = preview;

        descriptionTitle.text = preview.Name;
        descriptionContent.text = preview.Description;

        /*if (preview.Img != null)
        {
            Www = new WWW("http://upload.wikimedia.org/wikipedia/en/2/2e/Google_Hummingbird_Logo.png");
            StartCoroutine(WaitForRequest(Www));
        }
        else
            descriptionImage.gameObject.SetActive(false);
        */

        descriptionWindow.SetActive(true);
        gameObject.SetActive(false);
    }

    public void RequestCreateRoom()
    {
        if (WwwContent == null)
        {
            loadingBar.StartInfiniteLoadEvent();

            //descriptionWindow.SetActive(false);
            //gameObject.SetActive(true);
            //Back();
            inputSearch.text = "";

            string url = "localhost:60599/api/resourceapi/get/" + resultPreviews[0].Id;
            WwwContent = new WWW(url);

            StartCoroutine(WaitForRequest(WwwContent));
        }
    }

    private void CreateRoom()
    {
        /*JSONObject json = new JSONObject(WwwContent.text);
        WwwContent = null;        
        JSONObject result = json["Content"];
        selectedResult.HtmlContent = result.str.Replace("\\\"", "'");
        selectedResult.HtmlContent = Regex.Replace(selectedResult.HtmlContent, @"<br[^\<]*>", " ");
        //selectedResult.HtmlContent = Regex.Replace(selectedResult.HtmlContent, @"<b[^\<]*>", " ");
        selectedResult.HtmlContent = selectedResult.HtmlContent.Trim('\r', '\n');
        selectedResult.Content = new Article(selectedResult.Name, Parser.ConcatParagraph(Parser.Parse(selectedResult.HtmlContent)));

        List<Part> parts = new List<Part>();

        foreach (var p in selectedResult.Content.parts)
        {
            if (p is Part)
                parts.Add((Part)p);
        }
        selectedResult.Content.realParts = parts;*/

        //First the room is created
        //museumDoor.GetComponent<RoomUnlocker>().CreateRoom(selectedResult);
        libraryDoor.GetComponent<RoomUnlocker>().CreateLibraryRoom(resultPreviews);

        loadingBar.StopLoadEvent();

        //Then, we hide the description page
        //CancelDescription();
        //And we display the Search
        //Back();
        //Finally we reset the search input
        inputSearch.text = "";

    }

    public void CancelDescription()
    {
        selectedResult = null;

        descriptionWindow.SetActive(false);
        gameObject.SetActive(true);
    }
}
