using System.Xml;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System;
using Assets.Scripts.DataModels;
using UnityEngine;

namespace Assets.Scripts.Format
{
    public class Parser
    {
        private static XmlReader CloneReader(XmlReader reader)
        {
            XmlDocument xmlDoc = new XmlDocument();
            MemoryStream stream = new MemoryStream();
            xmlDoc.Load(reader);
            xmlDoc.Save(stream);
            return XmlReader.Create(new MemoryStream(stream.ToArray()));
        }

        public static List<Content> Parse(string htmlToParse)
        {
            List<Content> parts = new List<Content>();

            //Initialize wiyth the html to parse
            bool hasReadPart = false;
            XmlReaderSettings settings = new XmlReaderSettings { ConformanceLevel = ConformanceLevel.Fragment, IgnoreWhitespace = true };
            using (XmlReader reader = XmlReader.Create(new StringReader(htmlToParse), settings))
            {
                Regex partRegex = new Regex(@"h[2-6]", RegexOptions.IgnoreCase);
                while (hasReadPart || reader.Read())
                {
                    hasReadPart = false;
                    if (reader.IsStartElement())
                    {
                        if (partRegex.Match(reader.Name).Success)
                        {
                            string tag = reader.Name;
                            string partTitle = ParseText(reader.ReadInnerXml());

                            //XmlReader cloneReader = CloneReader(reader);
                            string value = ReadToNextTag(reader, tag);
                            List<Content> contents = Parse(value);
                            Part part = new Part() { name = partTitle, subParts = contents };
                            parts.Add(part);
                            hasReadPart = !reader.EOF;
                        }
                        else if (reader.Name == "p")
                        {
                            string value = ParseText(reader.ReadInnerXml()).Trim('\r', '\n');
                            if (value != "")
                            {
                                Paragraph paragraph = new Paragraph() { content = value };
                                paragraph.fixContent();
                                parts.Add(paragraph);
                            }
                        }
                        else
                        {
                            string value = ParseText(reader.ReadInnerXml()).Trim('\r', '\n');
                            if (value != "")
                            {
                                Paragraph paragraph = new Paragraph() { content = value };
                                paragraph.fixContent();
                                parts.Add(paragraph);
                            }
                        }
                    }

                }
            }
            return parts;

        }

        private static string ParseText(string htmlTitle)
        {
            String title = "";
            XmlReaderSettings settings = new XmlReaderSettings { ConformanceLevel = ConformanceLevel.Fragment, IgnoreWhitespace = true };
            using (XmlReader reader = XmlReader.Create(new StringReader(htmlTitle), settings))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Text)
                    {
                        title += reader.Value.Trim('\r', '\n');
                    }
                }
            }
            return title;
        }

        private static string ReadToNextTag(XmlReader reader, string tag)
        {
            string str = "";
            while (reader.Read())
            {
                if (reader.IsStartElement() && reader.Name == tag)
                    return str;
                switch (reader.NodeType)
                {
                    case XmlNodeType.Element:
                        str += "<" + reader.Name + ">";
                        break;
                    case XmlNodeType.Text:
                        str += reader.Value.Trim('\r', '\n');
                        break;
                    case XmlNodeType.EndElement:
                        str += "</" + reader.Name + ">";
                        break;
                }
            }
            
            return str;
        }

        public static List<Content> ConcatParagraph(List<Content> contents)
        {
            List<Content> newContents = new List<Content>();
            String str = "";
            foreach (Content content in contents)
            {
                if (content is Paragraph)
                {
                    string[] split = content.ToString().Split('.', '!', '?');
                    foreach (string phrase in split)
                    {
                        if (str.Length + phrase.Length < Content.lenPerUnity)
                        {
                            str += " " + phrase;
                        }
                        else
                        {
                            newContents.Add(new Paragraph() { content = str });
                            str = phrase;
                        }
                    }
                }
                else
                {
                    if (str != "")
                    {
                        newContents.Add(new Paragraph() { content = str });
                        str = "";
                    }
                    Part part = (Part)content;
                    part.subParts = ConcatParagraph(part.subParts);
                    newContents.Add(part);
                }
            }
            if (str != "")
                newContents.Add(new Paragraph() { content = str });

            return newContents;
        }
    }
}