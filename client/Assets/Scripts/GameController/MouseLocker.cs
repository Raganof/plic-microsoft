using System;
using UnityEngine;

public class MouseLocker: MonoBehaviour
{
    private bool isLock = true;

    public void Start()
    {
        Cursor.lockState = CursorLockMode.Locked; //Lock the cursor in the middle of the screen
        Cursor.visible = false; //Set the cursor invisible    
    }

    public void Display()
    {
        isLock = true;
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void Undisplay()
    {
        isLock = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;    
    }

    public void Lock()
    {
        if (isLock)
            Display();
        else
            Undisplay();
    }
}
