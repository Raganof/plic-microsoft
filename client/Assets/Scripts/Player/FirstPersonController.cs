using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(AudioSource))]
public class FirstPersonController : MonoBehaviour
{
    [SerializeField]
    private bool isWalking; //True if the character is walking
    [SerializeField]
    private float walkSpeed; //The speed when walking
    [SerializeField]
    private float runSpeed; //The speed when running
    [SerializeField]
    private float speedDampTime; //The time needed to pass from a speed to another (walkSpeed to runSpeed for example)
    [SerializeField]
    private float angularSpeedDampTime; //Same as speedDampTime but for rotation
    [SerializeField]
    [Range(0f, 1f)] //Block the value between 0 and 1
    private float runstepLenghten; //The length between each step when running
    [SerializeField]
    private float gravityMultiplier; //Gravity multiplier (Slowing the character when moving on +y)
    [SerializeField]
    private MouseLook mouseLook; //Script allowing the camera to follow the mouse
    [SerializeField]
    private float stepInterval; //The minimal step
    [SerializeField]
    private AudioClip[] footstepSounds;// An array of footstep sounds that will be randomly selected from.

    private Camera camera; //The main camera
    private float yRotation; //The rotation on Y of the character
    private Vector2 input; //The x and z input (horiz and vertical)
    private Vector3 moveDir = Vector3.zero; //The direction of the movement
    private CharacterController characterController;
    private CollisionFlags collisionFlags;
    private float stepCycle; //The current step value
    private float nextStep; //The nextStep value
    private AudioSource audioSource;

    private Animator anim; // The animator to animate the character
    private HashIDs hash; //Allowing to access easier to variable in animator

    public bool isLock = false; //Unable the player to move when needed

    // Use this for initialization
    private void Start()
    {
        characterController = GetComponent<CharacterController>();
        camera = Camera.main;
        stepCycle = 0f;
        nextStep = stepCycle / 2f;
        audioSource = GetComponent<AudioSource>();
        mouseLook.Init(transform, camera.transform);

        anim = GetComponentInChildren<Animator>();
        hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();
    }


    // Update is called once per frame
    private void Update()
    {
        if (!isLock)
            RotateView();
    }

    private void FixedUpdate()
    {
        if (!isLock)
        {
            float speed;
            GetInput(out speed);
            // always move along the camera forward as it is the direction that it being aimed at
            Vector3 desiredMove = transform.forward * input.y + transform.right * input.x;

            // get a normal for the surface that is being touched to move along it
            RaycastHit hitInfo;
            Physics.SphereCast(transform.position, characterController.radius, Vector3.down, out hitInfo,
                               characterController.height / 2f);
            desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

            moveDir.x = desiredMove.x * speed;
            moveDir.z = desiredMove.z * speed;

            collisionFlags = characterController.Move(moveDir * Time.fixedDeltaTime);

            ProgressStepCycle(speed);
        }
    }


    private void ProgressStepCycle(float speed)
    {
        //If we are moving
        if (characterController.velocity.sqrMagnitude > 0 && (input.x != 0 || input.y != 0))
        {
            stepCycle += (characterController.velocity.magnitude + (speed * (isWalking ? 1f : runstepLenghten))) *
                         Time.fixedDeltaTime;
        }

        if (!(stepCycle > nextStep))
        {
            return;
        }

        nextStep = stepCycle + stepInterval;

        PlayFootStepAudio();
    }


    private void PlayFootStepAudio()
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        int n = Random.Range(1, footstepSounds.Length);
        audioSource.clip = footstepSounds[n];
        audioSource.PlayOneShot(audioSource.clip);
        // move picked sound to index 0 so it's not picked next time
        footstepSounds[n] = footstepSounds[0];
        footstepSounds[0] = audioSource.clip;
    }


    private void GetInput(out float speed)
    {
        // Read input
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");

#if !MOBILE_INPUT
        // On standalone builds, walk/run speed is modified by a key press.
        // keep track of whether or not the character is walking or running
        isWalking = !Input.GetKey(KeyCode.LeftShift);
#endif
        // set the desired speed to be walking or running
        speed = isWalking ? walkSpeed : runSpeed;
        
        input = new Vector2(horizontal, vertical);

        // normalize input if it exceeds 1 in combined length:
        if (input.sqrMagnitude > 1)
        {
            input.Normalize();
        }
        anim.SetFloat(hash.speedFloat, input.y * speed);
    }


    private void RotateView()
    {
        mouseLook.LookRotation(transform, camera.transform);
    }


    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody body = hit.collider.attachedRigidbody;
        //dont move the rigidbody if the character is on top of it
        if (collisionFlags == CollisionFlags.Below)
        {
            return;
        }

        if (body == null || body.isKinematic)
        {
            return;
        }
        body.AddForceAtPosition(characterController.velocity * 0.1f, hit.point, ForceMode.Impulse);
    }
}
