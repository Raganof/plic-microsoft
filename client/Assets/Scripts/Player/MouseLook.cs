using System;
using UnityEngine;

[Serializable]
public class MouseLook
{
    public float XSensitivity = 2f; // Sensitivity on xAxis
    public float YSensitivity = 2f; //Sensitivity on yAxis
    public bool clampVerticalRotation = true; //Lock vertical rotation (on Y)
    public float MinimumX = -90F; // The minimum rotation on xAxis
    public float MaximumX = 90F; // The maximu rotation on xAxis
    public bool smooth = true; //Telling if we smooth the movement or not
    public float smoothTime = 5f; //The time to pass from a rotation to another


    private Quaternion characterTargetRotation;
    private Quaternion cameraTargetRotation;
    //private Animator anim;  //Le moindre micro mouvement et le personnage se met en mouvement donc d�sactiv�
    //private HashIDs hash;

    public void Init(Transform character, Transform camera)
    {
        characterTargetRotation = character.localRotation;
        cameraTargetRotation = camera.localRotation;
        //anim = character.GetComponent<Animator>();
        //hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();
    }


    public void LookRotation(Transform character, Transform camera)
    {
        float yRot = Input.GetAxis("Mouse X") * XSensitivity;
        float xRot = Input.GetAxis("Mouse Y") * YSensitivity;

        characterTargetRotation *= Quaternion.Euler(0f, yRot, 0f);
        cameraTargetRotation *= Quaternion.Euler(-xRot, 0f, 0f);

        if (clampVerticalRotation)
            cameraTargetRotation = ClampRotationAroundXAxis(cameraTargetRotation);

        if (smooth)
        {
            //anim.SetFloat(hash.angularSpeedFloat, characterTargetRotation.eulerAngles.sqrMagnitude - character.localRotation.eulerAngles.sqrMagnitude, smoothTime, Time.deltaTime);
            character.localRotation = Quaternion.Slerp(character.localRotation, characterTargetRotation,
                smoothTime * Time.deltaTime);
            camera.localRotation = Quaternion.Slerp(camera.localRotation, cameraTargetRotation,
                smoothTime * Time.deltaTime);
        }
        else
        {
            character.localRotation = characterTargetRotation;
            camera.localRotation = cameraTargetRotation;
        }
    }


    Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        q.x /= q.w;
        q.y /= q.w;
        q.z /= q.w;
        q.w = 1.0f;

        float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(q.x);

        angleX = Mathf.Clamp(angleX, MinimumX, MaximumX);

        q.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

        return q;
    }

}
