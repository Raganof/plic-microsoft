﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Tools
{
    public class RequestJob : ThreadedJob
    {
        public String Result { get; set; }
        public WWW Www { get; set; }

        protected override void ThreadFunction()
        {
            string url = "localhost:60599/api/wikiapi/search/" + "Phonograph" + "/count/20";
            Www = new WWW(url);
        }
        protected override void OnFinished()
        {
             Result = Www.text;
        }
    }
}
