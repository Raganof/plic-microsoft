﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Tools
{
    // Implementation inspired by http://answers.unity3d.com/questions/357033/unity3d-and-c-coroutines-vs-threading.html
    public class ThreadedJob
    {
        private bool isDone = false;
        private object handle = new object();
        private System.Threading.Thread thread = null;

        public bool IsDone
        {
            get
            {
                bool tmp;
                lock (handle)
                {
                    tmp = isDone;
                }
                return tmp;
            }
            set
            {
                lock (handle)
                {
                    isDone = value;
                }
            }
        }

        public virtual void Start()
        {
            thread = new System.Threading.Thread(Run);
            thread.Start();
        }

        public virtual void Abort()
        {
            thread.Abort();
        }

        protected virtual void ThreadFunction() { }

        protected virtual void OnFinished() { }

        public virtual bool Update()
        {
            if (IsDone)
            {
                OnFinished();
                return true;
            }
            return false;
        }

        private void Run()
        {
            ThreadFunction();
            IsDone = true;
        }
    }
}
