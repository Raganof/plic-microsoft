﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ComboBoxEvent : MonoBehaviour {
    public string nameScript;
    public Text comboBoxText;
	public GameObject itemsView;
    private ComboBoxAction comboBoxAction;

    private bool isDisplaying;

	public void Awake()
	{
        isDisplaying = false;
		itemsView.SetActive (false);
        comboBoxAction = GetComponent<ComboBoxAction>();
        
        //BringToFront(itemsView.transform);
	}

	public void setSelectedItem(string item)
	{
		comboBoxText.text = item;

        comboBoxAction.generateEvent(item);
	}

	public void DisplayItems()
	{
		if (isDisplaying)
			HideItems ();
		else
			ShowItems ();

		isDisplaying = !isDisplaying;
	}

	public void HideItems()
	{
		itemsView.SetActive (false);
	}

	public void ShowItems()
	{
		itemsView.SetActive (true);
        itemsView.transform.SetAsLastSibling();
    }

    private void BringToFront(Transform transform)
    {
        transform.SetAsFirstSibling();
        for (int i = 0; i < transform.childCount; ++i)
        {
            BringToFront(transform.GetChild(i));
        }
    }
}
