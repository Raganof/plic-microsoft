﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SelectItem : MonoBehaviour {
	private Text text;
	private ComboBoxEvent comboBoxEvent;

	public void Awake()
	{
		text = transform.GetComponentInChildren<Text> ();
        comboBoxEvent = transform.parent.parent.parent.parent.GetComponent<ComboBoxEvent>();
	}

	public void OnClick()
	{
		comboBoxEvent.setSelectedItem (text.text);
		comboBoxEvent.DisplayItems ();
	}
}
