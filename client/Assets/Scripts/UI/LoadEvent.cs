﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LoadEvent : MonoBehaviour {
    private Image image;
    private bool isInfiniteLoad = false;

	// Use this for initialization
	void Start () {
        image = this.GetComponent<Image>();
        image.fillAmount = 0;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (isInfiniteLoad)
        {
            image.fillAmount = (image.fillAmount + 0.01f) % 1.0f;
        }
	}

    public void StartInfiniteLoadEvent()
    {
        gameObject.SetActive(true);
        isInfiniteLoad = true;
        image.fillAmount = 0;
    }

    public void StartLoadEvent()
    {
        image.fillAmount = 0;
        gameObject.SetActive(true);
    }

    public void SetLoadAmount(float percent)
    {
        image.fillAmount = percent;
    }

    public void StopLoadEvent()
    {
        gameObject.SetActive(false);
        isInfiniteLoad = true;
    }
}
