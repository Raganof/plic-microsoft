﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ManageVideoSettings : MonoBehaviour {
	private int width; // The Width of the screen
	private int height; // The Height of the screen
    private Text resolution;
    private Toggle fullscreen;
    private int quality;

	public void Awake()
	{
		width = Screen.width;
		height = Screen.height;
        resolution = transform.FindChild("Resolution").FindChild("ComboBoxResolution").FindChild("Text").GetComponent<Text>();
        quality = QualitySettings.GetQualityLevel(); 

        // Update the state of the fullscreen toggle
        fullscreen = transform.FindChild("Fullscreen").GetComponent<Toggle>();
        fullscreen.isOn = Screen.fullScreen;
    }

	public void SetResolution(string resolution)
	{
		string[] wh = resolution.Split('*');
		width = int.Parse (wh [0]);
		height = int.Parse (wh [1]);
	}

    public void SetQuality(int quality)
    {
        this.quality = quality;
    }

	public void apply()
	{
		Screen.SetResolution(width, height, fullscreen.isOn);
        QualitySettings.SetQualityLevel(quality);
	}

	public void unapply()
	{
        // Restore to the current state
		width = Screen.width;
		height = Screen.height;
        resolution.text = width.ToString() + "*" + height.ToString();
        fullscreen.isOn = Screen.fullScreen;
        quality = QualitySettings.GetQualityLevel();
	}
}
