﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class QualitySetting : MonoBehaviour, ComboBoxAction {
    private Text comboBoxText;
	private Dictionary<string, int> qualityDictionary;
    private ManageVideoSettings videoSettings;

    public void Awake()
    {
        videoSettings = GameObject.Find("VideoSettings").GetComponent<ManageVideoSettings>();
        comboBoxText = transform.FindChild("Text").GetComponent<Text>();
		qualityDictionary = new Dictionary<string, int> ()
		{
			{"Fastest", 0},
			{"Fast", 1},
			{"Simple", 2},
			{"Good", 3},
			{"Beautiful", 4},
			{"Fantastic", 5}
		};
		switch (QualitySettings.GetQualityLevel()) {
		case 0:
			comboBoxText.text = "Fastest";
			break;
		case 1:
			comboBoxText.text = "Fast";
			break;
		case 2:
			comboBoxText.text = "Simple";
			break;
		case 3:
			comboBoxText.text = "Good";
			break;
		case 4:
			comboBoxText.text = "Beautiful";
			break;
		case 5:
			comboBoxText.text = "Fantastic";
			break;
		default:
			break;
		}
    }

    public void generateEvent(string item)
    {
		videoSettings.SetQuality(qualityDictionary [item]);
    }
}
