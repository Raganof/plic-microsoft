﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ResolutionSettings : MonoBehaviour, ComboBoxAction {
    private Text comboBoxText;
    private ManageVideoSettings videoSettings;


    public void Awake()
    {
        comboBoxText = transform.FindChild("Text").GetComponent<Text>();

        videoSettings = GameObject.Find("VideoSettings").GetComponent<ManageVideoSettings>();
        string width = Screen.width.ToString();
        string height = Screen.height.ToString();
        comboBoxText.text = width + "*" + height;
    }

    public void generateEvent(string item)
    {
        videoSettings.SetResolution(item);
    }
}
