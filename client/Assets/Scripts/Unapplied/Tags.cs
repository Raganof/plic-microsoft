﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour {
	// A list of tag strings.
	public const string player = "Player";
	public const string gameController = "GameController";
	public const string mainLight = "MainLight";
	public const string fader = "Fader";
    public const string libraryDoor = "LibraryDoor";
    public const string museumDoor = "MuseumDoor";
	public const string exitDoor = "ExitDoor";
    public const string book = "Book";
    public const string bookShelf = "BookShelf";
}
