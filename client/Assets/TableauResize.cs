﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TableauResize : MonoBehaviour {
    private Sprite image;
    public float maxHeight = 1200f;

    private float border = 0.5f;
    private WWW www;

	// Use this for initialization
    void Start()
    {
        if (image == null)
            return;

        //ApplyResize();
    }

    public void Update()
    {
        if (www != null && www.isDone)
        {
            SetImage(Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0)));
            www = null;
        }
    }

    public void SetImage(Sprite image)
    {
        this.image = image;
        if (this.image != null)
            ApplyResize();
    }

    public void SetImage(string url)
    {
       www = new WWW(url);
		Debug.Log (url);
    }

    private void ApplyResize()
    {
        Transform tableau = transform.FindChild("default");
        Transform canvas = transform.FindChild("ImageCanvas");

        Renderer canvasRenderer = canvas.GetComponent<Renderer>();
        RectTransform rect = canvas.GetComponent<RectTransform>();
        Image imageUI = canvas.FindChild("Image").GetComponent<Image>();
        imageUI.sprite = image;

        Renderer renderer = tableau.GetComponent<Renderer>();
        //Set Image Scale to fit the tableau
        //canvas.localScale = new Vector3(canvas.localScale.x, canvas.localScale.y, canvas.localScale.z);

        //Set Tableau Scale
        float oldWidth = rect.rect.width;
        float oldHeight = rect.rect.height;
        float height = rect.rect.height;
        if (image.rect.height >= image.rect.width)
            height = maxHeight;

        float width = (height / image.rect.height) * image.rect.width;

        //Scale Canvas
        rect.rect.Set(0, 0, width, height);

        //And then the object
        float scaleX = tableau.localScale.x;
        float scaleY = tableau.localScale.y;
        float ratio = width / height;

        if (ratio < oldWidth / oldHeight)
            scaleX *= ratio * oldHeight / oldWidth;
        else
            scaleY *= oldWidth / (oldHeight * ratio);
        float scaleZ = (scaleX + scaleY) / 2;

        tableau.localScale = new Vector3(scaleX, scaleY, scaleZ);

        //We place the drawing at the center
        if (image.rect.height >= image.rect.width)
            rect.localPosition = new Vector3((rect.localPosition.x - 1.5f) * scaleX, rect.localPosition.y * scaleY, rect.localPosition.z);
        else
            rect.localPosition = new Vector3(rect.localPosition.x * scaleX, rect.localPosition.y * scaleY, rect.localPosition.z);

    }
}
