#! /usr/bin/env python3

from html.parser import HTMLParser
from lxml import html
import requests

typeDictionnary = {
    "xsd:string" : "string",
    "xsd:nonNegativeInteger" : "int",
    "xsd:langString" : "string",
    "xsd:float" : "float",
    "xsd:double" : "double",
    "xsd:integer" : "int",
    "xsd:negativeInteger" : "int",
    "xsd:nonPositiveInteger" : "int",
    "xsd:positiveInteger" : "int",
    "xsd:boolean" : "bool",
    "owl:Thing" : "Thing",
}

classes = []

def encode_class(name):
    if (":" in name):
        global typeDictionnary
        if (name in typeDictionnary):
            return typeDictionnary[name]
        return name.replace(":", "__")
    return name

class Property():
    def __init__(self, propertyName, returnType, label, comment):
        self.propertyName = propertyName
        self.returnType = returnType
        self.label = label
        self.comment = comment

    def getName(self):
        return self.propertyName

    def print_(self, f):
        if (self.comment == "\n        "):
            f.write("        /* " + self.label + " */\n")
        else:
            f.write("        /* " + self.label + "\n        " + self.comment + " */\n")
        f.write("        public " + encode_class(self.returnType) + " " + self.propertyName + " { get; set; }\n\n")


class StockClass():
    def __init__(self, className, parent):
        self.baseName = className
        self.name = encode_class(className)
        self.parent = parent
        self.props = []
        self.dictionnary = []
        self.lastLevel = True

    def addProperty(self, name, rang, label, comment):
        self.props.append(Property(name, rang, label, comment))

    def addPropertyToDictionnary(self, prop):
        self.dictionnary.append(prop)

    def notLastLevel(self):
        self.lastLevel = False

    def isLastLevel(self):
        return self.lastLevel

    def getName(self):
        return self.name

    def getParent(self):
        return self.parent

    def getProperties(self):
        return self.props

    def print_(self):
        self.f = open("DAO/" + self.name + ".cs", "w+")
        self.f.write("using System;\n\n" +
                "namespace DAO\n" +
                "{\n\n" +
                "    [Display(Url = " + self.baseName + ")]\n" +
                "    public class " + self.name + " : " + self.parent + "\n" +
                "    {\n" +
                "        public " + self.name + "()\n" +
                "        {}" + "\n\n" +
                "        public void accept(Visitor v)\n" +
                "        {\n" +
                "            v.visit(this);\n" +
                "        }\n\n")

        for prop in self.props:
            prop.print_(self.f)

        if self.dictionnary != []:
            self.f.write("        public Dictionary<String, String> dictionary = new Dictionary<String, String>()\n" +
                         "        {\n")
            i = len(self.dictionnary)
            for element in self.dictionnary:
                if i != 0:
                    self.f.write("            {\"" + element + "\", null},\n")
                else:
                    self.f.write("            {\"" + element + "\", null}\n")
            self.f.write("        }\n\n")

        self.f.write("    }\n" +
                "}\n")

class HierarchyParser(HTMLParser):
    parents = ["Object"]

    def handle_starttag(self, tag, attrs):
        if tag == "a":
            for attr in attrs:
                if attr[0] == "name":
                    parent = self.parents.pop()
                    self.parents.append(parent)
                    for possibleParent in classes:
                        if possibleParent.getName() == parent:
                            possibleParent.notLastLevel()
                    generate_files(attr[1], parent)
                    self.parents.append(encode_class(attr[1]))

    def handle_endtag(self, tag):
        if tag == "li":
            self.parents.pop()

class ClassParser(HTMLParser):
    name = ""
    label = ""
    domain = ""
    rang = ""
    comment = ""
    td = 0
    parentInfo = False

    def __init__(self, className, parent):
        HTMLParser.__init__(self)
        self.className = encode_class(className)
        self.currentClass = StockClass(className, parent)

    def handle_starttag(self, tag, attrs):
        if tag == "td":
            self.td = self.td + 1


    def handle_endtag(self, tag):
        if tag == "tr":
            if ((self.td == 5) and (self.parentInfo)):
                self.currentClass.addProperty(self.name, self.rang, self.label, self.comment)

            self.td = 0
            self.name = ""
            self.label = ""
            self.parentInfo = False
            self.comment = ""
            self.rang = ""
        if tag == "body":
            classes.append(self.currentClass)

    def handle_data(self, data):
        if ((self.td == 1) and (self.name == "")):
            self.name = data
        elif ((self.td == 2) and (self.label == "")):
            self.label = data
        elif ((self.td == 3) and (not self.parentInfo)):
            self.parentInfo = (self.className == encode_class(data))
        elif ((self.td == 4) and (self.rang == "")):
            self.rang = data
        elif ((self.td == 5) and (self.comment == "")):
            self.comment = data



def create_properties(current, parent):
    parser = ClassParser(current, parent)
    url = "http://mappings.dbpedia.org/server/ontology/classes/" + current.replace(":", "%3A")
    page = requests.get(url)
    parser.feed(page.text)

def generate_files(current, parent):
    print(current)
    create_properties(current, parent)


parser = HierarchyParser()
page = requests.get('http://mappings.dbpedia.org/server/ontology/classes/')
parser.feed(page.text)

#generate_files("owl:Thing", "Object")
#generate_files("Activity", "Thing")

print("Number of classes found: " + str(len(classes)))
print("Set dictionnary")
for classe in classes:
 #   print(classe.getName() + str(classe.isLastLevel()))
    if classe.isLastLevel():
        for parent in classes:
            if parent.getName() == classe.getParent():
                for prop in classe.getProperties():
                    parent.addPropertyToDictionnary(prop.getName())

print("Print files")
filesCreated = 0
for classe in classes:
    if not classe.isLastLevel():
        classe.print_()
        filesCreated = filesCreated + 1
print("Files created: " + str(filesCreated))
