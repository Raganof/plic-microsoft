﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VDS.RDF;
using ResourceModels.Models;
using ResourceModels.Tools;

namespace ResourceModels.Data_Access.Data_Graph
{
    public class ResourceGraph : VirtualWikiGraph
    {

        public static long addResource(PlaceHolderResource pr)
        {
            long rid = 0;
            rid = getGraphConnect().Cypher
                .Merge("(res:Resource { Id: {id} })")
                .OnCreate()
                .Set("res = {pr}")
                .WithParams(new
                {
                    id = pr.Id,
                    pr
                })
                .Return<long>("id(res)")
                .Results
                .First();

            return rid;
        }

        public static long addResource(PlaceHolderResource pr, List<Triple> properties)
        {
            long rid = 0;

            var query = getGraphConnect().Cypher
                .Merge("(sres:Resource { NodeUri: {snodeUri} })")
                .OnCreate()
                .Set("sres = {spr}")
                .WithParams(new
                {
                    snodeUri = pr.NodeUri,
                    spr = pr
                })
                .OnMatch()
                .Set("sres = {spr2}")
                .WithParams(new
                {
                    spr2 = pr
                });


            int i = 0;
            foreach (Triple t in properties)
            {

                String relationName = UriToPropertyMapper.GetPropertyNameByAttribute<PlaceHolderResource>(t.Predicate.ToString());
                String relation = "sres-[:" + relationName + "]->ores" + i;
                
                query = query
                .Merge("(ores" + i + ":Resource { NodeUri: {onodeUri" + i + "} })")
                .OnCreate()
                .Set("ores" + i + " = {opr" + i + "}")
                .WithParam("onodeUri" + i, t.Object.ToString())
                .WithParam("opr" + i, new { nodeUri = t.Object.ToString() })
                .CreateUnique(relation);

                i++;
            }

            //t.Predicate.ToString()

            //System.Diagnostics.Debug.WriteLine("yop: " + query.Query.QueryText);


            query.ExecuteWithoutResults();

            return rid;
        }

        public static List<long> addResource(List<Triple> subjects)
        {
            List<long> rid = new List<long>();

            if (subjects.Count > 0)
            {
                int i = 0;
                var query = getGraphConnect().Cypher;

                foreach (Triple t in subjects)
                {
                    String relationName = UriToPropertyMapper.GetPropertyNameByAttribute<PlaceHolderResource>(t.Predicate.ToString());
                    String relation = "sres" + i + "-[:" + relationName + "]->ores" + i;

                    query = query
                    .Merge("(sres" + i + ":Resource { NodeUri: {snodeUri" + i + "} })")
                    .OnCreate()
                    .Set("sres" + i + " = {spr" + i + "}")
                    .WithParam("snodeUri" + i, t.Subject.ToString())
                    .WithParam("spr" + i, new { nodeUri = t.Subject.ToString() })
                    .Merge("(ores" + i + ":Resource { NodeUri: {onodeUri" + i + "} })")
                    .OnCreate()
                    .Set("ores" + i + " = {opr" + i + "}")
                    .WithParam("onodeUri" + i, t.Object.ToString())
                    .WithParam("opr" + i, new { nodeUri = t.Object.ToString() })
                    .CreateUnique(relation);

                    i++;
                }

                query.ExecuteWithoutResults();
            }
            

            return rid;
        }

        public static PlaceHolderResource getResourceById(int id)
        {
            PlaceHolderResource resource = null;

            resource = getGraphConnect().Cypher
                    .Match("(r:Resource)")
                    .Where("has(r.Id) AND (r.Id = {id})")
                    .WithParam("id", id)
                    .Return<PlaceHolderResource>("r")
                    .Results.FirstOrDefault();

            return resource;
        }

        public static PlaceHolderResource getResourceByNodeUri(String nodeUri)
        {
            PlaceHolderResource resource = null;

            resource = getGraphConnect().Cypher
                    .Match("(r:Resource)")
                    .Where("has(r.NodeUri) AND has(r.Name) AND (r.NodeUri = {uri})")
                    .WithParam("uri", nodeUri)
                    .Return<PlaceHolderResource>("r")
                    .Results.FirstOrDefault();

            return resource;
        }

        public static List<PlaceHolderResource> getResourcesByInput(String input, int count, int page)
        {
            List<PlaceHolderResource> resources = new List<PlaceHolderResource>();

            foreach (String s in input.Split('_'))
            {
                String regex = "(?i).*" + s + ".*";
                List<PlaceHolderResource> pr = getGraphConnect().Cypher
                    .Match("(r:Resource)")
                    .Where("has(r.Name) AND has(r.Comment) AND (r.Name =~ {regex} OR r.Comment =~ {regex})")
                    .WithParam("regex", regex)
                    .Return<PlaceHolderResource>("r")
                    .Skip(count * page)
                    .Limit(count)
                    .Results.ToList();

                resources = resources.Union(pr, new ResourceModels.Tools.ResourceComparer()).ToList();
            }


            return resources;
        }
    }
}