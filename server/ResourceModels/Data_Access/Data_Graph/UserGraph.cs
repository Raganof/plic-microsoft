﻿using ResourceModels.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResourceModels.Data_Access.Data_Graph
{
    public class UserGraph : VirtualWikiGraph
    {
        public static long addResource(User user)
        {
            long rid = 0;
            rid = getGraphConnect().Cypher
                .Merge("(res:User { Id: {id} })")
                .OnCreate()
                .Set("res = {user}")
                .WithParams(new
                {
                    id = user.Id,
                })
                .Return<long>("id(res)")
                .Results
                .First();

            return rid;
        }

        public void addUserToResourceRelation(User user, PlaceHolderResource pr, string relationType)
        {
            string relation = "guser-[:" + relationType + "]->r";

            getGraphConnect().Cypher
                .Match("(guser:User)", "(r:Resource)")
                .Where((User guser) => guser.Id == user.Id)
                .AndWhere((PlaceHolderResource r) => r.Id == pr.Id)
                .CreateUnique(relation)
                .ExecuteWithoutResults();
        }

        public void deleteUserToResourceRelation(User user, PlaceHolderResource pr, string relationType)
        {
            string colonRelationType = ":" + relationType;

            string relation = "(guser:User)-[" + colonRelationType + "]->(r:Resource)";

            getGraphConnect().Cypher
                .OptionalMatch(relation)
                .Where((User guser) => guser.Id == user.Id)
                .AndWhere((PlaceHolderResource r) => r.Id == pr.Id)
                .Delete(colonRelationType)
                .ExecuteWithoutResults();
        }

        public void addUserToUserRelation(User user, User user2, string relationType)
        {
            string relation = "guser1-[:" + relationType + "]->guser2";

            getGraphConnect().Cypher
                .Match("(guser1:User)", "(guser2:User)")
                .Where((User guser) => guser.Id == user.Id)
                .AndWhere((User guser2) => guser2.Id == user2.Id)
                .CreateUnique(relation)
                .ExecuteWithoutResults();
        }

        public void deleteUserToUserRelation(User user, User user2, string relationType)
        {
            string colonRelationType = ":" + relationType;

            string relation = "(guser1:User)-[" + colonRelationType + "]->(guser2:User)";
 
            getGraphConnect().Cypher
                .OptionalMatch(relation)
                .Where((User guser1) => guser1.Id == user.Id)
                .AndWhere((User guser2) => guser2.Id == user2.Id)
                .Delete(colonRelationType)
                .ExecuteWithoutResults();
        }
    }
}
