﻿using Neo4jClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ResourceModels.Data_Access.Data_Graph
{
    public class VirtualWikiGraph
    {
        const String username = "neo4j";
        const String password = "plicmti";

        protected static GraphClient graphClient;

        public static GraphClient getGraphConnect()
        {
            try
            {
                if (graphClient == null)
                {
                    //connection to the neo4j db initalization -> we should work with env info
                    graphClient = new GraphClient(new Uri(string.Concat("http://", HttpUtility.UrlEncode(username),
                                                                        ":", HttpUtility.UrlEncode(password), "@localhost.:7474/db/data")));
                    graphClient.Connect();

                }
            }
            catch (Exception exception)
            {
                Console.WriteLine("Neo4j is not launched.");
            }

            return graphClient;
        }
    }
}
