﻿using ResourceModels.Data_Access.Data_Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ResourceModels.Data_Access.Data_Table
{
    public class ResourceData
    {
        private static ResourceModels.Models.PlaceHolderResource convertBddToModel(T_Resource resourceBdd)
        {
            ResourceModels.Models.PlaceHolderResource r = null;
            if (resourceBdd != null)
            {
                r = new ResourceModels.Models.PlaceHolderResource();
                r.Id = resourceBdd.id;
                r.Content = resourceBdd.content;
            }

            return r;
        }

        public static bool CreateResource(ResourceModels.Models.PlaceHolderResource r)
        {
            try
            {

                using (VirtualWikiEntities model = new VirtualWikiEntities())
                {
                    T_Resource res = new T_Resource();
                    res.id = r.Id;
                    res.content = r.Content;

                    model.T_Resource.Add(res);
                    model.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool DeleteResource(long id)
        {
            try
            {
                using (VirtualWikiEntities model = new VirtualWikiEntities())
                {
                    model.T_Resource.Remove(model.T_Resource.Where(x => x.id == id).FirstOrDefault());
                    model.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateResource(ResourceModels.Models.PlaceHolderResource r)
        {
            try
            {
                using (VirtualWikiEntities model = new VirtualWikiEntities())
                {
                    T_Resource resourceDB = model.T_Resource.Where(x => x.id == r.Id).FirstOrDefault();
                    if (resourceDB != null)
                    {
                        resourceDB.content = r.Content;

                        model.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return CreateResource(r);
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static ResourceModels.Models.PlaceHolderResource GetResource(long id)
        {
            try
            {
                using (VirtualWikiEntities model = new VirtualWikiEntities())
                {
                    return convertBddToModel(model.T_Resource.Where(x => x.id == id).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}