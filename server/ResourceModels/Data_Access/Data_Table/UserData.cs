﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResourceModels.Data_Access.Data_Table
{
    public class UserData
    {
        private static ResourceModels.Models.User convertBddToModel(T_User userBdd)
        {
            ResourceModels.Models.User u = null;
            if (userBdd != null)
            {
                u = new ResourceModels.Models.User();
                u.Id = userBdd.id;
                u.Firstname = userBdd.firstname;
                u.Name = userBdd.name;
                u.Login = userBdd.login;
                u.Email = userBdd.email;
                u.PasswordHash = userBdd.password_hash;
                u.Token = userBdd.token;
                u.CreationDate = userBdd.creation_date;
            }

            return u;
        }

        public static bool CreateUser(ResourceModels.Models.User u)
        {
            try
            {

                using (VirtualWikiEntities model = new VirtualWikiEntities())
                {
                    T_User tu = model.T_User.Where(x => (x.email == u.Email || x.login == u.Login)).FirstOrDefault();

                    if (tu == null)
                    {
                        T_User user = new T_User();

                        user.firstname = u.Firstname;
                        user.name = u.Name;
                        user.login = u.Login;
                        user.email = u.Email;
                        user.password_hash = u.PasswordHash;

                        u.Token = Guid.NewGuid().ToString();

                        user.token = u.Token;
                        user.creation_date = u.CreationDate;

                        model.T_User.Add(user);
                        model.SaveChanges();

                        return true;
                    }
                    else
                        return false;
                    
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool DeleteUser(long id)
        {
            try
            {
                using (VirtualWikiEntities model = new VirtualWikiEntities())
                {
                    model.T_User.Remove(model.T_User.Where(x => x.id == id).FirstOrDefault());
                    model.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateUser(ResourceModels.Models.User u)
        {
            try
            {
                using (VirtualWikiEntities model = new VirtualWikiEntities())
                {
                    T_User userDB = model.T_User.Where(x => x.id == u.Id).FirstOrDefault();
                    if (userDB != null)
                    {
                        userDB.firstname = u.Firstname;
                        userDB.name = u.Name;
                        userDB.password_hash = u.PasswordHash;

                        model.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return CreateUser(u);
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static ResourceModels.Models.User GetUser(long id)
        {
            try
            {
                using (VirtualWikiEntities model = new VirtualWikiEntities())
                {
                    return convertBddToModel(model.T_User.Where(x => x.id == id).FirstOrDefault());
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static ResourceModels.Models.User GetUser(string identifier, string password)
        {
            try
            {
                using (VirtualWikiEntities model = new VirtualWikiEntities())
                {
                    return convertBddToModel(model.T_User.Where(x => (x.email == identifier || x.login == identifier)
                                                                     && x.password_hash == password).FirstOrDefault());
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static ResourceModels.Models.User GetUser(string token)
        {
            try
            {
                using (VirtualWikiEntities model = new VirtualWikiEntities())
                {
                    return convertBddToModel(model.T_User.Where(x => x.token == token).FirstOrDefault());
                }

            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
