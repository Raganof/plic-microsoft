﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Web;
using ResourceModels.Tools;

namespace ResourceModels.Models
{
    [JsonObject(MemberSerialization.OptOut)]
    [Requestable(Name = "actor")]
    [UriAttribute(Name = "to see")]
    public class PlaceHolderResource
    {
        public String NodeUri { get; set; }

        [UriAttribute(Name = "http://dbpedia.org/ontology/wikiPageID")]
        public long Id { get; set; }

        [UriAttribute(Name = "http://www.w3.org/2000/01/rdf-schema#label")]
        [Requestable(Name = "Name")]
        public String Name { get; set; }

        [Requestable(Name = "Birth")]
        [UriAttribute(Name = "http://www.w3.org/2000/01/rdf-schema#comment")]
        public String Comment { get; set; }

        [UriAttribute(Name = "http://xmlns.com/foaf/0.1/depiction")]
        public String Depiction { get; set; }

        [UriAttribute(Name = "http://dbpedia.org/ontology/thumbnail")]
        public String Thumbnail { get; set; }

        [Newtonsoft.Json.JsonIgnore]
        public String Content { get; set; }
    }
}