﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VDS.RDF;

namespace ResourceModels.Models
{
    public class ResourceTriple
    {
        public Tuple<PlaceHolderResource, List<Triple>, List<Triple>> Triple { get; set; }
    }
}
