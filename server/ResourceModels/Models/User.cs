﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResourceModels.Models
{
    public class User
    {
        public long Id { get; set; }

        public String Firstname { get; set; }

        public String Name { get; set; }

        public String Login { get; set; }

        public String Email { get; set; }

        public String PasswordHash { get; set; }

        public String Token { get; set; }

        public DateTime CreationDate { get; set; }

    }
}
