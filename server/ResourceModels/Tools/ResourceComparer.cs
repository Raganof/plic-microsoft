﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ResourceModels.Models;

namespace ResourceModels.Tools
{
    public class ResourceComparer : IEqualityComparer<PlaceHolderResource>
    {
        public bool Equals(PlaceHolderResource x, PlaceHolderResource y)
        {
            return x.NodeUri == y.NodeUri;
        }

        public int GetHashCode(PlaceHolderResource obj)
        {
            return (int) obj.Id;
        }
    }
}