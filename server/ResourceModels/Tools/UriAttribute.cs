﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ResourceModels.Tools
{
    [System.AttributeUsage(System.AttributeTargets.All)]
    public class UriAttribute : System.Attribute
    {
        public string Name { get; set; }
    }
}