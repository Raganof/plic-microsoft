﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ResourceModels.Tools
{
    public class UriToPropertyMapper
    {
        private static Dictionary<Type, Dictionary<String, String>> dc;

        private static Dictionary<String, Type> dst;

        private static Dictionary<String, Tuple<Type, List<String>>> requestable_prop_by_class;

        //private static Dictionary<String, Tuple<Type, String, String>> 

        public static Dictionary<String, String> getPropertiesFromAttributes<T>()
        {
            if (dc == null)
                dc = new Dictionary<Type, Dictionary<string, string>>();

            Dictionary<String, String> res = null;
            if (!dc.ContainsKey(typeof(T)))
            {
                res = new Dictionary<String, String>();
                PropertyInfo[] properties = typeof(T).GetProperties();

                for (int i = 0; i < properties.Length; i++)
                {
                    Tools.UriAttribute attribute = properties[i].GetCustomAttributes(typeof(Tools.UriAttribute), true)
                                                                    .Cast<Tools.UriAttribute>().FirstOrDefault();
                    if (attribute != null)
                        res.Add(attribute.Name, properties[i].Name);
                }

                dc.Add(typeof(T), res);
            }
            else
                dc.TryGetValue(typeof(T), out res);

            return res;
        }

        public static void SetPropertyByAttribute<T, Y>(T obj, String attrName, Y valueObj)
        {
            Dictionary<String, String> dic = getPropertiesFromAttributes<T>();

            String propName = "";

            if (dic.TryGetValue(attrName, out propName))
                obj.GetType().GetProperty(propName).SetValue(obj, valueObj);
        }

        public static Y GetPropertyByAttribute<T, Y>(T obj, String attrName)
        {
            Dictionary<String, String> dic = getPropertiesFromAttributes<T>();

            String propName = "";

            if (dic.TryGetValue(attrName, out propName))
                return (Y)obj.GetType().GetProperty(propName)
                                             .GetValue(null, null);
            return default(Y);
        }

        public static String GetPropertyNameByAttribute<T>(String attrName)
        {
            Dictionary<String, String> dic = getPropertiesFromAttributes<T>();

            String propName = "";

            if (dic.TryGetValue(attrName, out propName))
            {
                return propName;
            }
            else
                return "otherlink";
        }

        public static Dictionary<String, Tuple<Type, List<String>>> buildClassNameToRequestableProp()
        {
            if (requestable_prop_by_class == null)
            {
                requestable_prop_by_class = new Dictionary<String, Tuple<Type, List<String>>>();

                string @namespace = "ResourceModels.Models";

                var entities = from e in Assembly.GetExecutingAssembly().GetTypes()
                               where e.IsClass && e.Namespace == @namespace
                               select e;

                entities.ToList().ForEach(e =>
                {
                    List<String> properties = new List<string>();

                    Tools.Requestable classAttribute = e.GetCustomAttributes(typeof(Tools.Requestable), true)
                                                        .Cast<Tools.Requestable>().FirstOrDefault();
                    if (classAttribute != null)
                    {
                        e.GetProperties().ToList()
                         .ForEach(ea =>
                         {
                            Tools.Requestable propAttribute =  ea.GetCustomAttributes(typeof(Tools.Requestable), true)
                                                                 .Cast<Tools.Requestable>().FirstOrDefault();
                            
                             if (propAttribute != null)
                                properties.Add(propAttribute.Name);
                         });

                        requestable_prop_by_class.Add(classAttribute.Name, new Tuple<Type,List<string>>(e, properties));
                    }   
                });
            }

            return requestable_prop_by_class;
        }

        public static Tuple<Type, List<String>> getTypeByEntityName(String entityName)
        {
            buildClassNameToRequestableProp();
            Tuple<Type, List<String>> tp = null;

            if (requestable_prop_by_class.ContainsKey(entityName))
                requestable_prop_by_class.TryGetValue(entityName, out tp);
                
            return tp;
        }

        /*public static List<String> getRequestablePropByClassName(String className)
        {
            List<String> resqProp = new List<string>();
            Type t = getTypeByEntityName(className);

            if (t != null)
            {
                
            }
        }*/
    }
}