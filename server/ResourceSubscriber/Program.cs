﻿using EasyNetQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ResourceModels.Models;
using VDS.RDF;
using ResourceModels.Data_Access.Data_Graph;

namespace ResourceSubscriber
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var bus = RabbitHutch.CreateBus("host=localhost"))
            {
                bus.Subscribe<ResourceModels.Models.ResourceTriple>("test", HandleTextMessage);

                bus.Subscribe<ResourceModels.Models.PlaceHolderResource>("test", HandleResourceContent);

                Console.WriteLine("Listening for messages. Hit <return> to quit.");
                Console.ReadLine();
            }
        }

        static void HandleTextMessage(ResourceModels.Models.ResourceTriple testo)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Got message: {0}", testo.Triple.Item1.NodeUri);
            Console.ResetColor();


            ResourceGraph.addResource(testo.Triple.Item1, testo.Triple.Item2);

            ResourceGraph.addResource(testo.Triple.Item3);
        }

        static void HandleResourceContent(ResourceModels.Models.PlaceHolderResource tr)
        {
            ResourceModels.Data_Access.Data_Table.ResourceData.UpdateResource(tr);
        }
    }
}
