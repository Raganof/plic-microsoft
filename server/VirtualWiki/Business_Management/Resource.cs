﻿using EasyNetQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VirtualWiki.Business_Management
{
    public class Resource
    {
        public static bool CreateResource(ResourceModels.Models.PlaceHolderResource r)
        {
            return ResourceModels.Data_Access.Data_Table.ResourceData.CreateResource(r);
        }

        public static bool DeleteResource(long id)
        {
            return ResourceModels.Data_Access.Data_Table.ResourceData.DeleteResource(id);
        }

        public static ResourceModels.Models.PlaceHolderResource GetResource(long id)
        {
            return ResourceModels.Data_Access.Data_Table.ResourceData.GetResource(id);
        }

        public static ResourceModels.Models.PlaceHolderResource GetResource(ResourceModels.Models.PlaceHolderResource r)
        {
            ResourceModels.Models.PlaceHolderResource tr = ResourceModels.Data_Access.Data_Table.ResourceData.GetResource(r.Id);

            if (tr == null || tr.Content == null)
            {
                if (tr == null)
                {
                    tr = new ResourceModels.Models.PlaceHolderResource();
                    tr.Id = r.Id;
                }

                tr.Content = Data_Access.Wikipedia.Request.RunAsync((int)r.Id).Result;

                using (var bus = RabbitHutch.CreateBus("host=localhost"))
                {
                    bus.Publish(tr);
                }

                ResourceModels.Data_Access.Data_Table.ResourceData.UpdateResource(tr);
            }

            r.Content = tr.Content;
            return r;
        }
    }
}