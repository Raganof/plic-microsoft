﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;

namespace VirtualWiki.Business_Management
{
    public class User
    {
        public static bool CreateUser(ResourceModels.Models.User u)
        {
            string hash = ResourceModels.Tools.HashHelper.GetSHA512(u.PasswordHash);
            return ResourceModels.Data_Access.Data_Table.UserData.CreateUser(u);
        }

        public static bool DeleteUser(long id)
        {
            return ResourceModels.Data_Access.Data_Table.UserData.DeleteUser(id);
        }

        public static ResourceModels.Models.User GetUser(long id)
        {
            return ResourceModels.Data_Access.Data_Table.UserData.GetUser(id);
        }

        public static ResourceModels.Models.User GetUser(string identifier, string password)
        {
            return ResourceModels.Data_Access.Data_Table.UserData.GetUser(identifier, password);
        }

        public static ResourceModels.Models.User GetUser(string token)
        {
            return ResourceModels.Data_Access.Data_Table.UserData.GetUser(token);
        }
    }
}