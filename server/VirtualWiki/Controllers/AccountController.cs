﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using VirtualWiki.Models;

namespace VirtualWiki.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/

        public ActionResult Index()
        {
            return RedirectToAction("Login", "Account", null);
        }

        [HttpGet]
        public ActionResult Register(string returnUrl = "")
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Login", "Account", null);

            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [HttpPost]
        public ActionResult Register(Signup model, string returnUrl = "")
        {
            if (ModelState.IsValid)
            {
                ResourceModels.Models.User user = new ResourceModels.Models.User() { Firstname = model.Firstname, Name = model.Name, Login = model.Login, Email = model.Email, PasswordHash = model.Password, CreationDate = DateTime.Today};
                
                if (!Business_Management.User.CreateUser(user))
                    ModelState.AddModelError("", "An account already exists with these identifiers!");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Login(Login model, string returnUrl = "")
        {
            if (ModelState.IsValid)
            {
                if (Membership.ValidateUser(model.Identifier, model.Password))
                    FormsAuthentication.RedirectFromLoginPage(model.Identifier, model.Remember);

                ModelState.AddModelError("", "Your Username and / or password are incorrect!");
            }

            return View(model);
        }

        [Route("api/account/login")]
        [HttpGet]
        public JObject Login(string identifier, string password)
        {
            JObject jres = null;
            string status = "OK";
            string token = "none";

            if (identifier == null || identifier.Length == 0
                || password == null || password.Length == 0)
                status = "Identification error";
            else
            {
                ResourceModels.Models.User user = Business_Management.User.GetUser(identifier, password);

                if (user != null)
                    token = user.Token;
            }

            jres = new JObject(
                        new JProperty("status",
                                status
                        ),
                            new JProperty("token",
                                token
                        )
                    );

            return jres;
        }

        [HttpGet]
        public ActionResult Login(string returnUrl = "")
        {
            if (User.Identity.IsAuthenticated)
                return Logout();

            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [HttpPost]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Login", "Account", null);
        }

    }
}