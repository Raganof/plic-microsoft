﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace VirtualWiki.Controllers
{
    public class DiscoveryAPIController : ApiController
    {
        // GET: api/DiscoverAPI
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [Route("api/discoveryapi/entity/{entity}")]
        [HttpGet]
        public JObject Get(string entity)
        {
            Tuple<Type, List<String>> tp = null;
            if (entity != null)
                tp = ResourceModels.Tools.UriToPropertyMapper.getTypeByEntityName(entity);

            List<String> prop = tp != null ? tp.Item2 : new List<String>();

            JObject jres = new JObject(
                            new JProperty("properties",
                                JArray.FromObject(prop)
                            )
                           );

            return jres;
        }

        // GET: api/DiscoverAPI/5
        [Route("api/discoveryapi/entity/{entity}/relation/{relation}/condition/{condition:alpha}")]
        [HttpGet]
        public string Get(string entity, string relation, string condition)
        {
            return "value";
        }

        [Route("api/discoveryapi/entity/{entity}/relation/{relation}/condition/{condition:int}")]
        [HttpGet]
        public int Get(string entity, string relation, int condition)
        {
            return 42;
        }

        // POST: api/DiscoverAPI
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/DiscoverAPI/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/DiscoverAPI/5
        public void Delete(int id)
        {
        }
    }
}
