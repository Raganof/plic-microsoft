﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ResourceModels.Data_Access.Data_Graph;
using ResourceModels.Models;

namespace VirtualWiki.Controllers
{
    public class ResourceAPIController : ApiController
    {
        // GET: api/Resource
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Resource/5
        [Route("api/resourceapi/get/{id}")]
        [HttpGet]
        public JObject Get(int id)
        {
            PlaceHolderResource r = ResourceGraph.getResourceById(id);
            JObject jobj = null;

            if (r != null)
            {
                Business_Management.Resource.GetResource(r);
                jobj = JObject.FromObject(r);
                jobj.Add("Content", r.Content);
            }
            else
            {
                jobj = new JObject();
                jobj.Add("Error", "Resource not found");
            }

            return jobj;
        }

        // POST: api/Resource
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Resource/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Resource/5
        public void Delete(int id)
        {
        }
    }
}
