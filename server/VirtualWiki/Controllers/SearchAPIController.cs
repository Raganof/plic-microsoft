﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ResourceModels.Data_Access.Data_Graph;
using VirtualWiki.Data_Access.Dbpedia;
using ResourceModels.Models;

namespace VirtualWiki.Controllers
{
    public class SearchAPIController : ApiController
    {
        [Route("api/wikiapi/search/")]
        [HttpGet]
        // GET api/wikiapi
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [Route("api/wikiapi/search/{input}")]
        [HttpGet]
        // GET api/wikiapi/Tina_Fey
        public JObject Get(string input)
        {
            /*List<PlaceHolderResource> l = SearchRequest.getTokens(input, 0, SearchRequest.getResultResource).Result;
            String t = "";
            l.ForEach(pr => { ResourceGraph.addResource(pr); t += " " + pr.Name; });*/

            List<PlaceHolderResource> l = ResourceGraph.getResourcesByInput(input, 20, 0);

            JObject jres = new JObject(
                            new JProperty("count", l.Count),
                            new JProperty("resources",
                                new JArray(
                                    from r in l
                                    orderby r.Name
                                    select new JObject(
                                        new JProperty("Id", r.Id),
                                        new JProperty("Name", r.Name),
                                        new JProperty("Description", r.Comment)
                                    )
                                )
                            )
                           );

            return jres;
        }

        [Route("api/wikiapi/search/{input}/count/{count}/page/{page}")]
        [HttpGet]
        // GET api/wikiapi/Tina_Fey
        public JObject Get(string input, int count, int page)
        {
            List<PlaceHolderResource> l = ResourceGraph.getResourcesByInput(input, count, page);

            int i = 0;
            bool isEmpty = false;

            while (!isEmpty && l.Count < count)
            {
                List<PlaceHolderResource> nl = SearchRequest.getTokens(input, (count - l.Count) + 2, (count - l.Count) * i, l, SearchRequest.getResultResource).Result;

                if (nl != null)
                {
                    if (nl.Count > 0)
                        l = l.Concat(nl).ToList();
                }
                else
                   isEmpty = true;

                i++;
            }
            JObject jres = new JObject(
                            new JProperty("count", l.Count),
                            new JProperty("resources",
                                new JArray(
                                    from r in l
                                    select JObject.FromObject(r)
                                )
                            )
                           );

            return jres;
        }
    }
}
