﻿using Neo4jClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using VDS.RDF;
using VDS.RDF.Query;
using VDS.RDF.Writing;
using ResourceModels.Data_Access.Data_Graph;
using ResourceModels.Models;
using ResourceModels.Tools;
using EasyNetQ;

namespace VirtualWiki.Data_Access.Dbpedia
{
    public class SearchRequest
    {
        public static async Task<List<PlaceHolderResource>> getTokens(String input, int count, int offset, List<PlaceHolderResource> l, Func<String, Task<List<PlaceHolderResource>>> resJsonGet)
        {
            String t_input = input;
            List<PlaceHolderResource> res = new List<PlaceHolderResource>();

            if (t_input.Contains('_'))
            {
                t_input = t_input.ToUpperInvariant().Replace("_", " AND ");
                //s_search = " \' ( " + s_search + ") \' ";
            }

            SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri("http://dbpedia.org/sparql"));

            String query = " select ?s1 as ?c1 where    {      {        {          select ?s1, ( ?sc * 3e-1 ) as ?sc, ?o1, ( sql:rnk_scale ( <LONG::IRI_RANK> ( ?s1 ) ) ) as ?rank, ?g where          {            quad map virtrdf:DefaultQuadMap            {              graph ?g              {                ?s1 ?s1textp ?o1 .               ?o1 bif:contains ' (" + t_input + " ) ' option ( score ?sc ) .               FILTER (str(?g) = 'http://dbpedia.org') .               FILTER (lang(?o1) = 'en') .             }            }          }        group by ?s1        order by desc ( ?sc + ?rank * 0.5 ) limit " + count + " offset " + offset + "        }      }    }    group by ?s1    order by desc (SUM( ?sc + ?rank * 0.5 ))";

            endpoint.ResultsAcceptHeader = "application/sparql-results+json";

            String db = "";
            try
            {
                SparqlResultSet ss = endpoint.QueryWithResultSet(query);

                SparqlJsonWriter rdj = new SparqlJsonWriter();
                System.IO.StringWriter sw = new System.IO.StringWriter();

                rdj.Save(ss, sw);
                db = sw.ToString();
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                return res;
            }

            dynamic json2 = JsonConvert.DeserializeObject(db);

            JArray ja = json2.results.bindings;

            if (ja.Count > 0)
            {
                List<Task<List<PlaceHolderResource>>> tasks = new List<Task<List<PlaceHolderResource>>>();
                //we get search result links on dbpedia

                for (JToken ob = ja.First; ob != ja.Last; ob = ob.Next)
                {
                    if (l.Find(pr => pr.NodeUri.Equals(ob["c1"]["value"].ToString())) == null
                        && ResourceGraph.getResourceByNodeUri(ob["c1"]["value"].ToString()) == null)
                    {
                        /*PlaceHolderResource tempR = ResourceGraph.getResourceByNodeUri(ob["c1"]["value"].ToString());

                        if (tempR != null)
                            res.Add(tempR);
                        else
                        {*/
                            Task<List<PlaceHolderResource>> tr = resJsonGet(ob["c1"]["value"].ToString());
                            tasks.Add(tr);
                        //}

                    }

                    //List<PlaceHolderResource> tmp = tr.Result;
                    //res = res.Concat(tmp).ToList();
                    //tr.ContinueWith(t => res = res.Concat(t.Result).ToList());
                }

                while (tasks.Count > 0)
                {
                    Task<List<PlaceHolderResource>> firstRes = await Task.WhenAny<List<PlaceHolderResource>>(tasks).ConfigureAwait(false);

                    tasks.Remove(firstRes);

                    List<PlaceHolderResource> resources = await firstRes.ConfigureAwait(false);
                    res = res.Concat(resources).ToList();
                }
            }
            else
                res = null;

            return res;
        }

        public static async Task<List<PlaceHolderResource>> getResultResource(String token)
        {
            List<PlaceHolderResource> res = new List<PlaceHolderResource>();

            const String sbase = "http://dbpedia.org/sparql";

            SparqlRemoteEndpoint endpoint = new SparqlRemoteEndpoint(new Uri(sbase));

            String query = "describe <" + token + ">";

            IGraph g = null;

            try
            {
                g = endpoint.QueryWithResultGraph(query);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.Message);
                Debug.WriteLine(e.StackTrace);

                return res;
            }

            //first: get the main node and extract the info => subject
            //then: delete those triplet from the main graph
            // send the graph to the crawling process

            //IEnumerable<Triple> ts = g.GetTriplesWithPredicate(new Uri("http://www.w3.org/2002/07/owl#sameAs"));

            //IEnumerable<Triple> ts = g.GetTriplesWithSubject(new Uri(token));

            //IUriNode n = g.CreateUriNode(new Uri("http://www.w3.org/2002/07/owl#sameAs"));

            //IEnumerable<Triple> tt = ts.WithPredicate(n);

            List<Triple> subjects = new List<Triple>();
            List<Triple> properties = new List<Triple>();

            Dictionary<String, PlaceHolderResource> dsubj = new Dictionary<String, PlaceHolderResource>();

            PlaceHolderResource resultNode = new PlaceHolderResource() { NodeUri = token };
            foreach (Triple t in g.Triples)
            {

                if (!t.Predicate.ToString().Equals("http://www.w3.org/2002/07/owl#sameAs")
                    && (!t.Predicate.ToString().Equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type") || ((UriNode) t.Object).Uri.Host.Equals("dbpedia.org"))
                    && (t.Object.NodeType != NodeType.Literal || ((LiteralNode)t.Object).Language.Equals("en") || (((LiteralNode)t.Object).DataType != null && ((LiteralNode)t.Object).DataType.Fragment == "#integer")))
                {
                    if (t.Subject.ToString().Equals(token))
                    {
                        /*if (t.Object.NodeType == NodeType.Literal)
                        {
                            PlaceHolderResource.SetPropertiesFromAttributes(resultNode, t.Predicate.ToString(), ((LiteralNode)t.Object).Value);
                        }
                        else
                        {
                            List<PlaceHolderResource> lphr = PlaceHolderResource.GetPropertiesFromAttributes<PlaceHolderResource, List<PlaceHolderResource>>(resultNode, t.Predicate.ToString());

                            if (lphr == null)
                                lphr = new List<PlaceHolderResource>();
                                

                            lphr.Add(new PlaceHolderResource() { NodeUri = t.Object.ToString()});
                            PlaceHolderResource.SetPropertiesFromAttributes(resultNode, t.Predicate.ToString(), lphr);
                        }*/

                        if (t.Object.NodeType == NodeType.Literal)
                        {
                            LiteralNode ln = (LiteralNode)t.Object;

                            if (ln.DataType != null && ln.DataType.Fragment == "#integer")
                                UriToPropertyMapper.SetPropertyByAttribute(resultNode, t.Predicate.ToString(), Int64.Parse(ln.Value));
                            else
                                UriToPropertyMapper.SetPropertyByAttribute(resultNode, t.Predicate.ToString(), ln.Value);

                        }
                        else
                        {
                            if (t.Predicate.ToString().Equals("http://xmlns.com/foaf/0.1/depiction")
                                || t.Predicate.ToString().Equals("http://dbpedia.org/ontology/thumbnail"))
                            {
                                UriNode un = (UriNode)t.Object;
                                
                                if (un.EffectiveType != null)
                                    UriToPropertyMapper.SetPropertyByAttribute(resultNode, t.Predicate.ToString(), un.Uri.ToString());
                            }
                            else
                                properties.Add(t);
                        }
                    }
                    else
                    {
                        subjects.Add(t);

                        /*PlaceHolderResource subjectNode = new PlaceHolderResource() { NodeUri = t.Subject.ToString() };

                        List<PlaceHolderResource> lphr = PlaceHolderResource.GetPropertiesFromAttributes<PlaceHolderResource, List<PlaceHolderResource>>(resultNode, t.Predicate.ToString());

                        if (lphr == null)
                            lphr = new List<PlaceHolderResource>();


                        lphr.Add(new PlaceHolderResource() { NodeUri = token });
                        PlaceHolderResource.SetPropertiesFromAttributes(resultNode, t.Predicate.ToString(), lphr);*/
                    }
                }
            }

            Debug.WriteLine("namu: " + resultNode.Name);
            //ResourceGraph.addResource(resultNode, properties.Take(10).ToList());

            //ResourceGraph.addResource(subjects.Take(10).ToList());

            using (var bus = RabbitHutch.CreateBus("host=localhost"))
            {
                ResourceModels.Models.ResourceTriple tr = new ResourceTriple()
                {
                    Triple = new Tuple<PlaceHolderResource, List<Triple>, List<Triple>>(resultNode, properties.Take(10).ToList(),
                           subjects.Take(10).ToList())
                };
                bus.Publish(tr);
            }


            res.Add(resultNode);

            return res;
        }
    }
}