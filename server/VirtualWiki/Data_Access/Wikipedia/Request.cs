﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace VirtualWiki.Data_Access.Wikipedia
{
    public class Request
    {
        //RunAsync(value).Wait();
        public static async Task<String> RunAsync(int id)
        {
            String content = null;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://en.wikipedia.org/w/");
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("api.php?continue=&format=json&action=query&prop=extracts&pageids=" + id + "&redirects=true").ConfigureAwait(false);

                //http://en.wikipedia.org/w/api.php?continue=&action=query&format=json&titles=NeoGAF&generator=links&gpllimit=max 
                if (response.IsSuccessStatusCode)
                {
                    string s = await response.Content.ReadAsStringAsync();
                    dynamic json = JsonConvert.DeserializeObject(s);
                    content = json.query.pages[id.ToString()].extract;
                }
                else
                    content = null;
            }

            return content;
        }
    }
}